<?php

class Slave
{
    private $slv1 = 1;
    private $slv2 = 'slv2';

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}

class Test
{
    private $atr1 = 1;
    private $atr2 = 'atr2';
    private $slv;

    public function __construct($slv)
    {
        $this->slv = $slv;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}

$slv = new Slave();

$test = new Test($slv);
