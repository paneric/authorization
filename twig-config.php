<?php

return [
    'twig' => [
        'templates_dirs' => [

        ],
        'options' => [
            'debug' => false,
            'charset' => 'UTF-8',
            'strict_variables' => false,
            'autoescape' => 'html',
            'cache' => false,
            'auto_reload' => null,
            'optimizations' => -1,
        ],
    ]
];
