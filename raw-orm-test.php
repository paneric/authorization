<?php

declare(strict_types=1);

require 'vendor/autoload.php';

use Paneric\Authorization\ORM\Repository\AccessRepository;
use Paneric\Doctrine\EntityManagerBuilder;
use Paneric\Authorization\ORM\Repository\FieldRepository;
use Paneric\Authorization\ORM\Repository\PrivilegeRepository;
use Paneric\Authorization\ORM\Repository\RoleRepository;
use test\Paneric\Authorization\ORM\Query\AccessQueryTest;
use test\Paneric\Authorization\ORM\Query\FieldQueryTest;
use test\Paneric\Authorization\ORM\Query\PrivilegeQueryTest;
use test\Paneric\Authorization\ORM\Query\RoleQueryTest;
use test\Paneric\Authorization\ORM\Repository\AccessRepositoryTest;
use test\Paneric\Authorization\ORM\Repository\FieldRepositoryTest;
use test\Paneric\Authorization\ORM\Repository\PrivilegeRepositoryTest;
use test\Paneric\Authorization\ORM\Repository\RoleRepositoryTest;

/**  PREPARATIONS (without ORM - problems with created_at and updated_at automatic values):
  * ----------------
  * 1) CLI: sudo service mysql start;
  * 2) CLI: mysql -u root -p
  * 3) MySql Shell: mysql> create database authorization;
  * 4) MySql Shell: mysql> exit;
  * 5) fixtures and schema from: authorization.sql
  * 6) php raw-orm-test
  */

$EntityManagerBuilder = new EntityManagerBuilder();
$_em = $EntityManagerBuilder->build(
    require('/home/paneric/Bureau/php-dev/PANERIC/authorization/orm-config.php')
);

$accessRepository = new AccessRepository($_em);
$fieldRepository = new FieldRepository($_em);
$privilegeRepository = new PrivilegeRepository($_em);
$roleRepository = new RoleRepository($_em);


$accessRepositoryTest = new AccessRepositoryTest($_em);
$accessRepositoryTest->runTests();

$fieldRepositoryTest = new FieldRepositoryTest($_em);
$fieldRepositoryTest->runTests();

$privilegeRepositoryTest = new PrivilegeRepositoryTest($_em, $fieldRepository);
$privilegeRepositoryTest->runTests();

$roleRepositoryTest = new RoleRepositoryTest($_em, $privilegeRepository);
$roleRepositoryTest->runTests();


$accessQueryTest = new AccessQueryTest($_em, $accessRepository, $privilegeRepository);
$accessQueryTest->runTests();

$fieldQueryTest = new FieldQueryTest($_em, $fieldRepository, $privilegeRepository);
$fieldQueryTest->runTests();

$privilegeQueryTest = new PrivilegeQueryTest($_em, $fieldRepository, $privilegeRepository, $roleRepository, $accessRepository);
$privilegeQueryTest->runTests();

$roleQueryTest = new RoleQueryTest($_em, $privilegeRepository, $roleRepository);
$roleQueryTest->runTests();
