<?php

return [
    /* common */

    'main' => 'Main page',
    'authorization' => 'Authorization',
    'start' => 'Start',  
    'access' => 'Access',
    'accesses' => 'Accesses',
    'field' => 'Field',
    'fields' => 'Fields',
    'privilege' =>  'Privilege',
    'privileges' => 'Privileges',
    'role' => 'Role',
    'roles' => 'Roles',
    'level' => 'Level',
    'ref' => 'Reference',
    'info' => 'Description',
    'route' => 'Route',
    
    'cancel' => 'Cancel',
    'add' => 'Add',
    'confirm' => 'Confirm',
    'modify' => 'Modify',
    'update' => 'Update',
    'delete' => 'Delete',
    'choose' => 'Choose...',
    'back' => 'Back',

    'access_level_help' => 'Enter integer value of access level',
    'access_ref_help' => 'Enter short access level reference',
    'access_info_help' => 'Enter access level description',
    'access_help' => 'Choose access level',
    
    'field_ref_help' => 'Enter database table field name',

    'privilege_ref_help' => 'Enter short privilege reference',
    'privilege_route_help' => 'Enter privilege related route',
    'privilege_info_help' => 'Enter privilege description',

    'role_ref_help' => 'Enter short role reference',
    'role_info_help' => 'Enter role description',
    
    /* autho.access.add */

    'form_access_add_title' => 'Add new access',

    'db_access_add_error' => 'One or both (level or reference) values is/are already in use.',
    
    /* 'autho.access.show' */
    
    'content_accesses_show_title' => 'Accesses',
    
    /* autho.access.edit */

    'form_access_edit_title' => 'Modify access',

    'db_access_edit_error' => 'One or both (level or reference) values is/are already in use.',

    /* autho.access.delete */

    'form_access_delete_title' => 'Delete access',
    
    'access_delete_warning' => 'Your are about to delete an access. Please confirm.',
    
    /* autho.field.add */

    'form_field_add_title' => 'Add new field',

    'db_field_add_error' => 'Field name reference is already in use.',

    /* 'autho.fields.show' */

    'content_fields_show_title' => 'Fields',

    /* autho.field.edit */

    'form_field_edit_title' => 'Modify field',

    'db_field_edit_error' => 'Field name reference is already in use.',

    /* autho.field.delete */

    'form_field_delete_title' => 'Delete field',

    'field_delete_warning' => 'Your are about to delete a field. Please confirm.',

    /* autho.privilege.add */

    'form_privilege_add_title' => 'Add new privilege',

    'db_privilege_add_error' => 'Privilege name reference is already in use.',

    /* 'autho.privileges.show' */

    'content_privileges_show_title' => 'Privileges',

    /* autho.privilege.edit */

    'form_privilege_edit_title' => 'Modify privilege',

    'db_privilege_edit_error' => 'Privilege name reference is already in use.',

    /* autho.privilege.delete */

    'form_privilege_delete_title' => 'Delete privilege',

    'privilege_delete_warning' => 'Your are about to delete a privilege. Please confirm.',

    /* autho.role.add */

    'form_role_add_title' => 'Add new role',

    'db_role_add_error' => 'Role reference is already in use.',

    /* 'autho.roles.show' */

    'content_roles_show_title' => 'Roles',

    /* autho.role.edit */

    'form_role_edit_title' => 'Modify role',

    'db_role_edit_error' => 'Role reference is already in use.',

    /* autho.role.delete */

    'form_role_delete_title' => 'Delete role',

    'role_delete_warning' => 'Your are about to delete a role. Please confirm.',

    /* autho.role.privileges.edit */

    'form_role_privileges_edit_title' => 'Role privileges',

    /* autho.privilege.roles.edit */

    'form_privilege_roles_edit_title' => 'Privilege roles',

    /* autho.privilege.fields.edit */

    'form_privilege_fields_edit_title' => 'Privilege fields',

    /* autho.field.privileges.edit */

    'form_field_privileges_edit_title' => 'Field privileges',

    /* autho.access.privileges.edit */

    'form_access_privileges_edit_title' => 'Access privileges',
    
    /* autho.role.privileges.delete */

    'form_role_privileges_delete_title' => 'Delete privileges relations of a role.',
    'role_privileges_delete_warning' => 'Your are about to delete all privileges relations of a role. Please confirm.',

    /* autho.privilege.roles.delete */

    'form_privilege_roles_delete_title' => 'Delete roles relations of a privilege.',
    'privilege_roles_delete_warning' => 'Your are about to delete all roles relations of a privilege. Please confirm.',

    /* autho.privilege.fields.delete */

    'form_privilege_fields_delete_title' => 'Delete fields relations of a privilege.',
    'privilege_fields_delete_warning' => 'Your are about to delete all fields relations of a privilege. Please confirm.',

    /* autho.field.privileges.delete */

    'form_field_privileges_delete_title' => 'Delete privileges relations of a field.',
    'field_privileges_delete_warning' => 'Your are about to delete all privileges relations of a field. Please confirm.',

    /* autho.access.privileges.delete */

    'form_access_privileges_delete_title' => 'Delete privileges relations of an access.',
    'access_privileges_delete_warning' => 'Your are about to delete all privileges relations of an access. Please confirm.',
];
