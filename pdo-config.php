<?php

return [
    'limit' => 10,
    'host' => 'localhost',
    'dbName' => 'authorization',
    'charset' => 'utf8',
    'user' => 'root',
    'password' => 'root',
    'options' => [
        PDO::ATTR_PERSISTENT         => true,
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_WARNING, //\PDO::ERRMODE_SILENT, \PDO::ERRMODE_WARNING, \PDO::ERRMODE_EXCEPTION
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_CLASS,
        PDO::ATTR_EMULATE_PREPARES   => false,
        PDO::MYSQL_ATTR_FOUND_ROWS   => false, // true -> matched, false -> affected
    ],
];
