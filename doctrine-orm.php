<?php

declare(strict_types=1);

use Paneric\Doctrine\EntityManagerBuilder;

$entityManagerBuilder = new EntityManagerBuilder();

return $entityManagerBuilder->build(require 'orm-config.php');
