<?php

declare(strict_types=1);

namespace Paneric\Authorization\PDO\Repository;

use Paneric\Authorization\DTO\FieldDTO;
use Paneric\Authorization\Interfaces\FieldQueryInterface;
use Paneric\Authorization\Interfaces\RepositoryInterface;
use Paneric\PdoWrapper\Manager;
use PDO;

class FieldRepository extends PDORepository implements RepositoryInterface
{
    protected $fieldQuery;


    public function __construct(Manager $manager, FieldQueryInterface $fieldQuery)
    {
        parent::__construct($manager);

        $this->fieldQuery = $fieldQuery;

        $this->table = 'fields';
        $this->dtoClass = FieldDTO::class;
        $this->fetchMode = PDO::FETCH_CLASS;
    }


    public function findOneById(int $id): object// override of Paneric\PdoWrapper\Repository
    {
        $this->adaptManager();

        $queryBuilder = $this->manager->getQueryBuilder();
        $queryBuilder->select($this->table)
            ->where(['id' => $id]);

        $this->manager->beginTransaction();

        $stmt = $this->manager->setStmt(
            $queryBuilder->getQuery(),
            ['id' => $id]
        );

        $field = $stmt->fetch();

        if ($field === false){
            $this->manager->rollBack();

            return null;
        }

        $fieldId = $field->getId();

        $privileges = $this->fieldQuery->findPrivileges($fieldId);

        $this->manager->commit();

        $field->updatePrivileges($privileges);

        return $field;
    }
}
