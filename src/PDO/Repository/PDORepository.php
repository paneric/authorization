<?php

declare(strict_types=1);

namespace Paneric\Authorization\PDO\Repository;

use Exception;
use Paneric\Authorization\Interfaces\HydratorInterface;
use Paneric\PdoWrapper\Manager;
use Paneric\PdoWrapper\Repository;

class PDORepository extends Repository
{
    public function findOneById(int $id): ?object
    {
        $this->adaptManager();

        $queryBuilder = $this->manager->getQueryBuilder();
        $queryBuilder->select($this->table)
            ->where(['id' => $id]);

        $stmt = $this->manager->setStmt(
            $queryBuilder->getQuery(),
            ['id' => $id]
        );

        $field = $stmt->fetch();

        if ($field === false){

            return null;
        }

        return $field;
    }

    public function findOneByRef(string $ref): ?object
    {
        $this->adaptManager();

        $queryBuilder = $this->manager->getQueryBuilder();
        $queryBuilder->select($this->table)
            ->where(['ref' => $ref]);

        $stmt = $this->manager->setStmt(
            $queryBuilder->getQuery(),
            ['ref' => $ref]
        );

        $object = $stmt->fetch();

        if ($object === false){

            return null;
        }

        return $object;
    }

    public function findByEnhanced(array $criteria, string $operator = 'AND', int $id = null): ?array
    {
        $this->adaptManager();

        if ($id === null) {
            $queryBuilder = $this->manager->getQueryBuilder();
            $queryBuilder->select($this->table)
                ->where($criteria, $operator);

            $stmt = $this->manager->setStmt(
                $queryBuilder->getQuery(),
                $criteria
            );
        }

        if ($id !== null) {
            $queryBuilder = $this->manager->getQueryBuilder();
            $queryBuilder->select($this->table)
                ->where($criteria, $operator)
                ->whereIn(['id0' => $id], 'id', 'NOT', 'AND');

            $stmt = $this->manager->setStmt(
                $queryBuilder->getQuery(),
                array_merge($criteria, ['id0' => $id])
            );
        }

        $array = $stmt->fetchAll();

        if ($array === []) {

            return null;
        }

        return $array;
    }

    public function findByIds(array $ids): array
    {
        $this->adaptManager();

        return $this->manager->findBySame(['id' => $ids]);
    }

    public function create(HydratorInterface $hydrator): ?array
    {
        try {
            $this->manager->setTable($this->table);

            $this->manager->create($hydrator->convert());
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return null;
    }

    public function update(int $id, HydratorInterface $hydrator): ?array
    {
        try {
            $this->manager->setTable($this->table);

            $this->manager->update($hydrator->convert(), ['id' => $id]);
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return null;
    }

    public function remove(int $id): ?array
    {
        try {
            $this->manager->setTable($this->table);

            $this->manager->delete(['id' => $id]);
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return null;
    }
}
