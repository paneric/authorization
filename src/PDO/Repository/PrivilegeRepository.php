<?php

declare(strict_types=1);

namespace Paneric\Authorization\PDO\Repository;

use Paneric\Authorization\DTO\PrivilegeDTO;
use Paneric\Authorization\Interfaces\PrivilegeQueryInterface;
use Paneric\Authorization\Interfaces\RepositoryInterface;
use Paneric\PdoWrapper\Manager;
use PDO;

class PrivilegeRepository extends PDORepository implements RepositoryInterface
{
    protected $privilegeQuery;


    public function __construct(Manager $manager, PrivilegeQueryInterface $privilegeQuery)
    {
        parent::__construct($manager);

        $this->privilegeQuery = $privilegeQuery;

        $this->table = 'privileges';
        $this->dtoClass = PrivilegeDTO::class;
        $this->fetchMode = PDO::FETCH_CLASS;
    }


    public function findOneById(int $id): object// override of Paneric\PdoWrapper\Repository
    {
        $this->adaptManager();

        $queryBuilder = $this->manager->getQueryBuilder();
        $queryBuilder->select($this->table)
            ->where(['id' => $id]);

        $this->manager->beginTransaction();

        $stmt = $this->manager->setStmt(
            $queryBuilder->getQuery(),
            ['id' => $id]
        );

        $privilege = $stmt->fetch();

        if ($privilege === false){
            $this->manager->rollBack();

            return null;
        }

        $privilegeId = $privilege->getId();

        $roles = $this->privilegeQuery->findRoles($privilegeId);
        $fields = $this->privilegeQuery->findFields($privilegeId);
        $access = $this->privilegeQuery->findAccess($privilegeId);

        $this->manager->commit();

        $privilege->updateRoles($roles);
        $privilege->updateFields($fields);
        $privilege->updateAccess($access);

        return $privilege;
    }

    public function findOneByRef(string $ref): ?object// override of Paneric\PdoWrapper\Repository
    {
        $this->adaptManager();

        $queryBuilder = $this->manager->getQueryBuilder();
        $queryBuilder->select($this->table)
            ->where(['ref' => $ref]);

        $this->manager->beginTransaction();

        $stmt = $this->manager->setStmt(
            $queryBuilder->getQuery(),
            ['ref' => $ref]
        );

        $privilege = $stmt->fetch();

        if ($privilege === false){
            $this->manager->rollBack();

            return null;
        }

        $privilegeId = $privilege->getId();

        $roles = $this->privilegeQuery->findRoles($privilegeId);
        $fields = $this->privilegeQuery->findFields($privilegeId);
        $access = $this->privilegeQuery->findAccess($privilegeId);

        $this->manager->commit();

        $privilege->updateRoles($roles);
        $privilege->updateFields($fields);
        $privilege->updateAccess($access);

        return $privilege;
    }
}
