<?php

declare(strict_types=1);

namespace Paneric\Authorization\PDO\Repository;

use Paneric\Authorization\DTO\AccessDTO;
use Paneric\Authorization\Interfaces\RepositoryInterface;
use Paneric\PdoWrapper\Manager;
use PDO;

class AccessRepository extends PDORepository implements RepositoryInterface
{
    public function __construct(Manager $manager)
    {
        parent::__construct($manager);

        $this->table = 'accesses';
        $this->dtoClass = AccessDTO::class;
        $this->fetchMode = PDO::FETCH_CLASS;
    }
}
