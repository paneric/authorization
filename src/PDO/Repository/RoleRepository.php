<?php

declare(strict_types=1);

namespace Paneric\Authorization\PDO\Repository;

use Paneric\Authorization\DTO\RoleDTO;
use Paneric\Authorization\Interfaces\RoleQueryInterface;
use Paneric\Authorization\Interfaces\RepositoryInterface;
use Paneric\PdoWrapper\Manager;
use PDO;

class RoleRepository extends PDORepository implements RepositoryInterface
{
    protected $roleQuery;


    public function __construct(Manager $manager, RoleQueryInterface $roleQuery)
    {
        parent::__construct($manager);

        $this->roleQuery = $roleQuery;

        $this->table = 'roles';
        $this->dtoClass = RoleDTO::class;
        $this->fetchMode = PDO::FETCH_CLASS;
    }


    public function findOneById(int $id): ?object// override of Paneric\PdoWrapper\Repository
    {
        $this->adaptManager();

        $queryBuilder = $this->manager->getQueryBuilder();
        $queryBuilder->select($this->table)
            ->where(['id' => $id]);

        $this->manager->beginTransaction();

        $stmt = $this->manager->setStmt(
            $queryBuilder->getQuery(),
            ['id' => $id]
        );

        $role = $stmt->fetch();

        if ($role === false){
            $this->manager->rollBack();

            return null;
        }

        $privileges = $this->roleQuery->findPrivileges($role->getId());

        $this->manager->commit();

        $role->updatePrivileges($privileges);

        return $role;
    }
}
