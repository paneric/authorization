<?php

declare(strict_types=1);

namespace Paneric\Authorization\PDO\Query;

use Paneric\Authorization\DTO\PrivilegeDTO;
use Paneric\Authorization\Interfaces\FieldQueryInterface;
use Paneric\PdoWrapper\Manager;
use PDO;

class FieldQuery extends AbstractQuery implements FieldQueryInterface
{
    protected $privilegesQuerySelect = '
            SELECT pt.* 
            FROM privileges_fields pft
            LEFT JOIN privileges pt ON pft.privilege_id = pt.id
            WHERE pft.field_id = :id
        ';


    public function findPrivileges(int $fieldId): array
    {
        return $this->findRelated(
            $fieldId,
            $this->privilegesFieldsTable,
            PrivilegeDTO::class,
            PDO::FETCH_CLASS,
            $this->privilegesQuerySelect
        );
    }

    public function updatePrivileges(int $fieldId, array $privilegesIds): ?array
    {
        return $this->updateRelated(
            $fieldId,
            $privilegesIds,
            $this->privilegesFieldsTable,
            'field_id',
            'privilege_id'
        );
    }

    public function removePrivileges(int $fieldId): ?array
    {
        return $this->removeRelated($fieldId, $this->privilegesFieldsTable, 'field_id');
    }
}
