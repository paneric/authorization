<?php

declare(strict_types=1);

namespace Paneric\Authorization\PDO\Query;

use Exception;
use Paneric\Authorization\DTO\PrivilegeDTO;
use Paneric\Authorization\Interfaces\AccessQueryInterface;
use Paneric\PdoWrapper\Manager;
use PDO;

class AccessQuery extends AbstractQuery implements AccessQueryInterface
{
    protected $privilegesQuerySelect = '
            SELECT pt.* 
            FROM privileges pt
            LEFT JOIN accesses at ON pt.access_id = at.id
            WHERE pt.access_id = :id
        ';


    public function findPrivileges(int $accessId): array
    {
        return $this->findRelated(
            $accessId,
            $this->privilegesTable,
            PrivilegeDTO::class,
            PDO::FETCH_CLASS,
            $this->privilegesQuerySelect
        );
    }

    public function updatePrivileges(int $accessId, array $privilegesIds): ?array
    {
        try {
            $this->manager->setTable($this->privilegesTable);

            $this->manager->beginTransaction();

            $query = 'UPDATE `privileges` SET `access_id`=NULL WHERE access_id=:access_id';
            $this->manager->setStmt($query, ['access_id' => $accessId]);

            $this->manager->updateSame(['access_id' => $accessId], $privilegesIds, 'id');

            $this->manager->commit();
        } catch (Exception $e) {
            $this->manager->rollBack();

            echo $e->getMessage();
        }

        return null;
    }

    public function removePrivileges(int $accessId): ?array
    {
        try {
            $this->manager->setTable($this->privilegesTable);

            $query = 'UPDATE `privileges` SET `access_id`=NULL WHERE access_id=:access_id';
            $this->manager->setStmt($query, ['access_id' => $accessId]);
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return null;
    }
}
