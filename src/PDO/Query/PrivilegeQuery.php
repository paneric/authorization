<?php

declare(strict_types=1);

namespace Paneric\Authorization\PDO\Query;

use Exception;
use Paneric\Authorization\DTO\AccessDTO;
use Paneric\Authorization\DTO\FieldDTO;
use Paneric\Authorization\DTO\RoleDTO;
use Paneric\Authorization\Interfaces\FieldQueryInterface;
use Paneric\Authorization\Interfaces\PrivilegeQueryInterface;
use Paneric\PdoWrapper\Manager;
use PDO;

class PrivilegeQuery extends AbstractQuery implements PrivilegeQueryInterface
{
    protected $fieldQuery;
    protected $fieldsQuerySelect = '
            SELECT ft.* 
            FROM privileges_fields pft
            LEFT JOIN fields ft ON pft.field_id = ft.id
            WHERE pft.privilege_id = :id
        ';
    protected $roleQuerySelect = '
            SELECT rt.* 
            FROM roles_privileges rpt
            LEFT JOIN roles rt ON rpt.role_id = rt.id
            WHERE rpt.privilege_id = :id
        ';


    public function __construct(Manager $manager, FieldQueryInterface $fieldQuery)
    {
        parent::__construct($manager);

        $this->fieldQuery = $fieldQuery;
    }


    public function findFieldById(int $privilegeId, int $fieldId): ?object
    {
        $this->manager->setTable($this->privilegesFieldsTable);
        $this->manager->setDTOClass(FieldDTO::class);
        $this->manager->setFetchMode(PDO::FETCH_CLASS);

        $query = '
            SELECT ft.* 
            FROM privileges_fields pft
            LEFT JOIN fields ft ON pft.field_id = ft.id
            WHERE pft.privilege_id = :idp AND pft.field_id = :idf 
        ';

        $stmt = $this->manager->setStmt($query, ['idp' => $privilegeId, 'idf' => $fieldId]);

        $field = $stmt->fetch();

        if ($field === false) {
            return null;
        }

        return $field;
    }


    public function findFields(int $privilegeId): array
    {
        return $this->findRelated(
            $privilegeId,
            $this->privilegesFieldsTable,
            FieldDTO::class,
            PDO::FETCH_CLASS,
            $this->fieldsQuerySelect
        );
    }

    public function updateFields(int $privilegeId, array $fieldsIds): ?array
    {
        return $this->updateRelated(
            $privilegeId,
            $fieldsIds,
            $this->privilegesFieldsTable,
            'privilege_id',
            'field_id'
        );
    }

    public function removeFields(int $privilegeId): ?array
    {
        return $this->removeRelated($privilegeId, $this->privilegesFieldsTable, 'privilege_id');
    }


    public function findRoles(int $privilegeId): array
    {
        return $this->findRelated(
            $privilegeId,
            $this->rolesPrivilegesTable,
            RoleDTO::class,
            PDO::FETCH_CLASS,
            $this->roleQuerySelect
        );
    }

    public function updateRoles(int $privilegeId, array $rolesIds): ?array
    {
        return $this->updateRelated(
            $privilegeId,
            $rolesIds,
            $this->rolesPrivilegesTable,
            'privilege_id',
            'role_id'
        );
    }

    public function removeRoles(int $privilegeId): ?array
    {
        return $this->removeRelated($privilegeId, $this->rolesPrivilegesTable, 'privilege_id');
    }


    public function findAccess(int $privilegeId): ?object
    {
        $this->manager->setTable($this->privilegesTable);
        $this->manager->setDTOClass(AccessDTO::class);
        $this->manager->setFetchMode(PDO::FETCH_CLASS);

        $query = '
            SELECT at.* 
            FROM privileges pt
            LEFT JOIN accesses at ON pt.access_id = at.id
            WHERE pt.id = :id
        ';

        $stmt = $this->manager->setStmt($query, ['id' => $privilegeId]);

        $access = $stmt->fetch();

        if ($access === false){
            return null;
        }

        if ($access->getId() === null){
            return null;
        }

        return $access;
    }

    public function setAccess(int $privilegeId, int $accessId): ?array
    {
        try {
            $this->manager->setTable($this->privilegesTable);
            $this->manager->update(['access_id' => $accessId], ['id' => $privilegeId]);
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return null;
    }

    public function removeAccess(int $privilegeId): ?array
    {
        try {
            $this->manager->setTable($this->privilegesTable);
            $this->manager->update(['access_id' => null], ['id' => $privilegeId]);
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return null;
    }
}
