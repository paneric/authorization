<?php

declare(strict_types=1);

namespace Paneric\Authorization\PDO\Query;

use Paneric\Authorization\DTO\PrivilegeDTO;
use Paneric\Authorization\Interfaces\PrivilegeQueryInterface;
use Paneric\Authorization\Interfaces\RoleQueryInterface;
use Paneric\PdoWrapper\Manager;
use PDO;

class RoleQuery extends AbstractQuery implements RoleQueryInterface
{
    protected $privilegeQuery;
    protected $privilegesQuerySelect = '
            SELECT pt.* 
            FROM roles_privileges rpt
            LEFT JOIN privileges pt ON rpt.privilege_id = pt.id
            WHERE rpt.role_id = :id
        ';

    public function __construct(Manager $manager, PrivilegeQueryInterface $privilegeQuery)
    {
        parent::__construct($manager);

        $this->privilegeQuery = $privilegeQuery;
    }


    public function findPrivilegeById(int $roleId, int $privilegeId): ?object
    {
        $this->manager->setTable($this->rolesPrivilegesTable);
        $this->manager->setDTOClass(PrivilegeDTO::class);
        $this->manager->setFetchMode(PDO::FETCH_CLASS);

        $query = '
            SELECT pt.* 
            FROM roles_privileges rpt
            LEFT JOIN privileges pt ON rpt.privilege_id = pt.id
            WHERE rpt.role_id = :idr AND rpt.privilege_id = :idp
        ';

        $stmt = $this->manager->setStmt($query, ['idr' => $roleId, 'idp' => $privilegeId]);

        $privilege = $stmt->fetch();

        if ($privilege === false) {
            return null;
        }

        return $privilege;
    }

    public function findPrivilegeByRoute(int $roleId, string $route): ?object
    {
        $this->manager->setTable($this->rolesPrivilegesTable);
        $this->manager->setDTOClass(PrivilegeDTO::class);
        $this->manager->setFetchMode(PDO::FETCH_CLASS);

        $query = '
                    SELECT p.*
                    FROM roles_privileges rp
                    LEFT JOIN roles r ON rp.role_id = r.id
                    LEFT JOIN privileges p ON rp.privilege_id = p.id
                    WHERE r.id = :role_id AND p.route = :route
        		';

        $this->manager->beginTransaction();

        $stmt = $this->manager->setStmt($query, ['role_id' => $roleId, 'route' => $route]);

        $privilege = $stmt->fetch();

        if ($privilege === false) {
            $this->manager->rollBack();

            return null;
        }

        $privilege->updateFields(
            $this->privilegeQuery->findFields($privilege->getId())
        );

        $this->manager->commit();

        return $privilege;
    }


    public function findPrivileges(int $roleId): array
    {
        return $this->findRelated(
            $roleId,
            $this->rolesPrivilegesTable,
            PrivilegeDTO::class,
            PDO::FETCH_CLASS,
            $this->privilegesQuerySelect
        );
    }

    public function updatePrivileges(int $roleId, array $privilegesIds): ?array
    {
        return $this->updateRelated(
            $roleId,
            $privilegesIds,
            $this->rolesPrivilegesTable,
            'role_id',
            'privilege_id'
        );
    }

    public function removePrivileges(int $roleId): ?array
    {
        return $this->removeRelated($roleId, $this->rolesPrivilegesTable, 'role_id');
    }
}
