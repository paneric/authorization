<?php

declare(strict_types=1);

namespace Paneric\Authorization\PDO\Query;

use Exception;
use Paneric\PdoWrapper\Manager;

abstract class AbstractQuery
{
    protected $manager;

    protected $privilegesFieldsTable = 'privileges_fields';
    protected $rolesPrivilegesTable = 'roles_privileges';
    protected $privilegesTable = 'privileges';


    public function __construct(Manager $manager)
    {
        $this->manager = $manager;
    }


    protected function findRelated(
        int $leadingId,
        string $queryTable,
        string $relatedClass,
        int $fetchMethod,
        string $querySelect
    ): array {
        $this->manager->setTable($queryTable);
        $this->manager->setDTOClass($relatedClass);
        $this->manager->setFetchMode($fetchMethod);

        $stmt = $this->manager->setStmt($querySelect, ['id' => $leadingId]);

        return $stmt->fetchAll();
    }

    protected function addRelated(
        int $leadingId,
        array $relatedIds,
        $queryTable,
        $leadingIdName,
        $relatedIdName
    ): ?array {
        try {
            $this->manager->setTable($queryTable);

            $this->manager->createMultiple(
                $this->setIdsFieldSets(
                    $leadingIdName,
                    $relatedIdName,
                    $leadingId,
                    $relatedIds
                )
            );
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return null;
    }

    protected function updateRelated(
        int $leadingId,
        array $relatedIds,
        string $queryTable,
        string $leadingIdName,
        string $relatedIdName
    ): ?array {
        try {
            $this->manager->setTable($queryTable);

            $this->manager->beginTransaction();

            $this->manager->delete([$leadingIdName => $leadingId]);

            $this->manager->createMultiple(
                $this->setIdsFieldSets(
                    $leadingIdName,
                    $relatedIdName,
                    $leadingId,
                    $relatedIds
                )
            );

            $this->manager->commit();
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return null;
    }

    protected function removeRelated(
        int $leadingId,
        string $queryTable,
        string $leadingIdName
    ): ?array {
        try {
            $this->manager->setTable($queryTable);

            $this->manager->delete([$leadingIdName => $leadingId]);
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return null;
    }


    protected function setIdsFieldSets(
        string $leadingId,
        string $relatedId,
        int $leadingValue,
        array $relatedValues
    ): array {
        $relatedValesNumber = count($relatedValues);

        $relatedValues = array_values($relatedValues);

        $idsSets = [];
        for ($i = 0; $i < $relatedValesNumber; $i++) {
            $idsSets[$i][$leadingId] = $leadingValue;
            $idsSets[$i][$relatedId] = $relatedValues[$i];
        }

        return $idsSets;
    }
}
