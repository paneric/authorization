<?php

declare(strict_types=1);

namespace Paneric\Authorization\Controller\Client;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

trait PrivilegeControllerTrait
{
    public function showPrivileges(Response $response, int $id): Response // OK
    {
        return $this->render(
            $response,
            '@module/' . $this->entityName . '/show_privileges.html.twig',
            ['privileges' => $this->service->getPrivileges($id)]
        );
    }

    public function editPrivileges(Request $request, Response $response, int $id): Response // OK
    {
        $result = $this->service->updatePrivileges($id, $request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/autho/' . $this->adaptEntityName() . '/show',
                200
            );
        }

        if (is_array($result)) {
            return $this->render(
                $response,
                '@module/privilege/edit_multiple.html.twig',
                $result
            );
        }

        return $this->redirect(
            $response,
            '/autho/' . $this->entityName . '/' . $id . '/privileges/delete',
            200
        );
    }

    public function deletePrivileges(Request $request, Response $response, int $id): Response // OK
    {
        $result = $this->service->deletePrivileges($request, $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/autho/' . $this->adaptEntityName() . '/show',
                200
            );
        }

        return $this->render(
            $response,
            '@module/' . $this->entityName . '/delete_relations.html.twig',
            ['id' => $id, 'relations' => 'privileges']
        );
    }
}
