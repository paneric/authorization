<?php

declare(strict_types=1);

namespace Paneric\Authorization\Controller\Client;

use Psr\Http\Message\ResponseInterface as Response;
use Paneric\Authorization\Service\RoleService;
use Twig\Environment as Twig;

class RoleController extends ClientController
{
    use PrivilegeControllerTrait;

    public function __construct(RoleService $service, Twig $twig)
    {
        parent::__construct($service, $twig);

        $this->entityName = 'role';
    }


    public function showPrivilegeById(Response $response, int $id, int $idp): ?object// (GET) /role/{id}/privilege/{idp}
    {
        return $this->render(
            $response,
            'to reconsider',
            ['privilege' => $this->service->getPrivilegeById($id, $idp)]
        );
    }
}
