<?php

declare(strict_types=1);

namespace Paneric\Authorization\Controller\Client;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

trait FieldControllerTrait
{
    public function showFields(Response $response, int $id): Response // (GET) /privilege/{id}/fields
    {
        return $this->render(
            $response,
            'to reconsider',
            ['fields' => $this->service->getFields($id)]
        );
    }

    public function editFields(Request $request, Response $response, int $id): Response // (UPDATE) /privilege/{id}/fields/update
    {
        $result = $this->service->updateFields($id, $request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/autho/' . $this->adaptEntityName() . '/show',
                200
            );
        }

        if (is_array($result)) {
            return $this->render(
                $response,
                '@module/field/edit_multiple.html.twig',
                $result
            );
        }

        return $this->redirect(
            $response,
            '/autho/' . $this->entityName . '/' . $id . '/fields/delete',
            200
        );
    }

    public function deleteFields(Request $request, Response $response, int $id): Response // (DELETE) /privilege/{id}/fields/delete
    {
        $result = $this->service->deleteFields($request, $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/autho/' . $this->adaptEntityName() . '/show',
                200
            );
        }

        return $this->render(
            $response,
            '@module/' . $this->entityName . '/delete_relations.html.twig',
            ['id' => $id, 'relations' => 'fields']
        );
    }
}
