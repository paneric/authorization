<?php

declare(strict_types=1);

namespace Paneric\Authorization\Controller\Client;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

trait RoleControllerTrait
{
    public function showRoles(Response $response, int $id): Response // (GET) /privilege/{id}/fields
    {
        return $this->render(
            $response,
            'to reconsider',
            ['roles' => $this->service->getRoles($id)]
        );
    }

    public function editRoles(Request $request, Response $response, int $id): Response // (UPDATE) /privilege/{id}/fields/update
    {
        $result = $this->service->updateRoles($id, $request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/autho/' . $this->adaptEntityName() . '/show',
                301
            );
        }

        if (is_array($result)) {
            return $this->render(
                $response,
                '@module/role/edit_multiple.html.twig',
                $result
            );
        }

        return $this->redirect(
            $response,
            '/autho/' . $this->entityName . '/' . $id . '/roles/delete',
            200
        );
    }

    public function deleteRoles(Request $request, Response $response, int $id): Response // (DELETE) /privilege/{id}/fields/delete
    {
        $result = $this->service->deleteRoles($request, $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/autho/' . $this->adaptEntityName() . '/show',
                200
            );
        }

        return $this->render(
            $response,
            '@module/' . $this->entityName . '/delete_relations.html.twig',
            ['id' => $id, 'relations' => 'roles']
        );
    }
}
