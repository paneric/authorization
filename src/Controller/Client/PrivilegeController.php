<?php

declare(strict_types=1);

namespace Paneric\Authorization\Controller\Client;

use Paneric\Authorization\Service\PrivilegeService;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Twig\Environment as Twig;

class PrivilegeController extends ClientController
{
    use FieldControllerTrait;
    use RoleControllerTrait;

    public function __construct(PrivilegeService $service, Twig $twig)
    {
        parent::__construct($service, $twig);

        $this->entityName = 'privilege';
    }


    public function showFieldById(Response $response, int $id, int $idf): ?object// (GET) /privilege/{id}/field/{idf}
    {
        return $this->render(
            $response,
            'to reconsider',
            ['field' => $this->service->getFieldById($id, $idf)]
        );
    }


    public function showAccess(Response $response, int $id): ?object// (GET) /privilege/{id}/access
    {
        return $this->render(
            $response,
            'to reconsider',
            ['access' => $this->service->getAccess($id)]
        );
    }

    public function setAccess(Request $request, Response $response, int $id): Response // (DELETE) /privilege/{id}/access/delete
    {
        if ($result = $this->service->setAccess($id, $request) === null) {
            return $this->redirect(
                $response,
                '/autho/privileges/show',
                301
            );
        }

        return $this->render(
            $response,
            'set.html.twig',
            (array) $result
        );
    }

    public function deleteAccess(Response $response, int $id): Response // (DELETE) /privilege/{id}/access/delete
    {
        $this->service->deleteAccess($id);

        return $this->redirect(
            $response,
            'to reconsider',
            301
        );
    }
}
