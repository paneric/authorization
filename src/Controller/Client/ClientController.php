<?php

declare(strict_types=1);

namespace Paneric\Authorization\Controller\Client;

use Paneric\Authorization\Interfaces\ServiceInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Twig\Environment as Twig;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class ClientController
{
    protected $service;
    protected $twig;
    protected $entityName = '';// testing purpose


    public function __construct(ServiceInterface $service, Twig $twig)
    {
        $this->service = $service;
        $this->twig = $twig;
    }


    public function showAll(Response $response): Response // (GET) /xxx
    {
        return $this->render(
            $response,
            '@module/' . $this->entityName . '/show_all.html.twig',
            $this->service->getAll()
        );
    }

    public function showOneById(Response $response, int $id): Response // (GET) /xxx/{id}
    {
        return $this->render(
            $response,
            '@module/' . $this->entityName . '/show.html.twig',
            [$this->entityName => $this->service->getOneById($id)]
        );
    }

    public function add(Request $request, Response $response): Response // (POST) /xxx
    {
        $result = $this->service->create($request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/autho/' . $this->adaptEntityName() . '/show',
                200
            );
        }

        return $this->render(
            $response,
            '@module/' . $this->entityName . '/add.html.twig',
            $result
        );
    }

    public function edit(Request $request, Response $response, int $id): Response // (POST) /xxx
    {
        $result = $this->service->update($id, $request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/autho/' . $this->adaptEntityName() . '/show',
                200
            );
        }

        return $this->render(
            $response,
            '@module/' . $this->entityName . '/edit.html.twig',
            $result
        );
    }

    public function delete(Request $request, Response $response, int $id): Response // (POST) /xxx
    {
        $result = $this->service->delete($request, $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/autho/' . $this->adaptEntityName() . '/show',
                200
            );
        }

        return $this->render(
            $response,
            '@module/' . $this->entityName . '/delete.html.twig',
            ['id' => $id]
        );
    }


    protected function render(Response $response, string $template, array $data = [], int $status = null): ?Response
    {
        try {
            $response->getBody()->write(
                $this->twig->render($template, $data)
            );

            $response = $response->withHeader('Content-Type', 'text/html;charset=utf-8');

            if ($status !== null) {
                return $response->withStatus($status);
            }

            return $response;
        } catch (LoaderError $e) {
            echo $e->getMessage();
        } catch (RuntimeError $e) {
            echo $e->getMessage();
        } catch (SyntaxError $e) {
            echo $e->getMessage();
        }

        return null;
    }

    public function redirect(Response $response, string $url, int $status = null): Response
    {
        $response = $response->withHeader('Location', $url);

        if ($status !== null) {
            return $response->withStatus($status);
        }

        return $response;
    }

    protected function adaptEntityName(): string
    {
        return substr($this->entityName, -1) === 's' ? $this->entityName . 'es' : $this->entityName . 's';
    }
}
