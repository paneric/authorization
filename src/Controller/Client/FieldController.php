<?php

declare(strict_types=1);

namespace Paneric\Authorization\Controller\Client;

use Paneric\Authorization\Service\FieldService;
use Twig\Environment as Twig;

class FieldController extends ClientController
{
    use PrivilegeControllerTrait;

    public function __construct(FieldService $service, Twig $twig)
    {
        parent::__construct($service, $twig);

        $this->entityName = 'field';
    }
}
