<?php

declare(strict_types=1);

namespace Paneric\Authorization\Controller\Client;

use Paneric\Authorization\Service\AccessService;
use Twig\Environment as Twig;

class AccessController extends ClientController
{
    use PrivilegeControllerTrait;

    public function __construct(AccessService $service, Twig $twig)
    {
        parent::__construct($service, $twig);

        $this->entityName = 'access';
    }
}
