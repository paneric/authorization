<?php

declare(strict_types=1);

namespace Paneric\Authorization\Controller\API;

use Paneric\Authorization\Service\RoleService;
use Psr\Http\Message\ResponseInterface as Response;

class RoleAPIController extends APIController
{
    use PrivilegeAPIControllerTrait;

    public function __construct(RoleService $service)
    {
        parent::__construct($service);
    }


    public function getPrivilegeById(Response $response, int $id, int $idp): ?object// (GET) /role/{id}/privilege/{idp}
    {
        $privilege = $this->service->getPrivilegeById($id, $idp);

        return $this->jsonResponse(
            $response,
            $privilege->serialize()
        );
    }
}
