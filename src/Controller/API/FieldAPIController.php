<?php

declare(strict_types=1);

namespace Paneric\Authorization\Controller\API;

use Paneric\Authorization\Service\FieldService;

class FieldAPIController extends APIController
{
    use PrivilegeAPIControllerTrait;


    public function __construct(FieldService $service)
    {
        parent::__construct($service);
    }
}
