<?php

declare(strict_types=1);

namespace Paneric\Authorization\Controller\API;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

trait FieldAPIControllerTrait
{
    public function getFields(Response $response, int $id): Response // (GET) /privilege/{id}/fields
    {
        return $this->jsonResponse(
            $response,
            $this->service->getFields($id)
        );
    }

    public function updateFields(Request $request, Response $response, int $id): Response // (UPDATE) /privilege/{id}/fields/update
    {
        return $this->jsonResponse(
            $response,
            $this->service->updateFields($id, $request),
            301
        );
    }

    public function deleteFields(Request $request, Response $response, int $id): Response // (DELETE) /privilege/{id}/fields/delete
    {
        return $this->jsonResponse(
            $response,
            $this->service->deleteFields($request, $id),
            301
        );
    }
}
