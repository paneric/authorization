<?php

declare(strict_types=1);

namespace Paneric\Authorization\Controller\API;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

trait PrivilegeAPIControllerTrait
{
    public function getPrivileges(Response $response, int $id): Response // (GET) /role/{id}/privileges
    {
        return $this->jsonResponse(
            $response,
            $this->service->getPrivileges($id)
        );
    }

    public function updatePrivileges(Request $request, Response $response, int $id): Response // (UPDATE) /role/{id}/privileges/update
    {
        return $this->jsonResponse(
            $response,
            $this->service->updatePrivileges($id, $request),
            301
        );
    }

    public function deletePrivileges(Request $request, Response $response, int $id): Response // (DELETE) /role/{id}/privileges/delete
    {
        return $this->jsonResponse(
            $response,
            $this->service->deletePrivileges($request, $id),
            301
        );
    }
}
