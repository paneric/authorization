<?php

declare(strict_types=1);

namespace Paneric\Authorization\Controller\API;

use Paneric\Authorization\Interfaces\ServiceInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use RuntimeException;

class APIController
{
    protected $service;


    public function __construct(ServiceInterface $service)
    {
        $this->service = $service;
    }


    public function getAll(Response $response): Response // (GET) /xxx
    {
        return $this->jsonResponse(
            $response,
            $this->service->getAll()
        );
    }

    public function getByIds(Request $request, Response $response): Response // (POST) /xxx
    {
        return $this->jsonResponse(
            $response,
            $this->service->getByIds($request)
        );
    }

    public function getOneById(Response $response, int $id): Response // (GET) /xxx/{id}
    {
        $serializable = $this->service->getOneById($id);

        return $this->jsonResponse(
            $response,
            $serializable->serialize()
        );
    }

    public function getOneByRef(Response $response, string $ref): Response // (GET) /xxx/{id}
    {
        $serializable = $this->service->getOneByRef($ref);

        return $this->jsonResponse(
            $response,
            $serializable->serialize()
        );
    }

    public function create(Request $request, Response $response): Response // (POST) /xxx
    {
        return $this->jsonResponse(
            $response,
            $this->service->create($request),
            201
        );
    }

    public function update(Request $request, Response $response, int $id): Response // (UPDATE) /xxx/{id}/update
    {
        return $this->jsonResponse(
            $response,
            $this->service->update($id, $request),
            301
        );
    }

    public function delete(Request $request, Response $response, int $id): Response  // (DELETE) /xxx/{id}/delete
    {
        return $this->jsonResponse(
            $response,
            $this->service->delete($request, $id)
        );
    }


    protected function jsonResponse(
        Response $response,
        array $data = null,
        int $status = null,
        int $encodingOptions = 0)
    : Response {
        $response->getBody()->write(
            $json = json_encode($data, JSON_THROW_ON_ERROR | $encodingOptions, 512)
        );

        if ($json === false) {
            throw new RuntimeException(json_last_error_msg(), json_last_error());
        }

        $response = $response->withHeader('Content-Type', 'application/json;charset=utf-8');

        if (isset($status)) {
            return $response->withStatus($status);
        }

        return $response;
    }
}
