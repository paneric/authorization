<?php

declare(strict_types=1);

namespace Paneric\Authorization\Controller\API;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

trait RoleAPIControllerTrait
{
    public function getRoles(Response $response, int $id): Response // (GET) /privilege/{id}/roles
    {
        return $this->jsonResponse(
            $response,
            $this->service->getRoles($id)
        );
    }

    public function updateRoles(Request $request, Response $response, int $id): Response // (UPDATE) /privilege/{id}/roles/update
    {
        return $this->jsonResponse(
            $response,
            $this->service->updateRoles($id, $request),
            301
        );
    }

    public function deleteRoles(Request $request, Response $response, int $id): Response // (DELETE) /privilege/{id}/roles/delete
    {
        return $this->jsonResponse(
            $response,
            $this->service->deleteRoles($request, $id),
            301
        );
    }
}
