<?php

declare(strict_types=1);

namespace Paneric\Authorization\Controller\API;

use Paneric\Authorization\Service\PrivilegeService;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class PrivilegeAPIController extends APIController
{
    use FieldAPIControllerTrait;
    use RoleAPIControllerTrait;

    public function __construct(PrivilegeService $service)
    {
        parent::__construct($service);
    }


    public function getFieldById(Response $response, int $id, int $idf): ?object// (GET) /privilege/{id}/field/{idf}
    {
        $field = $this->service->getFieldById($id, $idf);

        return $this->jsonResponse(
            $response,
            $field->serialize()
        );
    }


    public function getAccess(Response $response, int $id): ?object// (GET) /privilege/{id}/access
    {
        $access = $this->service->getAccess($id);

        return $this->jsonResponse(
            $response,
            $access->serialize()
        );
    }

    public function setAccess(Request $request, Response $response, int $id): Response // (DELETE) /privilege/{id}/access/delete
    {
        return $this->jsonResponse(
            $response,
            $this->service->setAccess($id, $request),
            301
        );
    }

    public function deleteAccess(Response $response, int $id): Response // (DELETE) /privilege/{id}/access/delete
    {
        return $this->jsonResponse(
            $response,
            $this->service->deleteAccess($id)
        );
    }
}
