<?php

declare(strict_types=1);

namespace Paneric\Authorization\Controller\API;

use Paneric\Authorization\Service\AccessService;

class AccessAPIController extends APIController
{
    use PrivilegeAPIControllerTrait;


    public function __construct(AccessService $service)
    {
        parent::__construct($service);
    }
}
