<?php

declare(strict_types=1);

namespace Paneric\Authorization\HTTP;

use Exception;
use GuzzleHttp\ClientInterface as GuzzleHttpClient;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Psr7\Request;
use Paneric\Authorization\Interfaces\HydratorInterface;
use Psr\Http\Message\ResponseInterface as Response;

abstract class HttpClient
{
    protected $guzzleHttpClient;
    protected $requestOptions = [];
    protected $routePattern = '';


    public function __construct(GuzzleHttpClient $guzzleHttpClient)
    {
        $this->guzzleHttpClient = $guzzleHttpClient;
    }


    public function setRequestOptions(array $requestOptions): void
    {
        $this->requestOptions = $requestOptions;
    }

    public function setRoutePattern(string $routePattern, string $placeholderMarker): string
    {
        $routePattern = explode('/', $routePattern);

        foreach ($routePattern as $item => $value) {
            if (strpos($value, $placeholderMarker) !== false) {
                $routePattern[$item] = '%s';
            }
        }

        $this->routePattern = implode('/', $routePattern);

        return $this->routePattern;//for testing purpose
    }


    protected function mergeRequestOptions(array $options): array
    {
        if ($this->requestOptions !== []) {
            return array_merge($this->requestOptions, $options);
        }

        return $options;
    }

    protected function setResponse(Request $request, int $expectedStatusCode = 200): ?Response
    {
        try {
            $response = $this->guzzleHttpClient->send($request);

            if ($message = $this->setExceptionMessage(
                    $response,
                    $expectedStatusCode,
                    'Content-Type',
                    'application/json'
                ) !== null) {
                throw new BadResponseException($message, $request);
            }

            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return null;
    }

    protected function setExceptionMessage(
        Response $response,
        int $expectedStatusCode,
        string $expectedHeader,
        string $expectedHeaderValue
    ): ?string {

        $statusMsg = '';
        $headerMsg = '';
        $headerValueMsg = '';

        if($expectedStatusCode !==  $response->getStatusCode()) {
            $statusMsg = sprintf(
                '%s%s',
                'Expected status code:' . $expectedStatusCode. "\n",
                'Response status code: ' . $response->getStatusCode() . "\n"
            );
        }

        if($hasExpectedHeader = $response->hasHeader($expectedHeader) === false) {
            $headerMsg = 'Missing header: ' . $expectedHeader . "\n";
        }

        if (
            $hasExpectedHeader === true &&
            $responseHeaderValue = $response->getHeader($expectedHeader) !== $expectedHeaderValue
        ) {
            $headerValueMsg = sprintf('%s%s',
                'Expected header ' . $expectedHeader . ' value: ' . $expectedHeaderValue . "\n",
                'Response header ' . $expectedHeader . ' value: ' . $responseHeaderValue . "\n"
            );
        }

        $message = $statusMsg . $headerMsg . $headerValueMsg;

        if (!empty($message)) {
            return sprintf(
                '%s%s',
                'Unexpected status code or(and) missing(incorrect) response header:',
                $message
            );
        }

        return null;
    }

    protected function hydrateObjectsAttributes(HydratorInterface $hydrator, array $array): array
    {
        foreach ($array as $key => $attributes) {
            if (is_array($attributes)) {
                $array[$key] = $hydrator->hydrate($attributes);
            }
        }

        return $array;
    }
}
