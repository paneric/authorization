<?php

declare(strict_types=1);

namespace Paneric\Authorization\HTTP\Query;

use GuzzleHttp\ClientInterface as HttpClient;
use Paneric\Authorization\DTO\PrivilegeDTO;
use Paneric\Authorization\Interfaces\FieldQueryInterface;

class FieldQuery extends HttpClientQuery implements FieldQueryInterface
{
    public function findPrivileges(int $fieldId): array
    {
        return $this->requestFindRelations($fieldId, new PrivilegeDTO());
    }

    public function updatePrivileges(int $fieldId, array $privilegesIds): ?array
    {
        return $this->requestUpdateRelations($fieldId, $privilegesIds);
    }

    public function removePrivileges(int $fieldId): ?array
    {
        return $this->requestRemoveRelations($fieldId);
    }
}
