<?php

declare(strict_types=1);

namespace Paneric\Authorization\HTTP\Query;

use GuzzleHttp\ClientInterface as HttpClient;
use GuzzleHttp\Psr7\Request;
use Paneric\Authorization\DTO\PrivilegeDTO;
use Paneric\Authorization\Interfaces\RoleQueryInterface;

class RoleQuery extends HttpClientQuery implements RoleQueryInterface
{
    public function findPrivilegeById(int $roleId, int $privilegeId): ?object
    {
        $request = new Request(
            'GET',
            sprintf($this->routePattern, $roleId, $privilegeId),
            $this->requestOptions
        );

        $response = $this->setResponse($request);

        $attributes = json_decode($response->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR);

        if ($attributes !== ['NULL']) {
            $privilege = new PrivilegeDTO();

            return $privilege->hydrate((array)$attributes);
        }

        return null;
    }

    public function findPrivilegeByRoute(int $roleId, string $route): ?object
    {
        $request = new Request(
            'GET',
            sprintf($this->routePattern, $roleId, $route),
            $this->requestOptions
        );

        $response = $this->setResponse($request);

        $attributes = json_decode($response->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR);
        if ($attributes !== ['NULL']) {
            $privilege = new PrivilegeDTO();

            return $privilege->hydrate((array)$attributes);
        }

        return null;
    }


    public function findPrivileges(int $roleId): array
    {
        return $this->requestFindRelations($roleId, new PrivilegeDTO());
    }

    public function updatePrivileges(int $roleId, array $privilegesIds): ?array
    {
        return $this->requestUpdateRelations($roleId, $privilegesIds);
    }

    public function removePrivileges(int $roleId): ?array
    {
        return $this->requestRemoveRelations($roleId);
    }
}
