<?php

declare(strict_types=1);

namespace Paneric\Authorization\HTTP\Query;

use Exception;
use GuzzleHttp\ClientInterface as GuzzleHttpClient;
use GuzzleHttp\Psr7\Request;
use Paneric\Authorization\HTTP\HttpClient;
use Paneric\Authorization\Interfaces\HydratorInterface;
use Psr\Http\Message\StreamInterface;

class HttpClientQuery extends HttpClient
{
    public function __construct(GuzzleHttpClient $guzzleHttpClient)
    {
        parent::__construct($guzzleHttpClient);

        $this->guzzleHttpClient = $guzzleHttpClient;
    }

    protected function requestFindRelations(int $leadingId, HydratorInterface $hydrator): array
    {
        $request = new Request(
            'GET',
            sprintf($this->routePattern, $leadingId),
            $this->requestOptions
        );

        $response = $this->setResponse($request);

        return $this->hydrateObjectsAttributes(
            $hydrator, json_decode($response->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR)
        );
    }

    protected function requestAddRelations(int $leadingId, array $relationsIds): ?array
    {
        $request = new Request(
            'POST',
            sprintf($this->routePattern, $leadingId),
            $this->mergeRequestOptions(['form_params' => $relationsIds])
        );

        $response = $this->setResponse($request, 301);

        return json_decode($response->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR);
    }

    protected function requestUpdateRelations(int $leadingId, array $relationsIds): ?array
    {
        try {
            $request = new Request(
                'UPDATE',
                sprintf($this->routePattern, $leadingId),
                $this->mergeRequestOptions(['form_params' => $relationsIds])
            );

            $response = $this->setResponse($request, 301);

            return json_decode($response->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR);
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }

        return null;
    }

    protected function requestRemoveRelations(int $leadingId): ?array
    {
        try {
            $request = new Request(
                'DELETE',
                sprintf($this->routePattern, $leadingId),
                $this->requestOptions
            );

            $response = $this->setResponse($request, 301);

            return json_decode($response->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR);
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }

        return null;
    }
}
