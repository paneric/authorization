<?php

declare(strict_types=1);

namespace Paneric\Authorization\HTTP\Query;

use GuzzleHttp\ClientInterface as HttpClient;
use Paneric\Authorization\DTO\PrivilegeDTO;
use Paneric\Authorization\Interfaces\AccessQueryInterface;

class AccessQuery extends HttpClientQuery implements AccessQueryInterface
{
    public function findPrivileges(int $accessId): array
    {
        return $this->requestFindRelations($accessId, new PrivilegeDTO());
    }

    public function updatePrivileges(int $accessId, array $privilegesIds): ?array
    {
        return $this->requestUpdateRelations($accessId, $privilegesIds);
    }

    public function removePrivileges(int $accessId): ?array
    {
        return $this->requestRemoveRelations($accessId);
    }
}
