<?php

declare(strict_types=1);

namespace Paneric\Authorization\HTTP\Query;

use GuzzleHttp\ClientInterface as HttpClient;
use GuzzleHttp\Psr7\Request;
use Paneric\Authorization\DTO\AccessDTO;
use Paneric\Authorization\DTO\FieldDTO;
use Paneric\Authorization\DTO\RoleDTO;
use Paneric\Authorization\Interfaces\PrivilegeQueryInterface;

class PrivilegeQuery extends HttpClientQuery implements PrivilegeQueryInterface
{
    public function findFieldById(int $privilegeId, int $fieldId): ?object
    {
        $request = new Request(
            'GET',
            sprintf($this->routePattern, $privilegeId, $fieldId),
            $this->requestOptions
        );

        $response = $this->setResponse($request);

        $attributes = json_decode($response->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR);
        if ($attributes !== ['NULL']) {
            $privilege = new FieldDTO();

            return $privilege->hydrate($attributes);
        }

        return null;
    }


    public function findFields(int $privilegeId): array
    {
        return $this->requestFindRelations($privilegeId, new FieldDTO());
    }

    public function updateFields(int $privilegeId, array $fieldsIds): ?array
    {
        return $this->requestUpdateRelations($privilegeId, $fieldsIds);
    }

    public function removeFields(int $privilegeId): ?array
    {
        return $this->requestRemoveRelations($privilegeId);
    }


    public function findRoles(int $privilegeId): array
    {
        return $this->requestFindRelations($privilegeId, new RoleDTO());
    }

    public function updateRoles(int $privilegeId, array $rolesIds): ?array
    {
        return $this->requestUpdateRelations($privilegeId, $rolesIds);
    }

    public function removeRoles(int $privilegeId): ?array
    {
        return $this->requestRemoveRelations($privilegeId);
    }


    public function findAccess(int $privilegeId): ?object
    {
        $request = new Request(
            'GET',
            sprintf($this->routePattern, $privilegeId),
            $this->requestOptions
        );

        $response = $this->setResponse($request);

        $attributes = json_decode($response->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR);
        if ($attributes !== ['NULL']) {
            $privilege = new AccessDTO();

            return $privilege->hydrate((array)$attributes);
        }

        return null;
    }

    public function setAccess(int $privilegeId, int $accessId): ?array
    {
        $request = new Request(
            'POST',
            sprintf($this->routePattern, $privilegeId, $accessId),
            $this->requestOptions
        );

        $response = $this->setResponse($request, 301);

        return json_decode($response->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR);
    }

    public function removeAccess(int $privilegeId): ?array
    {
        return $this->requestRemoveRelations($privilegeId);
    }
}
