<?php

declare(strict_types=1);

namespace Paneric\Authorization\HTTP\Repository;

use GuzzleHttp\ClientInterface as GuzzleHttpClient;
use GuzzleHttp\Psr7\Request;
use Paneric\Authorization\HTTP\HttpClient;
use Paneric\Authorization\Interfaces\HydratorInterface;

abstract class HttpClientRepository extends HttpClient
{
    public function __construct(GuzzleHttpClient $guzzleHttpClient)
    {
        parent::__construct($guzzleHttpClient);

        $this->guzzleHttpClient = $guzzleHttpClient;
    }


    protected function requestFindAll(HydratorInterface $hydrator)
    {
        $request = new Request(
            'GET',
            $this->routePattern,
            $this->requestOptions
        );

        $response = $this->setResponse($request);

        $attributes = json_decode($response->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR);
        return $this->hydrateObjectsAttributes($hydrator, $attributes);
    }

    protected function requestFindByIds(array $ids, HydratorInterface $hydrator): array  // (POST) /accesses
    {
        $request = new Request(
            'POST',
            $this->routePattern,
            $this->mergeRequestOptions(['form_params' => $ids])
        );

        $response = $this->setResponse($request);

        return $this->hydrateObjectsAttributes(
            $hydrator,
            json_decode($response->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR)
        );
    }

    protected function requestFindOneById(int $id, HydratorInterface $hydrator): ?object // (GET) /access/{id}
    {
        $request = new Request(
            'GET',
            sprintf($this->routePattern, $id),
            $this->requestOptions
        );

        $response = $this->setResponse($request);

        $attributes = json_decode($response->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR);

        if ($attributes  === null) {
            return null;
        }

        return $hydrator->hydrate((array)$attributes);
    }

    protected function requestFindOneByRef(string $ref, HydratorInterface $hydrator): ?object // (GET) /access/{id}
    {
        $request = new Request(
            'GET',
            sprintf($this->routePattern, $ref),
            $this->requestOptions
        );

        $response = $this->setResponse($request);

        $attributes = json_decode($response->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR);

        if ($attributes === null) {
            return null;
        }

        return $hydrator->hydrate((array)$attributes);
    }


    public function create(HydratorInterface $hydrator): ?array // (GET)(POST) /access/add
    {
        $request = new Request(
            'POST',
            $this->routePattern,
            $this->mergeRequestOptions(['form_params' => $hydrator->convert()])
        );

        $response = $this->setResponse($request, 201);

        return json_decode($response->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR);
    }

    public function update(int $id, HydratorInterface $hydrator): ?array // (GET)(POST|UPDATE) /access/{id}/update
    {
        $request = new Request(
            'UPDATE',
            sprintf($this->routePattern, $id),
            $this->mergeRequestOptions(['form_params' => $hydrator->convert()])
        );

        $response = $this->setResponse($request, 301);

        return json_decode($response->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR);
    }

    public function remove(int $id): ?array // (GET)(POST|DELETE) /access/{id}/delete
    {
        $request = new Request(
            'DELETE',
            sprintf($this->routePattern, $id),
            $this->requestOptions
        );

        $response = $this->setResponse($request);

        return json_decode($response->getBody()->__toString(), true, 512, JSON_THROW_ON_ERROR);
    }
}
