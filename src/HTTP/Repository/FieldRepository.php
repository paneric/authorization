<?php

declare(strict_types=1);

namespace Paneric\Authorization\HTTP\Repository;

use GuzzleHttp\ClientInterface as HttpClient;
use Paneric\Authorization\Interfaces\RepositoryInterface;
use Paneric\Authorization\DTO\FieldDTO;

class FieldRepository extends HttpClientRepository implements RepositoryInterface
{
    public function findAll()
    {
        return $this->requestFindAll(new FieldDTO());
    }

    public function findByIds(array $ids): array
    {
        return $this->requestFindByIds($ids, new FieldDTO());
    }

    public function findOneById(int $id): ?object
    {
        return $this->requestFindOneById($id, new FieldDTO());
    }

    public function findOneByRef(string $ref): ?object
    {
        return $this->requestFindOneByRef($ref, new FieldDTO());
    }

    public function findByEnhanced(array $criteria, string $operator, int $id = null): ?array
    {
        // TODO: Implement findByEnhanced() method.
        return null;
    }
}
