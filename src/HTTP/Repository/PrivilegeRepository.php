<?php

declare(strict_types=1);

namespace Paneric\Authorization\HTTP\Repository;

use GuzzleHttp\ClientInterface as HttpClient;
use Paneric\Authorization\DTO\PrivilegeDTO;
use Paneric\Authorization\Interfaces\RepositoryInterface;

class PrivilegeRepository extends HttpClientRepository implements RepositoryInterface
{
    public function findAll()
    {
        return $this->requestFindAll(new PrivilegeDTO());
    }

    public function findByIds(array $ids): array
    {
        return $this->requestFindByIds($ids, new PrivilegeDTO());
    }

    public function findOneById(int $id): ?object
    {
        return $this->requestFindOneById($id, new PrivilegeDTO());
    }

    public function findOneByRef(string $ref): ?object
    {
        return $this->requestFindOneByRef($ref, new PrivilegeDTO());
    }

    public function findByEnhanced(array $criteria, string $operator, int $id = null): ?array
    {
        // TODO: Implement findByEnhanced() method.
        return null;
    }
}
