<?php

declare(strict_types=1);

namespace Paneric\Authorization\HTTP\Repository;

use GuzzleHttp\ClientInterface as HttpClient;
use Paneric\Authorization\DTO\RoleDTO;
use Paneric\Authorization\Interfaces\RepositoryInterface;

class RoleRepository extends HttpClientRepository implements RepositoryInterface
{
    public function findAll()
    {
        return $this->requestFindAll(new RoleDTO());
    }

    public function findByIds(array $ids): array
    {
        return $this->requestFindByIds($ids, new RoleDTO());
    }

    public function findOneById(int $id): ?object
    {
        return $this->requestFindOneById($id, new RoleDTO());
    }

    public function findOneByRef(string $ref): ?object
    {
        return $this->requestFindOneByRef($ref, new RoleDTO());
    }

    public function findByEnhanced(array $criteria, string $operator, int $id = null): ?array
    {
        // TODO: Implement findByEnhanced() method.
        return null;
    }
}
