<?php

declare(strict_types=1);

namespace Paneric\Authorization\HTTP\Repository;

use GuzzleHttp\ClientInterface as HttpClient;
use Paneric\Authorization\DTO\AccessDTO;
use Paneric\Authorization\Interfaces\RepositoryInterface;

class AccessRepository extends HttpClientRepository implements RepositoryInterface
{
    public function findAll()
    {
        return $this->requestFindAll(new AccessDTO());
    }

    public function findByIds(array $ids): array
    {
        return $this->requestFindByIds($ids, new AccessDTO());
    }

    public function findOneById(int $id): ?object
    {
        return $this->requestFindOneById($id, new AccessDTO());
    }

    public function findOneByRef(string $ref): ?object
    {
        return $this->requestFindOneByRef($ref, new AccessDTO());
    }

    public function findByEnhanced(array $criteria, string $operator, int $id = null): ?array
    {
        // TODO: Implement findByEnhanced() method.
        return null;
    }
}
