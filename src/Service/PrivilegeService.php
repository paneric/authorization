<?php

declare(strict_types=1);

namespace Paneric\Authorization\Service;

use Paneric\Authorization\DTO\FieldDTO;
use Paneric\Authorization\DTO\PrivilegeDTO;
use Paneric\Authorization\DTO\RoleDTO;
use Paneric\Authorization\Interfaces\RepositoryInterface;
use Paneric\Authorization\Interfaces\PrivilegeQueryInterface;
use Paneric\Authorization\Interfaces\ServiceInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class PrivilegeService extends Service implements ServiceInterface
{
    protected $privilegeRepository;
    protected $privilegeQuery;
    protected $fieldRepository;
    protected $roleRepository;
    protected $accessRepository;


    public function __construct(
        RepositoryInterface $privilegeRepository,
        PrivilegeQueryInterface $privilegeQuery,
        RepositoryInterface $fieldRepository,
        RepositoryInterface $roleRepository,
        RepositoryInterface $accessRepository,
        SessionInterface $session
    ) {
        parent::__construct($session);

        $this->privilegeRepository = $privilegeRepository;
        $this->privilegeQuery = $privilegeQuery;
        $this->fieldRepository = $fieldRepository;
        $this->roleRepository = $roleRepository;
        $this->accessRepository = $accessRepository;
    }


    public function getAll(): array // OK
    {
        $this->session->setFlash(['page_title' => 'content_privileges_show_title'], 'value');

        $pagination = $this->session->getData('pagination');
        $privileges = $this->privilegeRepository->findBy(
            [],
            ['ref' => 'ASC'],
            $pagination['limit'],
            $pagination['offset']
        );

        return [
            'privileges' => $this->jsonSerializeObjects($privileges),
            'accesses' => $this->jsonSerializeObjectsById($this->accessRepository->findAll()),
        ];
    }

    public function getByIds(Request $request): array
    {
        $validation = [];

        if ($request->getMethod() === 'POST') {

            $validation = $request->getAttribute('validation')[PrivilegeDTO::class];

            if (empty($validation)) {

                $privileges = $this->privilegeRepository->findByIds(
                    $request->getParsedBody()
                );

                return $this->jsonSerializeObjects($privileges);
            }
        }

        return [
            'privileges' => $this->privilegeRepository->findAll(),
            'validation' => $validation
        ];
    }

    public function getOneById(int $privilegeId): ?object
    {
        return $this->privilegeRepository->findOneById($privilegeId);
    }

    public function getOneByRef(string $ref): ?object // OK
    {
        return $this->privilegeRepository->findOneByRef($ref);
    }

    public function create(Request $request): ?array // OK
    {
        $attributes = [];
        $validation = [];

        $privilegeDTO = new PrivilegeDTO();

        if ($request->getMethod() === 'POST') {

            $validation = $request->getAttribute('validation')[PrivilegeDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

                $accessId = (int) $attributes['access'];

                $uniqueAttributes = [
                    'ref' => $attributes['ref'],
                ];

                if ($this->privilegeRepository->findByEnhanced($uniqueAttributes, 'OR') === null) {

                    unset($attributes['access']);

                    if ($this->privilegeRepository->create($privilegeDTO->hydrate($attributes)) === null) {
                        $privilege = $this->privilegeRepository->findOneByRef($attributes['ref']);

                        return $this->privilegeQuery->setAccess(
                            $privilege->getId(),
                            $accessId
                        );
                    }

                    $this->session->setFlash(['db_privilege_add_error'], 'error');
                }

                $this->session->setFlash(['db_privilege_add_error'], 'error');
            }
        }

        return [
            'privilege' => $privilegeDTO->hydrate($attributes),
            'accesses' => $this->jsonSerializeObjectsById(
                $this->accessRepository->findAll()
            ),
            'validation' => $validation
        ];
    }

    public function update(int $privilegeId, Request $request): ?array // OK
    {
        $validation = [];

        $privilegeDTO = new PrivilegeDTO();

        $method = $request->getMethod();

        if ($method === 'POST') {

            $validation = $request->getAttribute('validation')[PrivilegeDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

                $accessId = (int) $attributes['access'];

                $uniqueAttributes = [
                    'ref' => $attributes['ref'],
                ];

                if ($this->privilegeRepository->findByEnhanced($uniqueAttributes, 'OR', $privilegeId) === null) {

                    unset($attributes['access']);

                    if ($this->privilegeRepository->update($privilegeId, $privilegeDTO->hydrate($attributes)) === null) {
                        $privilege = $this->privilegeRepository->findOneByRef($attributes['ref']);

                        return $this->privilegeQuery->setAccess(
                            $privilege->getId(),
                            $accessId
                        );
                    }

                    $this->session->setFlash(['db_privilege_edit_error'], 'error');
                }

                $this->session->setFlash(['db_privilege_edit_error'], 'error');
            }
        }

        return [
            'privilege' => $this->privilegeRepository->findOneById($privilegeId),
            'accesses' => $this->jsonSerializeObjectsById(
                $this->accessRepository->findAll()
            ),
            'validation' => $validation
        ];
    }

    public function delete(Request $request, int $privilegeId): ?array  // OK
    {
        if ($request->getMethod() === 'POST') {

            return $this->privilegeRepository->remove($privilegeId);
        }

        $this->session->setFlash(['privilege_delete_warning'], 'warning');

        return [];
    }


    public function getFieldById(int $privilegeId, int $fieldId): ?object// (GET) /privilege/{id}/field
    {
        return $this->privilegeQuery->findFieldById($privilegeId, $fieldId);
    }


    public function getFields(int $privilegeId): array// (GET) /privilege/{id}/fields
    {
        return $this->jsonSerializeObjects(
            $this->privilegeQuery->findFields($privilegeId)
        );
    }

    public function updateFields(int $privilegeId, Request $request) // OK
    {
        $validation = [];

        if ($request->getMethod() === 'POST') {

            $validation = $request->getAttribute('validation')[FieldDTO::class];

            $arguments = $request->getParsedBody()['id'];

            if ($arguments === null) {
                return 'message';
            }

            if (empty($validation)) {
                return $this->privilegeQuery->updateFields(
                    $privilegeId,
                    $request->getParsedBody()['id']
                );
            }
        }

        $this->session->setFlash(['page_title' => 'form_privilege_fields_edit_title'], 'value');
        $this->session->setFlash(['relation_name' => 'privilege'], 'value');
        
        return [
            'relation' => $this->privilegeRepository->findOneById($privilegeId),
            'fields' => $this->fieldRepository->findAll(),
            'relation_fields' => $this->jsonSerializeObjectsById(
                $this->privilegeQuery->findFields($privilegeId)
            ),
            'validation' => $validation
        ];
    }

    public function deleteFields(Request $request, int $privilegeId): ?array // Ok
    {
        if ($request->getMethod() === 'POST') {

            return $this->privilegeQuery->removeFields($privilegeId);
        }

        $this->session->setFlash(['page_title' => 'form_privilege_fields_delete_title'], 'value');
        $this->session->setFlash(['privilege_fields_delete_warning'], 'warning');

        return [];
    }


    public function getRoles(int $privilegeId): array// (GET) /privilege/{id}/roles
    {
        return $this->jsonSerializeObjects(
            $this->privilegeQuery->findRoles($privilegeId)
        );
    }

    public function updateRoles(int $privilegeId, Request $request) // OK
    {
        $validation = [];

        if ($request->getMethod() === 'POST') {

            $validation = $request->getAttribute('validation')[RoleDTO::class];

            $arguments = $request->getParsedBody()['id'];

            if ($arguments === null) {
                return 'message';
            }

            if (empty($validation)) {
                return $this->privilegeQuery->updateRoles(
                    $privilegeId,
                    $request->getParsedBody()['id']
                );
            }
        }

        $this->session->setFlash(['page_title' => 'form_privilege_roles_edit_title'], 'value');
        $this->session->setFlash(['relation_name' => 'privilege'], 'value');
        
        return [
            'relation' => $this->privilegeRepository->findOneById($privilegeId),
            'roles' => $this->roleRepository->findAll(),
            'relation_roles' => $this->jsonSerializeObjectsById(
                $this->privilegeQuery->findRoles($privilegeId)
            ),
            'validation' => $validation
        ];
    }

    public function deleteRoles(Request $request, int $privilegeId): ?array // OK
    {
        if ($request->getMethod() === 'POST') {

            return $this->privilegeQuery->removeRoles($privilegeId);
        }
        
        $this->session->setFlash(['page_title' => 'form_privilege_roles_delete_title'], 'value');
        $this->session->setFlash(['privilege_roles_delete_warning'], 'warning');

        return [];
    }


    public function getAccess(int $privilegeId): ?object
    {
        return $this->privilegeQuery->findAccess($privilegeId);
    }

    public function setAccess(int $privilegeId, Request $request): ?array // (GET/POST||UPDATE) /privilege/{id}/access/update
    {
        $validation = [];

        $method = $request->getMethod();

        if ($method === 'POST' || $method === 'PATCH') {

            $validation = $request->getAttribute('validation')[PrivilegeDTO::class];

            if (empty($validation)) {

                return $this->privilegeQuery->setAccess(
                    $privilegeId,
                    $request->getParsedBody()['id']
                );
            }
        }

        return [
            'privilege' => $this->privilegeRepository->findOneById($privilegeId),
            'accesses' => $this->accessRepository->findAll(),
            'validation' => $validation
        ];
    }

    public function deleteAccess(int $privilegeId): ?array // (GET||DELETE) /privilege/{id}/access/delete
    {
        return $this->privilegeQuery->removeAccess($privilegeId);
    }
}
