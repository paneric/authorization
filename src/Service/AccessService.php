<?php

declare(strict_types=1);

namespace Paneric\Authorization\Service;

use Paneric\Authorization\DTO\AccessDTO;
use Paneric\Authorization\DTO\PrivilegeDTO;
use Paneric\Authorization\Interfaces\AccessQueryInterface;
use Paneric\Authorization\Interfaces\RepositoryInterface;
use Paneric\Authorization\Interfaces\ServiceInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class AccessService extends Service implements ServiceInterface
{
    protected $accessRepository;
    protected $accessQuery;
    protected $privilegeRepository;


    public function __construct(
        RepositoryInterface $accessRepository,
        AccessQueryInterface $accessQuery,
        RepositoryInterface $privilegeRepository,
        SessionInterface $session
    ) {
        parent::__construct($session);

        $this->accessRepository = $accessRepository;
        $this->accessQuery = $accessQuery;
        $this->privilegeRepository = $privilegeRepository;
    }


    public function getAll(): array
    {
        $this->session->setFlash(['page_title' => 'content_accesses_show_title'], 'value');

        $pagination = $this->session->getData('pagination');
        $accesses = $this->accessRepository->findBy(
            [],
            ['ref' => 'ASC'],
            $pagination['limit'],
            $pagination['offset']
        );

        return [
            'accesses' => $this->jsonSerializeObjects($accesses)
        ];
    }

    public function getByIds(Request $request): array
    {
        $validation = [];

        if ($request->getMethod() === 'POST') {

            $validation = $request->getAttribute('validation')[AccessDTO::class];

            if (empty($validation)) {

                $accesses = $this->accessRepository->findByIds(
                    $request->getParsedBody()
                );

                return $this->jsonSerializeObjects($accesses);
            }
        }

        return [
            'accesses' => $this->accessRepository->findAll(),
            'validation' => $validation
        ];
    }

    public function getOneById(int $accessId): ?object
    {
        return $this->accessRepository->findOneById($accessId);
    }

    public function getOneByRef(string $ref): ?object
    {
        return $this->accessRepository->findOneByRef($ref);
    }

    public function create(Request $request): ?array
    {
        $attributes = [];
        $validation = [];

        $accessDTO = new AccessDTO();

        if ($request->getMethod() === 'POST') {

            $validation = $request->getAttribute('validation')[AccessDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

                $uniqueAttributes = [
                    'ref' => $attributes['ref'],
                    'level' => $attributes['level']
                ];

                if ($this->accessRepository->findByEnhanced($uniqueAttributes, 'OR') === null) {
                    return $this->accessRepository->create($accessDTO->hydrate($attributes));
                }

                $this->session->setFlash(['db_access_add_error'], 'error');
            }
        }

        return [
            'access' => $accessDTO->hydrate($attributes),
            'validation' => $validation
        ];
    }

    public function update(int $accessId, Request $request): ?array
    {
        $validation = [];

        $accessDTO = new AccessDTO();

        $method = $request->getMethod();

        if ($method === 'POST') {

            $validation = $request->getAttribute('validation')[AccessDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

                $uniqueAttributes = [
                    'ref' => $attributes['ref'],
                    'level' => $attributes['level']
                ];

                if ($this->accessRepository->findByEnhanced($uniqueAttributes, 'OR', $accessId) === null) {
                    return $this->accessRepository->update(
                        $accessId,
                        $accessDTO->hydrate($attributes)
                    );
                }

                $this->session->setFlash(['db_access_edit_error'], 'error');
            }
        }

        return [
            'access' => $this->accessRepository->findOneById($accessId),
            'validation' => $validation
        ];
    }

    public function delete(Request $request, int $accessId): ?array
    {
        if ($request->getMethod() === 'POST') {

            return $this->accessRepository->remove($accessId);
        }

        $this->session->setFlash(['access_delete_warning'], 'warning');
        
        return [];
    }


    public function getPrivileges(int $accessId): array
    {
        return $this->jsonSerializeObjects(
            $this->accessQuery->findPrivileges($accessId)
        );
    }

    public function updatePrivileges(int $accessId, Request $request) // OK
    {
        $validation = [];

        if ($request->getMethod() === 'POST') {

            $validation = $request->getAttribute('validation')[PrivilegeDTO::class];

            $arguments = $request->getParsedBody()['id'];

            if ($arguments === null) {
                return 'message';
            }

            if (empty($validation)) {
                return $this->accessQuery->updatePrivileges(
                    $accessId,
                    $arguments
                );
            }
        }

        $this->session->setFlash(['page_title' => 'form_access_privileges_edit_title'], 'value');
        $this->session->setFlash(['relation_name' => 'access'], 'value');

        return [
            'relation' => $this->accessRepository->findOneById($accessId),
            'privileges' => $this->privilegeRepository->findAll(),
            'relation_privileges' => $this->jsonSerializeObjectsById(
                $this->accessQuery->findPrivileges($accessId)
            ),
            'validation' => $validation
        ];
    }

    public function deletePrivileges(Request $request, int $accessId): ?array
    {
        if ($request->getMethod() === 'POST') {

            return $this->accessQuery->removePrivileges($accessId);
        }

        $this->session->setFlash(['page_title' => 'form_access_privileges_delete_title'], 'value');
        $this->session->setFlash(['access_privileges_delete_warning'], 'warning');

        return [];
    }
}
