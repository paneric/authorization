<?php

declare(strict_types=1);

namespace Paneric\Authorization\Service;

use Paneric\Authorization\DTO\PrivilegeDTO;
use Paneric\Authorization\DTO\RoleDTO;
use Paneric\Authorization\Interfaces\RepositoryInterface;
use Paneric\Authorization\Interfaces\RoleQueryInterface;
use Paneric\Authorization\Interfaces\ServiceInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class RoleService extends Service implements ServiceInterface
{
    protected $roleRepository;
    protected $roleQuery;
    protected $privilegeRepository;


    public function __construct(
        RepositoryInterface $roleRepository,
        RoleQueryInterface $roleQuery,
        RepositoryInterface $privilegeRepository,
        SessionInterface $session
    ) {
        parent::__construct($session);

        $this->roleRepository = $roleRepository;
        $this->roleQuery = $roleQuery;
        $this->privilegeRepository = $privilegeRepository;
    }


    public function getAll(): array // OK
    {
        $this->session->setFlash(['page_title' => 'content_roles_show_title'], 'value');

        $pagination = $this->session->getData('pagination');
        $roles = $this->roleRepository->findBy(
            [],
            ['ref' => 'ASC'],
            $pagination['limit'],
            $pagination['offset']
        );

        return [
            'roles' => $this->jsonSerializeObjects($roles),
        ];
    }

    public function getByIds(Request $request): array
    {
        $validation = [];

        if ($request->getMethod() === 'POST') {

            $validation = $request->getAttribute('validation')[RoleDTO::class];

            if (empty($validation)) {

                $roles = $this->roleRepository->findByIds(
                    $request->getParsedBody()
                );

                return $this->jsonSerializeObjects($roles);
            }
        }

        return [
            'roles' => $this->roleRepository->findAll(),
            'validation' => $validation
        ];
    }

    public function getOneById(int $roleId): ?object
    {
        return $this->roleRepository->findOneById($roleId);
    }

    public function getOneByRef(string $ref): ?object
    {
        return $this->roleRepository->findOneByRef($ref);
    }

    public function create(Request $request): ?array // OK
    {
        $attributes = [];
        $validation = [];

        $roleDTO = new RoleDTO();

        if ($request->getMethod() === 'POST') {

            $validation = $request->getAttribute('validation')[RoleDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

                $uniqueAttributes = [
                    'ref' => $attributes['ref'],
                ];

                if ($this->roleRepository->findByEnhanced($uniqueAttributes, 'OR') === null) {
                    return $this->roleRepository->create($roleDTO->hydrate($attributes));
                }

                $this->session->setFlash(['db_role_add_error'], 'error');
            }
        }

        return [
            'role' => $roleDTO->hydrate($attributes),
            'validation' => $validation
        ];
    }

    public function update(int $roleId, Request $request): ?array // OK
    {
        $validation = [];

        $roleDTO = new RoleDTO();

        $method = $request->getMethod();

        if ($method === 'POST') {

            $validation = $request->getAttribute('validation')[RoleDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

                $uniqueAttributes = [
                    'ref' => $attributes['ref'],
                ];

                if ($this->roleRepository->findByEnhanced($uniqueAttributes, 'OR', $roleId) === null) {
                    return $this->roleRepository->update(
                        $roleId,
                        $roleDTO->hydrate($attributes)
                    );
                }

                $this->session->setFlash(['db_role_edit_error'], 'error');
            }
        }

        return [
            'role' => $this->roleRepository->findOneById($roleId),
            'validation' => $validation
        ];
    }

    public function delete(Request $request, int $roleId): ?array  // OK
    {
        if ($request->getMethod() === 'POST') {

            return $this->roleRepository->remove($roleId);
        }

        $this->session->setFlash(['role_delete_warning'], 'warning');

        return [];
    }


    public function getPrivilegeById(int $roleId, int $privilegeId): ?object // (GET) /role/{idr}/privilege/{idp}
    {
        return $this->roleQuery->findPrivilegeById($roleId, $privilegeId);
    }
    
    public function updatePrivileges(int $roleId, Request $request) // OK
    {
        $validation = [];

        if ($request->getMethod() === 'POST') {

            $validation = $request->getAttribute('validation')[PrivilegeDTO::class];

            $arguments = $request->getParsedBody()['id'];

            if ($arguments === null) {
                return 'message';
            }

            if (empty($validation)) {
                return $this->roleQuery->updatePrivileges(
                    $roleId,
                    $arguments
                );
            }
        }

        $this->session->setFlash(['page_title' => 'form_role_privileges_edit_title'], 'value');
        $this->session->setFlash(['relation_name' => 'role'], 'value');
        
        return [
            'relation' => $this->roleRepository->findOneById($roleId),
            'privileges' => $this->privilegeRepository->findAll(),
            'relation_privileges' => $this->jsonSerializeObjectsById(
                $this->roleQuery->findPrivileges($roleId)
            ),
            'validation' => $validation
        ];
    }

    public function getPrivileges(int $roleId): array // (GET) /role/{id}/privileges
    {
        return $this->jsonSerializeObjectsById(
            $this->roleQuery->findPrivileges($roleId)
        );
    }
    
    public function deletePrivileges(Request $request, int $roleId): ?array // (GET||DELETE) /role/{id}/privileges/delete
    {
        if ($request->getMethod() === 'POST') {

            return $this->roleQuery->removePrivileges($roleId);
        }

        $this->session->setFlash(['page_title' => 'form_role_privileges_delete_title'], 'value');
        $this->session->setFlash(['role_privileges_delete_warning'], 'warning');

        return [];
    }
}
