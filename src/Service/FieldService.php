<?php

declare(strict_types=1);

namespace Paneric\Authorization\Service;

use Paneric\Authorization\DTO\FieldDTO;
use Paneric\Authorization\DTO\PrivilegeDTO;
use Paneric\Authorization\Interfaces\FieldQueryInterface;
use Paneric\Authorization\Interfaces\RepositoryInterface;
use Paneric\Authorization\Interfaces\ServiceInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Interfaces\Session\SessionInterface;

class FieldService extends Service implements ServiceInterface
{
    protected $fieldRepository;
    protected $fieldQuery;
    protected $privilegeRepository;


    public function __construct(
        RepositoryInterface $fieldRepository,
        FieldQueryInterface $fieldQuery,
        RepositoryInterface $privilegeRepository,
        SessionInterface $session
    ) {
        parent::__construct($session);

        $this->fieldRepository = $fieldRepository;
        $this->fieldQuery = $fieldQuery;
        $this->privilegeRepository = $privilegeRepository;
    }


    public function getAll(): array // (GET) /fields
    {
        $this->session->setFlash(['page_title' => 'content_fields_show_title'], 'value');

        $pagination = $this->session->getData('pagination');
        $fields = $this->fieldRepository->findBy(
            [],
            ['ref' => 'ASC'],
            $pagination['limit'],
            $pagination['offset']
        );

        return [
            'fields' => $this->jsonSerializeObjects($fields)
        ];
    }

    public function getByIds(Request $request): array // (POST) /fields
    {
        $validation = [];

        if ($request->getMethod() === 'POST') {

            $validation = $request->getAttribute('validation')[FieldDTO::class];

            if (empty($validation)) {

                $fields = $this->fieldRepository->findByIds(
                    $request->getParsedBody()
                );

                return $this->jsonSerializeObjects($fields);
            }
        }

        return [
            'fields' => $this->fieldRepository->findAll(),
            'validation' => $validation
        ];
    }

    public function getOneById(int $fieldId): ?object // (GET) /field/{id}
    {
        return $this->fieldRepository->findOneById($fieldId);
    }

    public function getOneByRef(string $ref): ?object
    {
        return $this->fieldRepository->findOneByRef($ref);
    }

    public function create(Request $request): ?array // (GET/POST) /field
    {
        $attributes = [];
        $validation = [];

        $fieldDTO = new FieldDTO();

        if ($request->getMethod() === 'POST') {

            $validation = $request->getAttribute('validation')[FieldDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

                $uniqueAttributes = [
                    'ref' => $attributes['ref'],
                ];

                if ($this->fieldRepository->findByEnhanced($uniqueAttributes, 'OR') === null) {
                    return $this->fieldRepository->create($fieldDTO->hydrate($attributes));
                }

                $this->session->setFlash(['db_field_add_error'], 'error');
            }
        }

        return [
            'field' => $fieldDTO->hydrate($attributes),
            'validation' => $validation
        ];
    }

    public function update(int $fieldId, Request $request): ?array // (GET/POST||UPDATE) /field/{id}/update
    {
        $validation = [];

        $fieldDTO = new FieldDTO();

        $method = $request->getMethod();

        if ($method === 'POST') {

            $validation = $request->getAttribute('validation')[FieldDTO::class];

            $attributes = $request->getParsedBody();

            if (empty($validation)) {

                $uniqueAttributes = [
                    'ref' => $attributes['ref'],
                ];

                if ($this->fieldRepository->findByEnhanced($uniqueAttributes, 'OR', $fieldId) === null) {
                    return $this->fieldRepository->update(
                        $fieldId,
                        $fieldDTO->hydrate($attributes)
                    );
                }

                $this->session->setFlash(['db_field_edit_error'], 'error');
            }
        }

        return [
            'field' => $this->fieldRepository->findOneById($fieldId),
            'validation' => $validation
        ];
    }

    public function delete(Request $request, int $fieldId): ?array  // (GET||DELETE) /field/{id}/delete
    {
        if ($request->getMethod() === 'POST') {

            return $this->fieldRepository->remove($fieldId);
        }

        $this->session->setFlash(['field_delete_warning'], 'warning');

        return [];
    }


    public function getPrivileges(int $fieldId): array // (GET) /field/{id}/privileges
    {
        return $this->jsonSerializeObjects(
            $this->fieldQuery->findPrivileges($fieldId)
        );
    }

    public function updatePrivileges(int $fieldId, Request $request) // Ok
    {
        $validation = [];

        if ($request->getMethod() === 'POST') {

            $validation = $request->getAttribute('validation')[PrivilegeDTO::class];

            $arguments = $request->getParsedBody()['id'];

            if ($arguments === null) {
                return 'message';
            }

            if (empty($validation)) {
                return $this->fieldQuery->updatePrivileges(
                    $fieldId,
                    $arguments
                );
            }
        }

        $this->session->setFlash(['page_title' => 'form_field_privileges_edit_title'], 'value');
        $this->session->setFlash(['relation_name' => 'field'], 'value');
        
        return [
            'relation' => $this->fieldRepository->findOneById($fieldId),
            'privileges' => $this->privilegeRepository->findAll(),
            'relation_privileges' => $this->jsonSerializeObjectsById(
                $this->fieldQuery->findPrivileges($fieldId)
            ),
            'validation' => $validation
        ];
    }

    public function deletePrivileges(Request $request, int $fieldId): ?array // (GET||DELETE) /field/{id}/privileges/delete
    {
        if ($request->getMethod() === 'POST') {

            return $this->fieldQuery->removePrivileges($fieldId);
        }

        $this->session->setFlash(['page_title' => 'form_field_privileges_delete_title'], 'value');
        $this->session->setFlash(['field_privileges_delete_warning'], 'warning');

        return [];
    }
}
