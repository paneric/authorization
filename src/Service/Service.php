<?php

declare(strict_types=1);

namespace Paneric\Authorization\Service;

use Paneric\Interfaces\Session\SessionInterface;

abstract class Service
{
    protected $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function jsonSerializeObjects(array $objects): ?array
    {
        foreach ($objects as $key => $object) {
            $objects[$key] = $object->convert();
        }

        return $objects;
    }

    public function jsonSerializeObjectsById(array $objects): ?array
    {
        $objectsById = [];
        
        foreach ($objects as $object) {
            $key = $object->getId();
            $objectsById[$key] = $object->convert();
        }

        return $objectsById;
    }
}
