<?php

declare(strict_types=1);

namespace Paneric\Authorization;

use InvalidArgumentException;
use Paneric\Slim\Exception\HttpUnauthorizedException;
use Paneric\Authorization\Interfaces\RepositoryInterface;
use Paneric\Authorization\DTO\PrivilegeDTO;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class AuthorizationMiddleware implements MiddlewareInterface
{
    private $roleRepository;
    private $privilegeRepository;
    private $session;

    public function __construct(
        RepositoryInterface $roleRepository,
        RepositoryInterface $privilegeRepository,
        SessionInterface $session
    ) {
        $this->roleRepository = $roleRepository;
        $this->privilegeRepository = $privilegeRepository;
        $this->session = $session;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $routeName = $request->getAttribute('route_name');

        if ($routeName === null) {
            throw new InvalidArgumentException(
                'PANERIC: \'route_name\' key not found within server request attributes !!!'
            );
        }

        $requiredPrivilege = $this->privilegeRepository->findOneByRef($routeName);

        if ($requiredPrivilege !== null) {

            $userRole = $this->getUserRole();

            $userPrivilege = $this->getUserPrivilege(
                $requiredPrivilege,
                $userRole->getId()
            );

            if ($userPrivilege === null) {
                throw new HttpUnauthorizedException($request);
            }

            $request->withAttribute('authorization', $userPrivilege);

            return $handler->handle($request);
        }

        $request->withAttribute('authorization', $requiredPrivilege);

        return $handler->handle($request);
    }

    private function getUserRole(): ?object
    {
        $authentication = $this->session->getData('authentication');

        if ($authentication === null) {
            throw new InvalidArgumentException(
                '\'authentication\' key not found in session data !!!'
            );
        }

        if(!isset($authentication['role_id'])) {
            return null;
        }

        return $this->roleRepository->findOneById($authentication['role_id']);
    }

    private function getUserPrivilege(PrivilegeDTO $requiredPrivilege, int $roleId): ?object
    {
        $roles = $requiredPrivilege->getRoles();

        $roles = (array_filter($roles, static function($role) use ($roleId)
        {
            return $role->getId() === $roleId;
        }));

        if ($roles !== []) {
            return $requiredPrivilege;
        }

        return null;
    }
}
