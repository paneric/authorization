<?php

declare(strict_types=1);

namespace Paneric\Authorization\Interfaces;

interface RepositoryInterface
{
    /**
     * Finds all entities in the repository.
     *
     * @return array The entities.
     */
    public function findAll();
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): array;
    /**
     * Finds entities by a set of id.
     *
     * @param array      $ids
     *
     * @return array The objects.
     */
    public function findByIds(array $ids): array;

    public function findOneById(int $id): ?object;
    public function findOneByRef(string $ref): ?object;
    public function findByEnhanced(array $criteria, string $operator, int $id = null): ?array;

    public function create(HydratorInterface $hydrator): ?array;
    public function update(int $id, HydratorInterface $hydrator): ?array;
    public function remove(int $id): ?array;
}
