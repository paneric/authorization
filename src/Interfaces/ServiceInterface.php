<?php

declare(strict_types=1);

namespace Paneric\Authorization\Interfaces;

use Psr\Http\Message\ServerRequestInterface as Request;

interface ServiceInterface
{
    public function getAll(): array; // (GET) /xxx

    public function getByIds(Request $request): array; // (POST) /xxx

    public function getOneById(int $id): ?object; // (GET) /xxx/{id}

    public function getOneByRef(string $ref): ?object; // (GET) /xxx/ref/{ref}

    public function create(Request $request): ?array; // (POST) /xxx

    public function update(int $id, Request $request): ?array; // (UPDATE) /xxx/{id}/update

    public function delete(Request $request, int $id): ?array;  // (DELETE) /xxx/{id}/delete
}
