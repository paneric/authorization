<?php

declare(strict_types=1);

namespace Paneric\Authorization\Interfaces;

interface FieldQueryInterface
{
    public function findPrivileges(int $fieldId): array;

    public function updatePrivileges(int $fieldId, array $privilegesIds): ?array;
    public function removePrivileges(int $fieldId): ?array;
}
