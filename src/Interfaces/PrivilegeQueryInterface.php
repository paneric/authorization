<?php

declare(strict_types=1);

namespace Paneric\Authorization\Interfaces;

interface PrivilegeQueryInterface
{
    public function findFieldById(int $privilegeId, int $fieldId): ?object;

    public function findFields(int $privilegeId): array;

    public function updateFields(int $privilegeId, array $fieldsIds): ?array;
    public function removeFields(int $privilegeId): ?array;

    public function findRoles(int $privilegeId): array;

    public function updateRoles(int $privilegeId, array $rolesIds): ?array;
    public function removeRoles(int $privilegeId): ?array;

    public function findAccess(int $privilegeId): ?object;
    public function setAccess(int $privilegeId, int $accessId): ?array;
    public function removeAccess(int $privilegeId): ?array;
}
