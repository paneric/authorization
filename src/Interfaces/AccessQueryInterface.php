<?php

declare(strict_types=1);

namespace Paneric\Authorization\Interfaces;

interface AccessQueryInterface
{
    public function findPrivileges(int $accessId): array;

    public function updatePrivileges(int $accessId, array $privilegesIds): ?array;
    public function removePrivileges(int $accessId): ?array;
}
