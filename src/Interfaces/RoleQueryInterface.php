<?php

declare(strict_types=1);

namespace Paneric\Authorization\Interfaces;

interface RoleQueryInterface
{
    public function findPrivilegeById(int $roleId, int $privilegeId): ?object; // (GET) role/{idr}/privilege/{idp}
    public function findPrivilegeByRoute(int $roleId, string $route): ?object; // (GET) role/{idr}/route/{route}
    public function findPrivileges(int $roleId): array;// - (GET) role/{id}/privileges/

    public function updatePrivileges(int $roleId, array $privilegesIds): ?array;// - (UPDATE) role/{id}/privileges/
    public function removePrivileges(int $roleId): ?array;// - (DELETE) role/{id}/privileges
}
