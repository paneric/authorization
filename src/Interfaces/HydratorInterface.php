<?php

declare(strict_types=1);

namespace Paneric\Authorization\Interfaces;

interface HydratorInterface
{
    public function hydrate(array $attributes);

    public function convert(): array;
}
