<?php

declare(strict_types=1);

namespace Paneric\Authorization\ORM\Entity;

use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Paneric\Authorization\Interfaces\HydratorInterface;

/**
 * @ORM\Entity(repositoryClass="Paneric\Authorization\ORM\Repository\AccessRepository")
 * @ORM\Table(
 *     name="`accesses`",
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(name="ref_idx", columns={"ref"}),
 *         @ORM\UniqueConstraint(name="level_idx", columns={"level"})
 *     },
 *     indexes={
 *         @ORM\Index(name="created_at_idx", columns={"created_at"}),
 *         @ORM\Index(name="updated_at_idx", columns={"updated_at"})
 *     }
 * )
 * @ORM\HasLifecycleCallbacks
 */
class Access implements HydratorInterface, JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="integer")
     */
    private $level;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ref;
    /**
     * @var Collection|Privilege[]
     *
     * @OneToMany(targetEntity="Privilege", mappedBy="access", cascade={"persist"})
     */
    private $privileges;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $info;
    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;
    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $updatedAt;


    public function __construct()
    {
        $this->privileges = new ArrayCollection();
    }

    /** @ORM\PrePersist */
    public function onPrePersist(): void
    {
        $this->createdAt = new DateTimeImmutable('now');
        $this->updatedAt = new DateTimeImmutable('now');
    }
    /** @ORM\PreUpdate */
    public function onPreUpdate(): void
    {
        $this->updatedAt = new DateTimeImmutable('now');
    }
    /** @ORM\PreRemove */
    public function onPreRemove(): void
    {
        $this->removePrivileges();
    }


    public function addPrivilege(Privilege $privilege): void
    {
        if ($this->privileges->contains($privilege)) {
            return;
        }
        $this->privileges->add($privilege);
        $privilege->updateAccess($this);
    }

    public function addPrivileges(array $privileges): void
    {
        foreach ($privileges as $privilege) {
            $this->addPrivilege($privilege);
        }
    }

    public function updatePrivileges(array $privileges): void
    {
        $this->removePrivileges();

        foreach ($privileges as $privilege) {
            $this->addPrivilege($privilege);
        }
    }

    public function removePrivilege(Privilege $privilege): void
    {
        if (!$this->privileges->contains($privilege)) {
            return;
        }
        $this->privileges->removeElement($privilege);
        $privilege->updateAccess(null);
    }

    public function removePrivileges(array $privileges = null): void
    {
        if ($privileges === null) {
            foreach ($this->privileges as $privilege) {
                $this->removePrivilege($privilege);
            }

            return;
        }

        foreach ($privileges as $privilege) {
            $this->removePrivilege($privilege);
        }
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function getRef(): ?string
    {
        return $this->ref;
    }

    public function getPrivileges(): ?array
    {
        return $this->privileges->toArray();
    }

    public function getInfo(): ?string
    {
        return $this->info;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }


    public function hydrate(array $attributes): self
    {
        if (isset($attributes['level'])) {
            $this->level = (int) $attributes['level'];
        }

        if (isset($attributes['ref'])) {
            $this->ref = $attributes['ref'];
        }

        if (isset($attributes['info'])) {
            $this->info = $attributes['info'];
        }

        return $this;
    }

    public function convert(): array
    {
        $attributes = [];

        if ($this->level !== null) {
            $attributes['level'] = $this->level;
        }

        if ($this->ref !== null) {
            $attributes['ref'] = $this->ref;
        }

        if ($this->info !== null) {
            $attributes['info'] = $this->info;
        }

        return $attributes;
    }

    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }
}
