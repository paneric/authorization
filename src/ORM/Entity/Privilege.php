<?php

declare(strict_types=1);

namespace Paneric\Authorization\ORM\Entity;

use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use JsonSerializable;
use Paneric\Authorization\Interfaces\HydratorInterface;

/**
 * @ORM\Entity(repositoryClass="Paneric\Authorization\ORM\Repository\PrivilegeRepository")
 * @ORM\Table(
 *     name="`privileges`",
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(name="ref_idx", columns={"ref"})
 *     },
 *     indexes={
 *         @ORM\Index(name="route_idx", columns={"route"}),
 *         @ORM\Index(name="created_at_idx", columns={"created_at"}),
 *         @ORM\Index(name="updated_at_idx", columns={"updated_at"})
 *     }
 * )
 * @ORM\HasLifecycleCallbacks
 */
class Privilege implements HydratorInterface, JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ref;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $route;
    /**
     * @var Access
     *
     * @ManyToOne(targetEntity="Access", inversedBy="privileges")
     */
    private $access;
    /**
     * @var Collection|Field[]
     *
     * @ManyToMany(targetEntity="Field", inversedBy="privileges", cascade={"persist"})
     * @JoinTable(name="privileges_fields")
     */
    private $fields;
    /**
     * @var Collection|Role[]
     *
     * @ManyToMany(targetEntity="Role", mappedBy="privileges", cascade={"persist"})
     */
    private $roles;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $info;
    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;
    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $updatedAt;


    public function __construct()
    {
        $this->roles = new ArrayCollection();
        $this->fields = new ArrayCollection();
    }

    /** @ORM\PrePersist */
    public function onPrePersist(): void
    {
        $this->createdAt = new DateTimeImmutable('now');
        $this->updatedAt = new DateTimeImmutable('now');
    }
    /** @ORM\PreUpdate */
    public function onPreUpdate(): void
    {
        $this->updatedAt = new DateTimeImmutable('now');
    }
    /** @ORM\PreRemove */
    public function onPreRemove(): void
    {
        $this->removeFields();
        $this->removeRoles();
    }


    public function updateAccess(?Access $access): void
    {
        if ($this->access === $access) {
            return;
        }

        if ($this->access !== null) {
            $this->access->removePrivilege($this);
        }

        if ($access !== null) {
            $access->addPrivilege($this);
        }

        $this->access = $access;
    }


    public function addField(Field $field): void
    {
        if ($this->fields->contains($field)) {
            return;
        }
        $this->fields->add($field);
        $field->addPrivilege($this);
    }

    public function addFields(array $fields): void
    {
        foreach ($fields as $field) {
            $this->addField($field);
        }
    }

    public function updateFields(array $fields): void
    {
        $this->removeFields();

        foreach ($fields as $field) {
            $this->addField($field);
        }
    }

    public function removeField(Field $field): void
    {
        if (!$this->fields->contains($field)) {
            return;
        }
        $this->fields->removeElement($field);
        $field->removePrivilege($this);
    }

    public function removeFields(array $fields = null): void
    {
        if ($fields === null) {
            foreach ($this->fields as $field) {
                $this->removeField($field);
            }

            return;
        }

        foreach ($fields as $field) {
            $this->removeField($field);
        }
    }


    public function addRole(Role $role): void
    {
        if ($this->roles->contains($role)) {
            return;
        }
        $this->roles->add($role);
        $role->addPrivilege($this);
    }

    public function addRoles(array $roles): void
    {
        foreach ($roles as $role) {
            $this->addRole($role);
        }
    }

    public function updateRoles(array $roles): void
    {
        $this->removeRoles();

        foreach ($roles as $role) {
            $this->addRole($role);
        }
    }

    public function removeRole(Role $role): void
    {
        if (!$this->roles->contains($role)) {
            return;
        }
        $this->roles->removeElement($role);
        $role->removePrivilege($this);
    }

    public function removeRoles(array $roles = null): void
    {
        if ($roles === null) {
            foreach ($this->roles as $role) {
                $this->removeRole($role);
            }

            return;
        }

        foreach ($roles as $role) {
            $this->removeRole($role);
        }
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRef(): ?string
    {
        return $this->ref;
    }

    public function getRoute(): ?string
    {
        return $this->route;
    }

    public function getAccess(): ?Access
    {
        return $this->access;
    }

    public function getFields(): ?array
    {
        return $this->fields->toArray();
    }

    public function getRoles(): ?array
    {
        return $this->roles->toArray();
    }

    public function getInfo(): ?string
    {
        return $this->info;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }


    public function hydrate(array $attributes): self
    {
        if (isset($attributes['ref'])) {
            $this->ref = $attributes['ref'];
        }

        if (isset($attributes['route'])) {
            $this->route = $attributes['route'];
        }

        if (isset($attributes['info'])) {
            $this->info = $attributes['info'];
        }

        return $this;
    }

    public function convert(): array
    {
        $attributes = [];

        if ($this->ref !== null) {
            $attributes['ref'] = $this->ref;
        }

        if ($this->route !== null) {
            $attributes['route'] = $this->route;
        }

        if ($this->info !== null) {
            $attributes['info'] = $this->info;
        }

        return $attributes;
    }

    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }
}
