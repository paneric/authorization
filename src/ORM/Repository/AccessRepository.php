<?php

declare(strict_types=1);

namespace Paneric\Authorization\ORM\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Paneric\Authorization\Interfaces\RepositoryInterface;
use Paneric\Authorization\Interfaces\HydratorInterface;
use Paneric\Authorization\ORM\Entity\Access;

class AccessRepository extends ORMRepository implements RepositoryInterface
{
    public function __construct(EntityManagerInterface $_em)
    {
        $this->entityClass = Access::class;

        parent::__construct($_em);
    }


    public function create(HydratorInterface $accessDTO): ?array
    {
        return $this->queryCreate($accessDTO, new Access());
    }
}
