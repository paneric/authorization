<?php

declare(strict_types=1);

namespace Paneric\Authorization\ORM\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Paneric\Authorization\ORM\Entity\Role;
use Paneric\Authorization\Interfaces\HydratorInterface;
use Paneric\Authorization\Interfaces\RepositoryInterface;

class RoleRepository extends ORMRepository implements RepositoryInterface
{
    public function __construct(EntityManagerInterface $_em)
    {
        $this->entityClass = Role::class;

        parent::__construct($_em);
    }


    public function create(HydratorInterface $roleDTO): ?array
    {
        return $this->queryCreate($roleDTO, new Role());
    }
}
