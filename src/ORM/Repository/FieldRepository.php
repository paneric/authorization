<?php

declare(strict_types=1);

namespace Paneric\Authorization\ORM\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Paneric\Authorization\ORM\Entity\Field;
use Paneric\Authorization\Interfaces\HydratorInterface;
use Paneric\Authorization\Interfaces\RepositoryInterface;

class FieldRepository extends ORMRepository implements RepositoryInterface
{
    public function __construct(EntityManagerInterface $_em)
    {
        $this->entityClass = Field::class;

        parent::__construct($_em);
    }


    public function create(HydratorInterface $fieldDTO): ?array
    {
        return $this->queryCreate($fieldDTO, new Field());
    }
}
