<?php

declare(strict_types=1);

namespace Paneric\Authorization\ORM\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;
use Paneric\Authorization\Interfaces\HydratorInterface;

class ORMRepository extends EntityRepository
{
    protected $entityClass;

    public function __construct(EntityManagerInterface $_em)
    {
        parent::__construct($_em, $_em->getClassMetadata($this->entityClass));
    }


    public function findByIds(array $ids): array
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder->select('t')
            ->from($this->entityClass, 't')
            ->where('t.id = ?0');

        $l = sizeof($ids);

        for ($i = 1; $i < $l; $i++) {
            $queryBuilder->orWhere('t.id = ?' . $i);
        }

        $queryBuilder->setParameters($ids);

        $query = $queryBuilder->getQuery();

        return $query->getResult();
    }

    public function findOneById(int $id): ?object
    {
        return $this->findOneBy(['id' => $id]);
    }

    public function findOneByRef(string $ref): ?object
    {
        return $this->findOneBy(['ref' => $ref]);
    }

    public function findByEnhanced(array $criteria, string $operator = 'AND', int $id = null): ?array
    {
        // TODO: findByEnhanced method body missing !!!
        return null;
    }


    protected function queryCreate(HydratorInterface $hydrator, HydratorInterface $entity): ?array
    {
        try {
            $entity->hydrate($hydrator->convert());

            $this->_em->persist($entity);
            $this->_em->flush();
        } catch (ORMException $e) {
            echo $e->getMessage();
        }

        return null;
    }

    public function update(int $id, HydratorInterface $hydrator): ?array
    {
        try {
            $entity = $this->_em->find($this->entityClass, $id);
            $entity->hydrate($hydrator->convert());

            $this->_em->flush();
        } catch (ORMException $e) {
            echo $e->getMessage();
        }

        return null;
    }

    public function remove(int $id): ?array
    {
        try {
            $entity = $this->_em->find($this->entityClass, $id);

            $this->_em->remove($entity);
            $this->_em->flush();
        } catch (ORMException $e) {
            echo $e->getMessage();
        }

        return null;
    }
}
