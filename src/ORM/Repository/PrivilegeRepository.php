<?php

declare(strict_types=1);

namespace Paneric\Authorization\ORM\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Paneric\Authorization\ORM\Entity\Privilege;
use Paneric\Authorization\Interfaces\HydratorInterface;
use Paneric\Authorization\Interfaces\RepositoryInterface;

class PrivilegeRepository extends ORMRepository implements RepositoryInterface
{
    public function __construct(EntityManagerInterface $_em)
    {
        $this->entityClass = Privilege::class;

        parent::__construct($_em);
    }


    public function create(HydratorInterface $privilegeDTO): ?array
    {
        return $this->queryCreate($privilegeDTO, new Privilege());
    }
}
