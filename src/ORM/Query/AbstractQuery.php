<?php

declare(strict_types=1);

namespace Paneric\Authorization\ORM\Query;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Paneric\Authorization\Interfaces\RepositoryInterface;

abstract class AbstractQuery
{
    protected $_em;
    protected $repository;


    public function __construct(EntityManagerInterface $_em, RepositoryInterface $repository)
    {
        $this->_em = $_em;
        $this->repository = $repository;
    }


    protected function findRelations(int $leadingId, string $relationName): array
    {
        $entity = $this->repository->findOneById($leadingId);

        return $entity->{'get' . ucfirst($relationName) . 's'}();
    }

    public function addRelations(int $leadingId, array $relationsIds, string $relationName): ?array
    {
        $entity = $this->repository->findOneById($leadingId);
        $entity->{'add' . ucfirst($relationName) . 's'}(
            $this->{$relationName . 'Repository'}->findByIds($relationsIds)
        );

        $this->_em->flush();

        return null;
    }

    public function updateRelations(int $leadingId, array $relationsIds, string $relationName): ?array
    {
        $entity = $this->repository->findOneById($leadingId);
        $entity->{'update' . ucfirst($relationName) . 's'}(
            $this->{$relationName . 'Repository'}->findByIds($relationsIds)
        );

        $this->_em->flush();

        return null;
    }

    public function removeRelations(int $leadingId, string $relationName): ?array
    {
        $entity = $this->repository->findOneById($leadingId);
        $entity->{'remove' . ucfirst($relationName) . 's'}();

        $this->_em->flush();

        return null;
    }
}
