<?php

declare(strict_types=1);

namespace Paneric\Authorization\ORM\Query;

use Doctrine\ORM\EntityManagerInterface;
use Paneric\Authorization\Interfaces\AccessQueryInterface;
use Paneric\Authorization\Interfaces\RepositoryInterface;

class AccessQuery extends AbstractQuery implements AccessQueryInterface
{
    protected $privilegeRepository;


    public function __construct(
        EntityManagerInterface $_em,
        RepositoryInterface $accessRepository,
        RepositoryInterface $privilegeRepository
    ) {
        parent::__construct($_em, $accessRepository);

        $this->privilegeRepository = $privilegeRepository;
    }

    public function findPrivileges(int $accessId): array
    {
        return $this->findRelations($accessId, 'privilege');
    }

    public function updatePrivileges(int $accessId, array $privilegesIds): ?array
    {
        return $this->updateRelations($accessId, $privilegesIds, 'privilege');
    }

    public function removePrivileges(int $accessId): ?array
    {
        return $this->removeRelations($accessId,'privilege');
    }
}
