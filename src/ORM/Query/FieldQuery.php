<?php

declare(strict_types=1);

namespace Paneric\Authorization\ORM\Query;

use Doctrine\ORM\EntityManagerInterface;
use Paneric\Authorization\Interfaces\FieldQueryInterface;
use Paneric\Authorization\Interfaces\RepositoryInterface;

class FieldQuery extends AbstractQuery implements FieldQueryInterface
{
    protected $privilegeRepository;


    public function __construct(
        EntityManagerInterface $_em,
        RepositoryInterface $fieldRepository,
        RepositoryInterface $privilegeRepository
    ) {
        parent::__construct($_em, $fieldRepository);

        $this->privilegeRepository = $privilegeRepository;
    }

    public function findPrivileges(int $fieldId): array
    {
        return $this->findRelations($fieldId, 'privilege');
    }

    public function updatePrivileges(int $fieldId, array $privilegesIds): ?array
    {
        return $this->updateRelations($fieldId, $privilegesIds, 'privilege');
    }

    public function removePrivileges(int $fieldId): ?array
    {
        return $this->removeRelations($fieldId,'privilege');
    }
}
