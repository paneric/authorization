<?php

declare(strict_types=1);

namespace Paneric\Authorization\ORM\Query;

use Doctrine\ORM\EntityManagerInterface;
use Paneric\Authorization\Interfaces\RepositoryInterface;
use Paneric\Authorization\Interfaces\RoleQueryInterface;
use Paneric\Authorization\ORM\Entity\Privilege;

class RoleQuery extends AbstractQuery  implements RoleQueryInterface
{
    protected $privilegeRepository;


    public function __construct(
        EntityManagerInterface $_em,
        RepositoryInterface $privilegeRepository,
        RepositoryInterface $roleRepository
    ) {
        parent::__construct($_em, $roleRepository);

        $this->privilegeRepository = $privilegeRepository;
    }


    public function findPrivilegeById(int $roleId, int $privilegeId): ?object
    {
        $role = $this->repository->findOneById($roleId);

        $privileges = $role->getPrivileges();

        $privileges = (array_filter($privileges, static function(Privilege $privilege) use ($privilegeId)
        {
            return $privilege->getId() === $privilegeId;
        }));

        if (empty($privileges)) {
            return null;
        }

        return $privileges[array_keys($privileges)[0]];
    }

    public function findPrivilegeByRoute(int $roleId, string $route): ?object
    {
        $role = $this->repository->findOneById($roleId);

        $privileges = $role->getPrivileges();

        $privileges = (array_filter($privileges, static function(Privilege $privilege) use ($route)
        {
            return $privilege->getRoute() === $route;
        }));

        return $privileges[array_keys($privileges)[0]];
    }


    public function findPrivileges(int $roleId): array
    {
        return $this->findRelations($roleId, 'privilege');
    }

    public function updatePrivileges(int $roleId, array $privilegesIds): ?array
    {
        return $this->updateRelations($roleId, $privilegesIds, 'privilege');
    }

    public function removePrivileges(int $roleId): ?array
    {
        return $this->removeRelations($roleId,'privilege');
    }
}
