<?php

declare(strict_types=1);

namespace Paneric\Authorization\ORM\Query;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Paneric\Authorization\Interfaces\PrivilegeQueryInterface;
use Paneric\Authorization\Interfaces\RepositoryInterface;
use Paneric\Authorization\ORM\Entity\Field;

class PrivilegeQuery extends AbstractQuery implements PrivilegeQueryInterface
{
    protected $fieldRepository;
    protected $roleRepository;
    protected $accessRepository;


    public function __construct(
        EntityManagerInterface $_em,
        RepositoryInterface $fieldRepository,
        RepositoryInterface $privilegeRepository,
        RepositoryInterface $roleRepository,
        RepositoryInterface $accessRepository
    ) {
        parent::__construct($_em, $privilegeRepository);

        $this->fieldRepository = $fieldRepository;
        $this->roleRepository = $roleRepository;
        $this->accessRepository = $accessRepository;
    }


    public function findFieldById(int $privilegeId, int $fieldId): ?object
    {
        $privilege = $this->repository->findOneById($privilegeId);

        $fields = $privilege->getFields();

        $fields = (array_filter($fields, static function(Field $field) use ($fieldId)
        {
            return $field->getId() === $fieldId;
        }));

        return $fields[array_keys($fields)[0]];
    }


    public function findFields(int $privilegeId): array
    {
        return $this->findRelations($privilegeId, 'field');
    }

    public function updateFields(int $privilegeId, array $fieldsIds): ?array
    {
        return $this->updateRelations($privilegeId, $fieldsIds, 'field');
    }

    public function removeFields(int $privilegeId): ?array
    {
        return $this->removeRelations($privilegeId,'field');
    }


    public function findRoles(int $privilegeId): array
    {
        return $this->findRelations($privilegeId, 'role');
    }

    public function updateRoles(int $privilegeId, array $rolesIds): ?array
    {
        return $this->updateRelations($privilegeId, $rolesIds, 'role');
    }

    public function removeRoles(int $privilegeId): ?array
    {
        return $this->removeRelations($privilegeId,'role');
    }


    public function findAccess(int $privilegeId): ?object
    {
        $privilege = $this->repository->findOneById($privilegeId);

        return $privilege->getAccess();
    }

    public function setAccess(int $privilegeId, int $accessId): ?array
    {
        $privilege = $this->repository->findOneById($privilegeId);
        $privilege->updateAccess(
            $this->accessRepository->findOneById($accessId)
        );

        $this->_em->flush();

        return null;
    }

    public function removeAccess(int $privilegeId): ?array
    {
        $privilege = $this->repository->findOneById($privilegeId);
        $privilege->updateAccess(null);

        $this->_em->flush();

        return null;
    }
}
