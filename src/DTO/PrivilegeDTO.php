<?php

declare(strict_types=1);

namespace Paneric\Authorization\DTO;

use Paneric\DTO\DTO;

class PrivilegeDTO extends DTO
{
    protected $id;
    protected $ref;
    protected $route;
    protected $access;
    protected $fields;
    protected $roles;
    protected $info;

    public function updateAccess(?AccessDTO $access): void
    {
        $this->access = $access;
    }
    public function updateFields(array $fields): void
    {
        $this->fields = $fields;
    }
    public function updateRoles(array $roles): void
    {
        $this->roles = $roles;
    }

    public function getId()
    {
        return $this->id;
    }
    public function getRef()
    {
        return $this->ref;
    }
    public function getRoute()
    {
        return $this->route;
    }
    public function getAccess()//? or string or AccessDTO
    {
        return $this->access;
    }
    public function getFields()
    {
        return $this->fields;
    }
    public function getRoles()
    {
        return $this->roles;
    }
    public function getInfo()
    {
        return $this->info;
    }

    public function setId($id): void
    {
        $this->id = is_array($id) ?
            $id :
            (int) $id;
    }
    public function setRef($ref): void
    {
        $this->ref = $ref;
    }
    public function setRoute($route): void
    {
        $this->route = $route;
    }
    public function setAccess($access): void
    {
        $this->access = $access;
    }
    public function setFields($fields): void
    {
        $this->fields = $fields;
    }
    public function setRoles($roles): void
    {
        $this->roles = $roles;
    }
    public function setInfo($info): void
    {
        $this->info = $info;
    }
}
