<?php

declare(strict_types=1);

namespace Paneric\Authorization\DTO;

use Paneric\DTO\DTO;

class AccessDTO extends DTO
{
    protected $id;
    protected $level;
    protected $ref;
    protected $privileges;
    protected $info;

    public function updatePrivileges(array $privileges): void
    {
        $this->privileges = $privileges;
    }

    public function getId()
    {
        return $this->id;
    }
    public function getLevel()
    {
        return $this->level;
    }
    public function getRef()
    {
        return $this->ref;
    }
    public function getPrivileges()
    {
        return $this->privileges;
    }
    public function getInfo()
    {
        return $this->info;
    }

    public function setId($id): void
    {
        $this->id = is_array($id) ?
            $id :
            (int) $id;
    }
    public function setLevel($level): void
    {
        $this->level = is_array($level) ?
            $level :
            (int) $level;
    }
    public function setRef($ref): void
    {
        $this->ref = $ref;
    }
    public function setPrivileges($privileges): void
    {
        $this->privileges = $privileges;
    }
    public function setInfo($info): void
    {
        $this->info = $info;
    }
}
