SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `accesses` (
                            `id` int(11) NOT NULL,
                            `level` int(2) NOT NULL,
                            `ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                            `info` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                            `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)',
                            `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `fields` (
                          `id` int(11) NOT NULL,
                          `ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                          `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)',
                          `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `privileges` (
                              `id` int(11) NOT NULL,
                              `access_id` int(11) DEFAULT NULL,
                              `ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                              `route` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                              `info` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                              `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)',
                              `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `privileges_fields` (
                                     `privilege_id` int(11) NOT NULL,
                                     `field_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `roles` (
                         `id` int(11) NOT NULL,
                         `ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                         `info` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                         `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)',
                         `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `roles_privileges` (
                                    `role_id` int(11) NOT NULL,
                                    `privilege_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `accesses`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `level_idx` (`level`),
    ADD UNIQUE KEY `ref_idx` (`ref`),
    ADD KEY `created_at_idx` (`created_at`),
    ADD KEY `updated_at_idx` (`updated_at`);

ALTER TABLE `fields`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `ref_idx` (`ref`),
    ADD KEY `created_at_idx` (`created_at`),
    ADD KEY `updated_at_idx` (`updated_at`);

ALTER TABLE `privileges`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `ref_idx` (`ref`),
    ADD KEY `IDX_6855F9124FEA67CF` (`access_id`),
    ADD KEY `route_idx` (`route`),
    ADD KEY `created_at_idx` (`created_at`),
    ADD KEY `updated_at_idx` (`updated_at`);

ALTER TABLE `privileges_fields`
    ADD PRIMARY KEY (`privilege_id`,`field_id`),
    ADD KEY `IDX_4B6B5E6A32FB8AEA` (`privilege_id`),
    ADD KEY `IDX_4B6B5E6A443707B0` (`field_id`);

ALTER TABLE `roles`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `ref_idx` (`ref`),
    ADD KEY `created_at_idx` (`created_at`),
    ADD KEY `updated_at_idx` (`updated_at`);

ALTER TABLE `roles_privileges`
    ADD PRIMARY KEY (`role_id`,`privilege_id`),
    ADD KEY `IDX_2472C79AD60322AC` (`role_id`),
    ADD KEY `IDX_2472C79A32FB8AEA` (`privilege_id`);

ALTER TABLE `accesses`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

ALTER TABLE `fields`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

ALTER TABLE `privileges`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

ALTER TABLE `roles`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

ALTER TABLE `privileges`
    ADD CONSTRAINT `FK_6855F9124FEA67CF` FOREIGN KEY (`access_id`) REFERENCES `accesses` (`id`);

ALTER TABLE `privileges_fields`
    ADD CONSTRAINT `FK_4B6B5E6A32FB8AEA` FOREIGN KEY (`privilege_id`) REFERENCES `privileges` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `FK_4B6B5E6A443707B0` FOREIGN KEY (`field_id`) REFERENCES `fields` (`id`) ON DELETE CASCADE;

ALTER TABLE `roles_privileges`
    ADD CONSTRAINT `FK_2472C79A32FB8AEA` FOREIGN KEY (`privilege_id`) REFERENCES `privileges` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `FK_2472C79AD60322AC` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
