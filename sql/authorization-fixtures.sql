SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

CREATE TABLE `accesses` (
                            `id` int(11) NOT NULL,
                            `ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                            `info` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                            `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)',
                            `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `accesses` (`id`, `ref`, `info`, `created_at`, `updated_at`) VALUES
(1, 'access1', NULL, '2019-12-03 21:45:17', '2019-12-03 21:45:17'),
(2, 'access2', NULL, '2019-12-03 21:45:17', '2019-12-03 21:45:17');

CREATE TABLE `fields` (
                          `id` int(11) NOT NULL,
                          `ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                          `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)',
                          `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `fields` (`id`, `ref`, `created_at`, `updated_at`) VALUES
(1, 'field1', '2019-12-03 21:45:17', '2019-12-03 21:45:17'),
(2, 'field2', '2019-12-03 21:45:17', '2019-12-03 21:45:17'),
(3, 'field3', '2019-12-03 21:45:17', '2019-12-03 21:45:17'),
(4, 'field4', '2019-12-03 21:45:17', '2019-12-03 21:45:17');

CREATE TABLE `privileges` (
                              `id` int(11) NOT NULL,
                              `access_id` int(11) DEFAULT NULL,
                              `ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                              `route` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                              `info` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                              `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)',
                              `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `privileges` (`id`, `access_id`, `ref`, `route`, `info`, `created_at`, `updated_at`) VALUES
(1, 1, 'privilege1', 'route1', NULL, '2019-12-03 21:45:17', '2019-12-03 21:45:17'),
(2, 2, 'privilege2', 'route2', NULL, '2019-12-03 21:45:17', '2019-12-03 21:45:17'),
(3, 1, 'privilege3', 'route3', NULL, '2019-12-03 21:45:17', '2019-12-03 21:45:17'),
(4, 2, 'privilege4', 'route4', NULL, '2019-12-03 21:45:17', '2019-12-03 21:45:17');

CREATE TABLE `privileges_fields` (
                                     `privilege_id` int(11) NOT NULL,
                                     `field_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `privileges_fields` (`privilege_id`, `field_id`) VALUES
(1, 2),
(1, 3),
(2, 1),
(2, 3),
(3, 1),
(3, 2),
(4, 1),
(4, 3);

CREATE TABLE `roles` (
                         `id` int(11) NOT NULL,
                         `ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                         `info` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                         `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)',
                         `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `roles` (`id`, `ref`, `info`, `created_at`, `updated_at`) VALUES
(1, 'role1', NULL, '2019-12-03 21:45:17', '2019-12-03 21:45:17'),
(2, 'role2', NULL, '2019-12-03 21:45:17', '2019-12-03 21:45:17'),
(3, 'role3', NULL, '2019-12-03 21:45:17', '2019-12-03 21:45:17'),
(4, 'role4', NULL, '2019-12-03 21:45:17', '2019-12-03 21:45:17');

CREATE TABLE `roles_privileges` (
                                    `role_id` int(11) NOT NULL,
                                    `privilege_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `roles_privileges` (`role_id`, `privilege_id`) VALUES
(1, 2),
(1, 3),
(2, 1),
(2, 3),
(3, 1),
(3, 2),
(4, 1),
(4, 3);

ALTER TABLE `accesses`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `ref_idx` (`ref`),
    ADD KEY `created_at_idx` (`created_at`),
    ADD KEY `updated_at_idx` (`updated_at`);

ALTER TABLE `fields`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `ref_idx` (`ref`),
    ADD KEY `created_at_idx` (`created_at`),
    ADD KEY `updated_at_idx` (`updated_at`);

ALTER TABLE `privileges`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `ref_idx` (`ref`),
    ADD KEY `IDX_6855F9124FEA67CF` (`access_id`),
    ADD KEY `route_idx` (`route`),
    ADD KEY `created_at_idx` (`created_at`),
    ADD KEY `updated_at_idx` (`updated_at`);

ALTER TABLE `privileges_fields`
    ADD PRIMARY KEY (`privilege_id`,`field_id`),
    ADD KEY `IDX_4B6B5E6A32FB8AEA` (`privilege_id`),
    ADD KEY `IDX_4B6B5E6A443707B0` (`field_id`);

ALTER TABLE `roles`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `ref_idx` (`ref`),
    ADD KEY `created_at_idx` (`created_at`),
    ADD KEY `updated_at_idx` (`updated_at`);

ALTER TABLE `roles_privileges`
    ADD PRIMARY KEY (`role_id`,`privilege_id`),
    ADD KEY `IDX_2472C79AD60322AC` (`role_id`),
    ADD KEY `IDX_2472C79A32FB8AEA` (`privilege_id`);

ALTER TABLE `accesses`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

ALTER TABLE `fields`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

ALTER TABLE `privileges`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

ALTER TABLE `roles`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

ALTER TABLE `privileges`
    ADD CONSTRAINT `FK_6855F9124FEA67CF` FOREIGN KEY (`access_id`) REFERENCES `accesses` (`id`);

ALTER TABLE `privileges_fields`
    ADD CONSTRAINT `FK_4B6B5E6A32FB8AEA` FOREIGN KEY (`privilege_id`) REFERENCES `privileges` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `FK_4B6B5E6A443707B0` FOREIGN KEY (`field_id`) REFERENCES `fields` (`id`) ON DELETE CASCADE;

ALTER TABLE `roles_privileges`
    ADD CONSTRAINT `FK_2472C79A32FB8AEA` FOREIGN KEY (`privilege_id`) REFERENCES `privileges` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `FK_2472C79AD60322AC` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;