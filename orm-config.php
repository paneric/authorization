<?php

declare(strict_types=1);

return [
    // you should add any other path containing annotated entity classes
    'paths' => [
        __DIR__ . '/src/ORM/Entity',
    ],

    // if true, metadata caching is forcefully disabled
    'is_dev_mode' => true,

    'proxy_dir' => null,

    'cache' => null,

    'use_simple_annotation_reader' => false,

    // path where the compiled metadata info will be cached
    // make sure the path exists and it is writable
    'cache_dir' => __DIR__ . '/var/cache/doctrine',

    'connection' => [
        'driver' => 'pdo_mysql',
        'host' => '127.0.0.1',
        'port' => 3306,
        'dbname' => 'authorization',
        'user' => 'root',
        'password' => 'root',
        'charset' => 'UTF8'
    ]
];
