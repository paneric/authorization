<?php

return [
    /* common */

    'main' => 'Strona główna',
    'authorization' => 'Autoryzacja',
    'start' => 'Start',  
    'access' => 'Dostęp',
    'accesses' => 'Dostępy',
    'field' => 'Pole',
    'fields' => 'Pola',
    'privilege' =>  'Przywilej',
    'privileges' => 'Przywileje',
    'role' => 'Rola',
    'roles' => 'Role',
    'level' => 'Poziom',
    'ref' => 'Referencja',
    'info' => 'Opis',
    'route' => 'Ścieżka',
    
    'cancel' => 'Anuluj',
    'add' => 'Dodaj',
    'confirm' => 'Potwierdź',
    'modify' => 'Modyfikuj',
    'update' => 'Aktualizuj',
    'delete' => 'Usuń',
    'choose' => 'Wybierz...',
    'back' => 'Wróć',

    'access_level_help' => 'Wprowadź liczbę naturalną dla oznaczenia poziomu dostępu',
    'access_ref_help' => 'Wprowadź krotka referencje poziomu dostępu',
    'access_info_help' => 'Wprowadź krotki opis poziomu dostęp',
    'access_help' => 'Wybierz poziom dostępu',
    
    'field_ref_help' => 'Wprowadź nazwę pola bazy danych',

    'privilege_ref_help' => 'Wprowadź krótką referencję przywileju',
    'privilege_route_help' => 'Wprowadź ścieżkę związany z przywilejem',
    'privilege_info_help' => 'Wprowadź krotki opis przywileju',

    'role_ref_help' => 'Wprowadź krótką referencję dla roli',
    'role_info_help' => 'Wprowadź opis roli',
    
    /* autho.access.add */

    'form_access_add_title' => 'Dodaj nowy poziom dostępu',

    'db_access_add_error' => 'Jedna lub dwie wartości (poziom lub referencja) są już użyte.',
    
    /* 'autho.access.show' */
    
    'content_accesses_show_title' => 'Poziomy dostępu',
    
    /* autho.access.edit */

    'form_access_edit_title' => 'Modyfikuj poziom dostępu',

    'db_access_edit_error' => 'Jedna lub dwie wartości (poziom lub referencja) są już użyte.',

    /* autho.access.delete */

    'form_access_delete_title' => 'Usuń poziom dostępu',
    
    'access_delete_warning' => 'Zamierzasz usunąć poziom dostępu. Wymagane potwierdzenie.',
    
    /* autho.field.add */

    'form_field_add_title' => 'Dodaj nowe pole',

    'db_field_add_error' => 'Rerefencja pola jest już użyta.',

    /* 'autho.fields.show' */

    'content_fields_show_title' => 'Pola',

    /* autho.field.edit */

    'form_field_edit_title' => 'Modyfikuj pole',

    'db_field_edit_error' => 'Rerefencja pola jest już użyta.',

    /* autho.field.delete */

    'form_field_delete_title' => 'Usuń pole',

    'field_delete_warning' => 'Zamierzasz usunąć pole. Wymagane potwierdzenie.',

    /* autho.privilege.add */

    'form_privilege_add_title' => 'Dodaj nowy przywilej',

    'db_privilege_add_error' => 'Rerefencja przywileju jest już użyta.',

    /* 'autho.privileges.show' */

    'content_privileges_show_title' => 'Przywileje',

    /* autho.privilege.edit */

    'form_privilege_edit_title' => 'Modyfikuj przywilej',

    'db_privilege_edit_error' => 'Rerefencja przywileju jest już użyta.',

    /* autho.privilege.delete */

    'form_privilege_delete_title' => 'Usuń przywilej',

    'privilege_delete_warning' => 'Zamierzasz usunąć przywilej. Wymagane potwierdzenie.',

    /* autho.role.add */

    'form_role_add_title' => 'Dodaj nową rolę',

    'db_role_add_error' => 'Rerefencja roli jest już użyta.',

    /* 'autho.roles.show' */

    'content_roles_show_title' => 'Role',

    /* autho.role.edit */

    'form_role_edit_title' => 'Modyfikuj rolę',

    'db_role_edit_error' => 'Rerefencja roli jest już użyta.',

    /* autho.role.delete */

    'form_role_delete_title' => 'Usuń rolę',

    'role_delete_warning' => 'Zamierzasz usunąć rolę. Wymagane potwierdzenie.',

    /* autho.role.privileges.edit */

    'form_role_privileges_edit_title' => 'Role -> przywileje',

    /* autho.privilege.roles.edit */

    'form_privilege_roles_edit_title' => 'Przywilej -> role',

    /* autho.privilege.fields.edit */

    'form_privilege_fields_edit_title' => 'Przywilej -> pola',

    /* autho.field.privileges.edit */

    'form_field_privileges_edit_title' => 'Pole -> przywileje',

    /* autho.access.privileges.edit */

    'form_access_privileges_edit_title' => 'Poziom dostępu -> przywileje',
    
    /* autho.role.privileges.delete */

    'form_role_privileges_delete_title' => 'Usuń relacje przywilejów do roli.',
    'role_privileges_delete_warning' => 'Zamierzasz usunąć relacje przywilejów do roli. Wymagane potwierdzenie.',

    /* autho.privilege.roles.delete */

    'form_privilege_roles_delete_title' => 'Usuń relacje ról do przywileju.',
    'privilege_roles_delete_warning' => 'Zamierzasz usunąć relacje ról do przywileju. Wymagane potwierdzenie.',

    /* autho.privilege.fields.delete */

    'form_privilege_fields_delete_title' => 'Usuń relacje pól do przywileju.',
    'privilege_fields_delete_warning' => 'Zamierzasz usunąć relacje pól do przywileju. Wymagane potwierdzenie.',

    /* autho.field.privileges.delete */

    'form_field_privileges_delete_title' => 'Usuń relacje przywilejów do pola.',
    'field_privileges_delete_warning' => 'Zamierzasz usunąć relacje przywilejów do pola. Wymagane potwierdzenie.',

    /* autho.access.privileges.delete */

    'form_access_privileges_delete_title' => 'Usuń relacje przywilejów do poziomu dostępu.',
    'access_privileges_delete_warning' => 'Zamierzasz usunąć relacje przywilejów do poziomu dostępu. Wymagane potwierdzenie.',
];
