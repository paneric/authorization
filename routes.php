<?php

declare(strict_types=1);

use Paneric\Authorization\Controller\Client\AccessController;
use Paneric\Authorization\Controller\Client\PrivilegeController;
use Paneric\Authorization\Controller\Client\RoleController;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Paneric\Authorization\Controller\Client\FieldController;
use Paneric\Middleware\CSRFMiddleware;
use Paneric\Validation\ValidationMiddleware;
use Paneric\Middleware\PaginationMiddleware;

$app->get('/autho/accesses/show[/{page}]', function (Request $request, Response $response) {
    return $this->get(AccessController::class)->showAll($response);
})->setName('autho.accesses.show')
    ->addMiddleware($container->get(PaginationMiddleware::class));

$app->get('/autho/access/{id}/show', function (Request $request, Response $response, array $args) {
    return $this->get(AccessController::class)->showOneById($response, (int) $args['id']);
})->setName('autho.access.show');

$app->map(['GET', 'POST'], '/autho/access/add', function (Request $request, Response $response) {
    return $this->get(AccessController::class)->add($request, $response);
})->setName('autho.access.add')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));

$app->map(['GET', 'POST'], '/autho/access/{id}/edit', function (Request $request, Response $response, array $args) {
    return $this->get(AccessController::class)->edit($request, $response, (int) $args['id']);
})->setName('autho.access.edit')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));

$app->map(['GET', 'POST'], '/autho/access/{id}/delete', function (Request $request, Response $response, array $args) {
    return $this->get(AccessController::class)->delete($request, $response, (int) $args['id']);
})->setName('autho.access.delete')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));

$app->get('/autho/access/{id}/privileges/show', function (Request $request, Response $response, array $args) {
    return $this->get(AccessController::class)->showPrivileges($response, (int) $args['id']);
})->setName('autho.access.privileges.show');

$app->map(['GET', 'POST'], '/autho/access/{id}/privileges/edit', function (Request $request, Response $response, array $args) {
    return $this->get(AccessController::class)->editPrivileges($request, $response, (int) $args['id']);
})->setName('autho.access.privileges.edit')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));

$app->map(['GET', 'POST'], '/autho/access/{id}/privileges/delete', function (Request $request, Response $response, array $args) {
    return $this->get(AccessController::class)->deletePrivileges($request, $response, (int) $args['id']);
})->setName('autho.access.privileges.delete')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));


$app->get('/autho/fields/show[/{page}]', function (Request $request, Response $response) {
    return $this->get(FieldController::class)->showAll($response);
})->setName('autho.fields.show')
    ->addMiddleware($container->get(PaginationMiddleware::class));

$app->get('/autho/field/{id}/show', function (Request $request, Response $response, array $args) {
    return $this->get(FieldController::class)->showOneById($response, (int) $args['id']);
})->setName('autho.field.show');

$app->map(['GET', 'POST'], '/autho/field/add', function (Request $request, Response $response) {
    return $this->get(FieldController::class)->add($request, $response);
})->setName('autho.field.add')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));

$app->map(['GET', 'POST'], '/autho/field/{id}/edit', function (Request $request, Response $response, array $args) {
    return $this->get(FieldController::class)->edit($request, $response, (int) $args['id']);
})->setName('autho.field.edit')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));

$app->map(['GET', 'POST'], '/autho/field/{id}/delete', function (Request $request, Response $response, array $args) {
    return $this->get(FieldController::class)->delete($request, $response, (int) $args['id']);
})->setName('autho.field.delete')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));

$app->map(['GET', 'POST'], '/autho/field/{id}/privileges/edit', function (Request $request, Response $response, array $args) {
    return $this->get(FieldController::class)->editPrivileges($request, $response, (int) $args['id']);
})->setName('autho.field.privileges.edit')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));

$app->map(['GET', 'POST'], '/autho/field/{id}/privileges/delete', function (Request $request, Response $response, array $args) {
    return $this->get(FieldController::class)->deletePrivileges($request, $response, (int) $args['id']);
})->setName('autho.field.privileges.delete')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));


$app->get('/autho/privileges/show[/{page}]', function (Request $request, Response $response) {
    return $this->get(PrivilegeController::class)->showAll($response);
})->setName('autho.privileges.show')
    ->addMiddleware($container->get(PaginationMiddleware::class));

$app->get('/autho/privilege/{id}/show', function (Request $request, Response $response, array $args) {
    return $this->get(PrivilegeController::class)->showOneById($response, (int) $args['id']);
})->setName('autho.privilege.show');

$app->map(['GET', 'POST'], '/autho/privilege/add', function (Request $request, Response $response) {
    return $this->get(PrivilegeController::class)->add($request, $response);
})->setName('autho.privilege.add')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));

$app->map(['GET', 'POST'], '/autho/privilege/{id}/edit', function (Request $request, Response $response, array $args) {
    return $this->get(PrivilegeController::class)->edit($request, $response, (int) $args['id']);
})->setName('autho.privilege.edit')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));

$app->map(['GET', 'POST'], '/autho/privilege/{id}/delete', function (Request $request, Response $response, array $args) {
    return $this->get(PrivilegeController::class)->delete($request, $response, (int) $args['id']);
})->setName('autho.privilege.delete')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));

$app->map(['GET', 'POST'], '/autho/privilege/{id}/fields/edit', function (Request $request, Response $response, array $args) {
    return $this->get(PrivilegeController::class)->editFields($request, $response, (int) $args['id']);
})->setName('autho.privilege.fields.edit')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));

$app->map(['GET', 'POST'], '/autho/privilege/{id}/roles/edit', function (Request $request, Response $response, array $args) {
    return $this->get(PrivilegeController::class)->editRoles($request, $response, (int) $args['id']);
})->setName('autho.privilege.roles.edit')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));

$app->map(['GET', 'POST'], '/autho/privilege/{id}/fields/delete', function (Request $request, Response $response, array $args) {
    return $this->get(PrivilegeController::class)->deleteFields($request, $response, (int) $args['id']);
})->setName('autho.privilege.fields.delete')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));

$app->map(['GET', 'POST'], '/autho/privilege/{id}/roles/delete', function (Request $request, Response $response, array $args) {
    return $this->get(PrivilegeController::class)->deleteRoles($request, $response, (int) $args['id']);
})->setName('autho.privilege.roles.delete')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));


$app->get('/autho/roles/show[/{page}]', function (Request $request, Response $response) {
    return $this->get(RoleController::class)->showAll($response);
})->setName('autho.roles.show')
    ->addMiddleware($container->get(PaginationMiddleware::class));

$app->get('/autho/role/{id}/show', function (Request $request, Response $response, array $args) {
    return $this->get(RoleController::class)->showOneById($response, (int) $args['id']);
})->setName('autho.role.show');

$app->map(['GET', 'POST'], '/autho/role/add', function (Request $request, Response $response) {
    return $this->get(RoleController::class)->add($request, $response);
})->setName('autho.role.add')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));

$app->map(['GET', 'POST'], '/autho/role/{id}/edit', function (Request $request, Response $response, array $args) {
    return $this->get(RoleController::class)->edit($request, $response, (int) $args['id']);
})->setName('autho.role.edit')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));

$app->map(['GET', 'POST'], '/autho/role/{id}/delete', function (Request $request, Response $response, array $args) {
    return $this->get(RoleController::class)->delete($request, $response, (int) $args['id']);
})->setName('autho.role.delete')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));

$app->map(['GET', 'POST'], '/autho/role/{id}/privileges/edit', function (Request $request, Response $response, array $args) {
    return $this->get(RoleController::class)->editPrivileges($request, $response, (int) $args['id']);
})->setName('autho.role.privileges.edit')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));

$app->map(['GET', 'POST'], '/autho/role/{id}/privileges/delete', function (Request $request, Response $response, array $args) {
    return $this->get(RoleController::class)->deletePrivileges($request, $response, (int) $args['id']);
})->setName('autho.role.privileges.delete')
    ->addMiddleware($container->get(ValidationMiddleware::class))
    ->addMiddleware($container->get(CSRFMiddleware::class));
