<?php

namespace spec\Paneric\Authorization;

use Paneric\Authorization\AuthorizationMiddleware;
use Paneric\Authorization\DTO\PrivilegeDTO;
use Paneric\Authorization\DTO\RoleDTO;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface;
use Paneric\Authorization\Interfaces\RepositoryInterface;
use Paneric\Interfaces\Session\SessionInterface;
use PhpSpec\ObjectBehavior;

class AuthorizationMiddlewareSpec extends ObjectBehavior
{
    public function let(
        RepositoryInterface $roleRepository,
        RepositoryInterface $privilegeRepository,
        SessionInterface $session
    ): void {
        $this->beConstructedWith($roleRepository, $privilegeRepository, $session);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(AuthorizationMiddleware::class);
    }

    public function it_processes(
        Request $request,
        Response $response,
        RequestHandlerInterface $handler,
        RepositoryInterface $privilegeRepository,
        RepositoryInterface $roleRepository,
        SessionInterface $session,
        RoleDTO $role,
        PrivilegeDTO $privilege
    ): void {
        $request->getAttribute('route_name')->willReturn('some_route');
        $privilegeRepository->findOneByRef('some_route')->willReturn($privilege);

        $session->getData('authentication')->willReturn(['role_id' => 1]);
        $roleRepository->findOneById(1)->willReturn($role);

        $privilege->getRoles()->willReturn([$role]);
        $role->getId()->willReturn(1);

        $request->withAttribute('authorization', $privilege)->shouldBeCalled();
        $handler->handle($request)->willReturn($response);


        $this->process($request, $handler)->shouldReturn($response);
    }
}
