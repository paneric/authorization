<?php

namespace spec\Paneric\Authorization\Controller\API;

use JsonSerializable;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Authorization\Service\RoleService;
use Paneric\Authorization\Controller\API\RoleAPIController;
use PhpSpec\ObjectBehavior;
use Psr\Http\Message\StreamInterface;
use Prophecy\Argument;

class RoleAPIControllerSpec extends ObjectBehavior
{
    public function let(RoleService $service): void
    {
        $this->beConstructedWith($service);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(RoleAPIController::class);
    }


    public function it_gets_privilege_by_id(
        RoleService $service,
        JsonSerializable $jsonSerializable,
        Response $response,
        StreamInterface $body
    ): void {
        $service->getPrivilegeById(1, 1)->willReturn($jsonSerializable);
        $jsonSerializable->jsonSerialize()->willReturn(['ref' => 'ref1']);

        $this->specJsonResponse($response, $body);

        $this->getPrivilegeById($response, 1, 1)->shouldReturn($response);
    }


    public function it_gets_privileges(
        RoleService $service,
        Response $response,
        StreamInterface $body
    ): void {
        $service->getPrivileges(1)->willReturn([['ref' => 'ref1']]);

        $this->specJsonResponse($response, $body);

        $this->getPrivileges($response, 1)->shouldReturn($response);
    }

    public function it_updates_privileges(
        RoleService $service,
        Request $request,
        Response $response,
        StreamInterface $body
    ): void {
        $service->updatePrivileges(1, $request)->willReturn(null);

        $this->specJsonResponse($response, $body);

        $this->updatePrivileges($request, $response, 1)->shouldReturn($response);
    }

    public function it_deletes_privileges(
        RoleService $service,
        Request $request,
        Response $response,
        StreamInterface $body
    ): void {
        $service->deletePrivileges($request, 1)->willReturn(null);

        $this->specJsonResponse($response, $body);

        $this->deletePrivileges($request, $response, 1)->shouldReturn($response);
    }


    private function specJsonResponse(
        Response $response,
        StreamInterface $body
    ): void {
        $response->getBody()->willReturn($body);
        $body->write(Argument::type('string'))->shouldBeCalled();
        $response->withHeader(Argument::type('string'), Argument::type('string'))->willReturn($response);
        $response->withStatus(Argument::type('integer'))->willReturn($response);
    }
}