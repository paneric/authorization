<?php

namespace spec\Paneric\Authorization\Controller\API;

use JsonSerializable;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Authorization\Service\PrivilegeService;
use Paneric\Authorization\Controller\API\PrivilegeAPIController;
use PhpSpec\ObjectBehavior;
use Psr\Http\Message\StreamInterface;
use Prophecy\Argument;

class PrivilegeAPIControllerSpec extends ObjectBehavior
{
    public function let(PrivilegeService $service): void
    {
        $this->beConstructedWith($service);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(PrivilegeAPIController::class);
    }


    public function it_gets_field_by_id(
        PrivilegeService $service,
        JsonSerializable $jsonSerializable,
        Response $response,
        StreamInterface $body
    ): void {
        $service->getFieldById(1, 1)->willReturn($jsonSerializable);
        $jsonSerializable->jsonSerialize()->willReturn(['ref' => 'ref1']);

        $this->specJsonResponse($response, $body);

        $this->getFieldById($response, 1, 1)->shouldReturn($response);
    }


    public function it_gets_access(
        PrivilegeService $service,
        JsonSerializable $jsonSerializable,
        Response $response,
        StreamInterface $body
    ): void {
        $service->getAccess(1)->willReturn($jsonSerializable);
        $jsonSerializable->jsonSerialize()->willReturn(['ref' => 'ref1']);

        $this->specJsonResponse($response, $body);

        $this->getAccess($response, 1)->shouldReturn($response);
    }

    public function it_sets_access(
        PrivilegeService $service,
        Request $request,
        Response $response,
        StreamInterface $body
    ): void {
        $service->setAccess(1, $request)->willReturn(null);

        $this->specJsonResponse($response, $body);

        $this->setAccess($request, $response, 1)->shouldReturn($response);
    }

    public function it_deletes_access(
        PrivilegeService $service,
        Response $response,
        StreamInterface $body
    ): void {
        $service->deleteAccess(1)->willReturn(null);

        $this->specJsonResponse($response, $body);

        $this->deleteAccess($response, 1)->shouldReturn($response);
    }


    public function it_gets_fields(
        PrivilegeService $service,
        Response $response,
        StreamInterface $body
    ): void {
        $service->getFields(1)->willReturn([['ref' => 'ref1']]);

        $this->specJsonResponse($response, $body);

        $this->getFields($response, 1)->shouldReturn($response);
    }

    public function it_updates_fields(
        PrivilegeService $service,
        Request $request,
        Response $response,
        StreamInterface $body
    ): void {
        $service->updateFields(1, $request)->willReturn(null);

        $this->specJsonResponse($response, $body);

        $this->updateFields($request, $response, 1)->shouldReturn($response);
    }

    public function it_deletes_fields(
        PrivilegeService $service,
        Request $request,
        Response $response,
        StreamInterface $body
    ): void {
        $service->deleteFields($request,1)->willReturn(null);

        $this->specJsonResponse($response, $body);

        $this->deleteFields($request, $response, 1)->shouldReturn($response);
    }


    public function it_gets_roles(
        PrivilegeService $service,
        Response $response,
        StreamInterface $body
    ): void {
        $service->getRoles(1)->willReturn([['ref' => 'ref1']]);

        $this->specJsonResponse($response, $body);

        $this->getRoles($response, 1)->shouldReturn($response);
    }

    public function it_updates_roles(
        PrivilegeService $service,
        Request $request,
        Response $response,
        StreamInterface $body
    ): void {
        $service->updateRoles(1, $request)->willReturn(null);

        $this->specJsonResponse($response, $body);

        $this->updateRoles($request, $response, 1)->shouldReturn($response);
    }

    public function it_deletes_roles(
        PrivilegeService $service,
        Request $request,
        Response $response,
        StreamInterface $body
    ): void {
        $service->deleteRoles($request, 1)->willReturn(null);

        $this->specJsonResponse($response, $body);

        $this->deleteRoles($request, $response, 1)->shouldReturn($response);
    }


    private function specJsonResponse(
        Response $response,
        StreamInterface $body
    ): void {
        $response->getBody()->willReturn($body);
        $body->write(Argument::type('string'))->shouldBeCalled();
        $response->withHeader(Argument::type('string'), Argument::type('string'))->willReturn($response);
        $response->withStatus(Argument::type('integer'))->willReturn($response);
    }
}
