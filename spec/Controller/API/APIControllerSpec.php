<?php

namespace spec\Paneric\Authorization\Controller\API;

use JsonSerializable;
use Paneric\Authorization\Controller\API\APIController;
use Paneric\Authorization\Interfaces\ServiceInterface;
use Paneric\Authorization\Service\AccessService;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\StreamInterface;

class APIControllerSpec extends ObjectBehavior
{
    public function let(ServiceInterface $service): void
    {
        $this->beConstructedWith($service);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(APIController::class);
    }


    public function it_gets_all(
        AccessService $service,
        Response $response,
        StreamInterface $body
    ): void {
        $service->getAll()->willReturn([['ref' => 'ref1']]);

        $this->specJsonResponse($response, $body);

        $this->getAll($response)->shouldReturn($response);
    }

    public function it_gets_by_ids(
        AccessService $service,
        Request $request,
        Response $response,
        StreamInterface $body
    ): void {
        $service->getByIds($request)->willReturn([['ref' => 'ref1']]);

        $this->specJsonResponse($response, $body);

        $this->getByIds($request, $response)->shouldReturn($response);
    }

    public function it_gets_one_by_id(
        AccessService $service,
        Response $response,
        StreamInterface $body,
        JsonSerializable $jsonSerializable
    ): void {
        $service->getOneById(1)->willReturn($jsonSerializable);
        $jsonSerializable->jsonSerialize()->willReturn(['ref' => 'ref1']);

        $this->specJsonResponse($response, $body);

        $this->getOneById($response, 1)->shouldReturn($response);
    }

    public function it_creates(
        AccessService $service,
        Request $request,
        Response $response,
        StreamInterface $body
    ): void {
        $service->create($request)->willReturn(null);

        $this->specJsonResponse($response, $body);

        $this->create($request, $response)->shouldReturn($response);
    }

    public function it_updates(
        AccessService $service,
        Request $request,
        Response $response,
        StreamInterface $body
    ): void {
        $service->update(1, $request)->willReturn(null);

        $this->specJsonResponse($response, $body);

        $this->update($request, $response, 1)->shouldReturn($response);
    }

    public function it_deletes(
        AccessService $service,
        Request $request,
        Response $response,
        StreamInterface $body
    ): void {
        $service->delete($request, 1)->willReturn(null);

        $this->specJsonResponse($response, $body);

        $this->delete($request, $response, 1)->shouldReturn($response);
    }


    private function specJsonResponse(
        Response $response,
        StreamInterface $body
    ): void {
        $response->getBody()->willReturn($body);
        $body->write(Argument::type('string'))->shouldBeCalled();
        $response->withHeader(Argument::type('string'), Argument::type('string'))->willReturn($response);
        $response->withStatus(Argument::type('integer'))->willReturn($response);
    }
}
