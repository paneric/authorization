<?php

namespace spec\Paneric\Authorization\Controller\API;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Authorization\Service\AccessService;
use Paneric\Authorization\Controller\API\AccessAPIController;
use PhpSpec\ObjectBehavior;
use Psr\Http\Message\StreamInterface;
use Prophecy\Argument;

class AccessAPIControllerSpec extends ObjectBehavior
{
    public function let(AccessService $service): void
    {
        $this->beConstructedWith($service);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(AccessAPIController::class);
    }


    public function it_gets_privileges(
        AccessService $service,
        Response $response,
        StreamInterface $body
    ): void {
        $service->getPrivileges(1)->willReturn([['ref' => 'ref1']]);

        $this->specJsonResponse($response, $body);

        $this->getPrivileges($response, 1)->shouldReturn($response);
    }

    public function it_updates_privileges(
        AccessService $service,
        Request $request,
        Response $response,
        StreamInterface $body
    ): void {
        $service->updatePrivileges(1, $request)->willReturn(null);

        $this->specJsonResponse($response, $body);

        $this->updatePrivileges($request, $response, 1)->shouldReturn($response);
    }

    public function it_deletes_privileges(
        AccessService $service,
        Request $request,
        Response $response,
        StreamInterface $body
    ): void {
        $service->deletePrivileges($request, 1)->willReturn(null);

        $this->specJsonResponse($response, $body);

        $this->deletePrivileges($request, $response, 1)->shouldReturn($response);
    }


    private function specJsonResponse(
        Response $response,
        StreamInterface $body
    ): void {
        $response->getBody()->willReturn($body);
        $body->write(Argument::type('string'))->shouldBeCalled();
        $response->withHeader(Argument::type('string'), Argument::type('string'))->willReturn($response);
        $response->withStatus(Argument::type('integer'))->willReturn($response);
    }
}
