<?php /** @noinspection AccessModifierPresentedInspection */

namespace spec\Paneric\Authorization\Controller\Client;

use JsonSerializable;
use Paneric\Authorization\Controller\Client\ClientController;
use Paneric\Authorization\Interfaces\ServiceInterface;
use Paneric\Authorization\Service\AccessService;
use Prophecy\Argument;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\StreamInterface;
use Twig\Environment as Twig;
use PhpSpec\ObjectBehavior;

class ClientControllerSpec extends ObjectBehavior
{
    public function let(ServiceInterface $service, Twig $twig): void
    {
        $this->beConstructedWith($service, $twig);
    }

    function it_is_initializable(): void
    {
        $this->shouldHaveType(ClientController::class);
    }


    public function it_shows_all(
        ServiceInterface $service,
        Response $response,
        Twig $twig,
        StreamInterface $body
    ): void {
        $service->getAll()->willReturn([['ref' => 'ref1']]);

        $this->specRender($response, $twig, $body);

        $this->showAll($response)->shouldReturn($response);
    }

    public function it_shows_one_by_id(
        AccessService $service,
        Response $response,
        Twig $twig,
        StreamInterface $body,
        JsonSerializable $jsonSerializable
    ): void {
        $service->getOneById(1)->willReturn($jsonSerializable);

        $this->specRender($response, $twig, $body);

        $this->showOneById($response, 1)->shouldReturn($response);
    }

    public function it_adds(
        AccessService $service,
        Request $request,
        Response $response
    ): void {
        $service->create($request)->willReturn(null);

        $entityName = 'entity';

        $this->specRedirect($response, 201);

        $this->add($request, $response)->shouldReturn($response);
    }

    public function it_edits(
        AccessService $service,
        Request $request,
        Response $response
    ): void {
        $service->update(1, $request)->willReturn(null);

        $this->specRedirect($response, 301);

        $this->edit($request, $response, 1)->shouldReturn($response);
    }

    public function it_deletes_1(
        AccessService $service,
        Request $request,
        Response $response
    ): void {
        $service->delete($request, 1)->willReturn(null);

        $this->specRedirect($response, 200);

        $this->delete($request, $response, 1)->shouldReturn($response);
    }

    public function it_deletes_2(
        AccessService $service,
        Request $request,
        Response $response,
        Twig $twig,
        StreamInterface $body
    ): void {
        $service->delete($request, 1)->willReturn([]);

        $this->specRender($response, $twig, $body);

        $this->delete($request, $response, 1)->shouldReturn($response);
    }


    private function specRender(
        Response $response,
        Twig $twig,
        StreamInterface $body
    ): void {
        $response->getBody()->willReturn($body);
        $twig->render(Argument::type('string'), Argument::type('array'))->willReturn('body');
        $body->write(Argument::type('string'))->shouldBeCalled();
        $response->withHeader(Argument::type('string'), Argument::type('string'))->willReturn($response);
    }

    private function specRedirect(
        Response $response,
        int $status = null
    ): void {
        $response->withHeader(Argument::type('string'), Argument::type('string'))->willReturn($response);

        if ($status !== null) {
            $response->withStatus(Argument::type('integer'))->willReturn($response);
        }
    }
}
