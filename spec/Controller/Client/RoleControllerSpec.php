<?php

namespace spec\Paneric\Authorization\Controller\Client;

use JsonSerializable;
use Paneric\Authorization\Controller\Client\RoleController;
use Paneric\Authorization\Service\RoleService;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\StreamInterface;
use Twig\Environment as Twig;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class RoleControllerSpec extends ObjectBehavior
{
    public function let(RoleService $service, Twig $twig): void
    {
        $this->beConstructedWith($service, $twig);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(RoleController::class);
    }


    public function it_shows_privilege_by_id(
        RoleService $service,
        JsonSerializable $jsonSerializable,
        Response $response,
        Twig $twig,
        StreamInterface $body
    ): void {
        $service->getPrivilegeById(1, 1)->willReturn($jsonSerializable);

        $this->specRender($response, $twig, $body);

        $this->showPrivilegeById($response, 1, 1)->shouldReturn($response);
    }


    public function it_shows_privileges(
        RoleService $service,
        Response $response,
        Twig $twig,
        StreamInterface $body
    ): void {
        $service->getPrivileges(1)->willReturn([['ref' => 'ref1']]);

        $this->specRender($response, $twig, $body);

        $this->showPrivileges($response, 1)->shouldReturn($response);
    }

    public function it_edits_privileges(
        RoleService $service,
        Request $request,
        Response $response,
        StreamInterface $body
    ): void {
        $service->updatePrivileges(1, $request)->willReturn(null);

        $this->specRedirect($response, 301);

        $this->editPrivileges($request, $response, 1)->shouldReturn($response);
    }

    public function it_deletes_privileges(
        RoleService $service,
        Response $response,
        Request $request
    ): void {
        $service->deletePrivileges($request, 1)->willReturn(null);

        $this->specRedirect($response, 301);

        $this->deletePrivileges($request, $response, 1)->shouldReturn($response);
    }


    private function specRender(
        Response $response,
        Twig $twig,
        StreamInterface $body
    ): void {
        $response->getBody()->willReturn($body);
        try {
            $twig->render(Argument::type('string'), Argument::type('array'))->willReturn('body');
        } catch (LoaderError $e) {
            echo $e->getMessage();
        } catch (RuntimeError $e) {
            echo $e->getMessage();
        } catch (SyntaxError $e) {
            echo $e->getMessage();
        }
        $body->write(Argument::type('string'))->shouldBeCalled();
        $response->withHeader(Argument::type('string'), Argument::type('string'))->willReturn($response);
    }

    private function specRedirect(
        Response $response,
        int $status = null
    ): void {
        $response->withHeader(Argument::type('string'), Argument::type('string'))->willReturn($response);

        if ($status !== null) {
            $response->withStatus(Argument::type('integer'))->willReturn($response);
        }
    }
}
