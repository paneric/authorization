<?php /** @noinspection AccessModifierPresentedInspection */

namespace spec\Paneric\Authorization\Controller\Client;

use JsonSerializable;
use Paneric\Authorization\Controller\Client\PrivilegeController;
use Paneric\Authorization\Service\PrivilegeService;
use Prophecy\Argument;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\StreamInterface;
use Twig\Environment as Twig;
use PhpSpec\ObjectBehavior;

class PrivilegeControllerSpec extends ObjectBehavior
{
    public function let(PrivilegeService $service, Twig $twig): void
    {
        $this->beConstructedWith($service, $twig);
    }

    function it_is_initializable(): void
    {
        $this->shouldHaveType(PrivilegeController::class);
    }


    public function it_shows_field_by_id(
        PrivilegeService $service,
        JsonSerializable $jsonSerializable,
        Response $response,
        Twig $twig,
        StreamInterface $body
    ): void {
        $service->getFieldById(1, 1)->willReturn($jsonSerializable);

        $this->specRender($response, $twig, $body);

        $this->showFieldById($response, 1, 1)->shouldReturn($response);
    }

    public function it_shows_access(
        PrivilegeService $service,
        JsonSerializable $jsonSerializable,
        Response $response,
        Twig $twig,
        StreamInterface $body
    ): void {
        $service->getAccess(1)->willReturn($jsonSerializable);

        $this->specRender($response, $twig, $body);

        $this->showAccess($response, 1)->shouldReturn($response);
    }

    public function it_sets_access(
        PrivilegeService $service,
        Request $request,
        Response $response
    ): void {
        $service->setAccess(1, $request)->willReturn(null);

        $this->specRedirect($response, 301);

        $this->setAccess($request, $response, 1)->shouldReturn($response);
    }

    public function it_deletes_access(
        PrivilegeService $service,
        Request $request,
        Response $response
    ): void {
        $service->deleteAccess(1)->willReturn(null);

        $this->specRedirect($response, 301);

        $this->deleteAccess($response, 1)->shouldReturn($response);
    }


    public function it_shows_fields(
        PrivilegeService $service,
        Response $response,
        Twig $twig,
        StreamInterface $body
    ): void {
        $service->getFields(1)->willReturn([['ref' => 'ref1']]);

        $this->specRender($response, $twig, $body);

        $this->showFields($response, 1)->shouldReturn($response);
    }

    public function it_edits_fields(
        PrivilegeService $service,
        Request $request,
        Response $response
    ): void {
        $service->updateFields(1, $request)->willReturn(null);

        $this->specRedirect($response, 301);

        $this->editFields($request, $response, 1)->shouldReturn($response);
    }

    public function it_deletes_fields(
        PrivilegeService $service,
        Request $request,
        Response $response
    ): void {
        $service->deleteFields($request, 1)->willReturn(null);

        $this->specRedirect($response, 301);

        $this->deleteFields($request, $response, 1)->shouldReturn($response);
    }


    public function it_shows_roles(
        PrivilegeService $service,
        Response $response,
        Twig $twig,
        StreamInterface $body
    ): void {
        $service->getRoles(1)->willReturn([['ref' => 'ref1']]);

        $this->specRender($response, $twig, $body);

        $this->showRoles($response, 1)->shouldReturn($response);
    }

    public function it_edits_roles(
        PrivilegeService $service,
        Request $request,
        Response $response,
        StreamInterface $body
    ): void {
        $service->updateRoles(1, $request)->willReturn(null);

        $this->specRedirect($response, 301);

        $this->editRoles($request, $response, 1)->shouldReturn($response);
    }

    public function it_deletes_roles(
        PrivilegeService $service,
        Request $request,
        Response $response
    ): void {
        $service->deleteRoles($request, 1)->willReturn(null);

        $this->specRedirect($response, 301);

        $this->deleteRoles($request, $response, 1)->shouldReturn($response);
    }


    private function specRender(
        Response $response,
        Twig $twig,
        StreamInterface $body
    ): void {
        $response->getBody()->willReturn($body);
        $twig->render(Argument::type('string'), Argument::type('array'))->willReturn('body');
        $body->write(Argument::type('string'))->shouldBeCalled();
        $response->withHeader(Argument::type('string'), Argument::type('string'))->willReturn($response);
    }

    private function specRedirect(
        Response $response,
        int $status = null
    ): void {
        $response->withHeader(Argument::type('string'), Argument::type('string'))->willReturn($response);

        if ($status !== null) {
            $response->withStatus(Argument::type('integer'))->willReturn($response);
        }
    }
}
