<?php

namespace spec\Paneric\Authorization\ORM\Query;

use Doctrine\ORM\EntityManagerInterface;
use Paneric\Authorization\Interfaces\RepositoryInterface;
use Paneric\Authorization\ORM\Entity\Access;
use Paneric\Authorization\ORM\Entity\Field;
use Paneric\Authorization\ORM\Entity\Privilege;
use Paneric\Authorization\ORM\Entity\Role;
use Paneric\Authorization\ORM\Query\PrivilegeQuery;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class PrivilegeQuerySpec extends ObjectBehavior
{
    public function let(
        EntityManagerInterface $_em,
        RepositoryInterface $fieldRepository,
        RepositoryInterface $privilegeRepository,
        RepositoryInterface $roleRepository,
        RepositoryInterface $accessRepository
    ): void {
        $this->beConstructedWith($_em, $fieldRepository, $privilegeRepository, $roleRepository, $accessRepository);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(PrivilegeQuery::class);
    }


    public function it_finds_field_by_id(
        Privilege $privilege,
        RepositoryInterface $privilegeRepository,
        Field $field
    ): void {
        $privilegeId = 1;
        $fieldId = 1;

        $privilegeRepository->findOneById($privilegeId)->willReturn($privilege);

        $privilege->getFields()->willReturn([$field]);

        $field->getId()->willReturn($fieldId);

        $this->findFieldById($privilegeId, $fieldId)->shouldReturn($field);
    }

    public function it_finds_fields(
        Privilege $privilege,
        RepositoryInterface $privilegeRepository,
        Field $field): void {
        $privilegeId = 1;

        $privilegeRepository->findOneById($privilegeId)->willReturn($privilege);

        $privilege->getFields()->willReturn([$field]);

        $this->findFields($privilegeId)->shouldReturn([$field]);
    }

    public function it_updates_fields(
        EntityManagerInterface $_em,
        RepositoryInterface $fieldRepository,
        RepositoryInterface $privilegeRepository,
        Privilege $privilege
    ): void {
        $privilegeId = 1;
        $fieldsIds = [1, 2, 3];

        $privilegeRepository->findOneById($privilegeId)->willReturn($privilege);

        $fieldRepository->findByIds($fieldsIds)->willReturn([]);

        $privilege->updateFields([])->shouldBeCalled();

        $_em->flush()->shouldBeCalled();

        $this->updateFields($privilegeId, $fieldsIds);
    }

    public function it_removes_fields(
        EntityManagerInterface $_em,
        RepositoryInterface $privilegeRepository,
        Privilege $privilege
    ): void {
        $privilegeId = 1;

        $privilegeRepository->findOneById($privilegeId)->willReturn($privilege);

        $privilege->removeFields()->shouldBeCalled();

        $_em->flush()->shouldBeCalled();

        $this->removeFields($privilegeId);
    }


    public function it_finds_roles(
        Privilege $privilege,
        RepositoryInterface $privilegeRepository,
        Role $role
    ): void {
        $privilegeId = 1;

        $privilegeRepository->findOneById($privilegeId)->willReturn($privilege);

        $privilege->getRoles()->willReturn([$role]);

        $this->findRoles($privilegeId)->shouldReturn([$role]);
    }

    public function it_updates_roles(
        EntityManagerInterface $_em,
        RepositoryInterface $roleRepository,
        RepositoryInterface $privilegeRepository,
        Privilege $privilege
    ): void {
        $privilegeId = 1;
        $rolesIds = [1, 2, 3];

        $privilegeRepository->findOneById($privilegeId)->willReturn($privilege);

        $roleRepository->findByIds($rolesIds)->willReturn([]);

        $privilege->updateRoles([])->shouldBeCalled();

        $_em->flush()->shouldBeCalled();

        $this->updateRoles($privilegeId, $rolesIds);
    }

    public function it_removes_roles(
        EntityManagerInterface $_em,
        RepositoryInterface $privilegeRepository,
        Privilege $privilege
    ): void {
        $privilegeId = 1;

        $privilegeRepository->findOneById($privilegeId)->willReturn($privilege);

        $privilege->removeRoles()->shouldBeCalled();

        $_em->flush()->shouldBeCalled();

        $this->removeRoles($privilegeId);
    }


    public function it_finds_access(
        RepositoryInterface $privilegeRepository,
        Privilege $privilege,
        Access $access
    ): void {
        $privilegeId = 1;

        $privilegeRepository->findOneById($privilegeId)->willReturn($privilege);

        $privilege->getAccess()->willReturn($access);

        $this->findAccess($privilegeId)->shouldReturn($access);
    }

    public function it_sets_access(
        EntityManagerInterface $_em,
        RepositoryInterface $privilegeRepository,
        Privilege $privilege,
        RepositoryInterface $accessRepository,
        Access $access
    ): void {
        $privilegeId = 1;
        $accessId = 1;

        $privilegeRepository->findOneById(Argument::type('integer'))->willReturn($privilege);

        $accessRepository->findOneById(Argument::type('integer'))->willReturn($access);

        $privilege->updateAccess($access)->shouldBeCalled();

        $_em->flush()->shouldBeCalled();

        $this->setAccess($privilegeId, $accessId);
    }

    public function it_removes_access(
        EntityManagerInterface $_em,
        RepositoryInterface $privilegeRepository,
        Privilege $privilege
    ): void {
        $privilegeId = 1;

        $privilegeRepository->findOneById(Argument::type('integer'))->willReturn($privilege);
        $privilege->updateAccess(null)->shouldBeCalled();

        $_em->flush()->shouldBeCalled();

        $this->removeAccess($privilegeId);
    }
}
