<?php /** @noinspection AccessModifierPresentedInspection */

declare(strict_types=1);

namespace spec\Paneric\Authorization\ORM\Query;

use Doctrine\ORM\EntityManagerInterface;
use Paneric\Authorization\Interfaces\RepositoryInterface;
use Paneric\Authorization\ORM\Entity\Privilege;
use Paneric\Authorization\ORM\Entity\Role;
use Paneric\Authorization\ORM\Query\RoleQuery;
use PhpSpec\ObjectBehavior;

class RoleQuerySpec extends ObjectBehavior
{
    public function let(
        EntityManagerInterface $_em,
        RepositoryInterface $privilegeRepository,
        RepositoryInterface $roleRepository
    ): void {
        $this->beConstructedWith($_em, $privilegeRepository, $roleRepository);
    }

    function it_is_initializable(): void
    {
        $this->shouldHaveType(RoleQuery::class);
    }

    public function it_finds_privilege_by_id(
        Role $role,
        RepositoryInterface $roleRepository,
        Privilege $privilege
    ): void {
        $roleId = 1;
        $privilegeId = 1;

        $roleRepository->findOneById($roleId)->willReturn($role);

        $role->getPrivileges()->willReturn([$privilege]);

        $privilege->getId()->willReturn($privilegeId);

        $this->findPrivilegeById($roleId, $privilegeId)->shouldReturn($privilege);
    }

    public function it_privilege_by_route(
        RepositoryInterface $roleRepository,
        Role $role,
        Privilege $privilege
    ): void {
        $roleId = 1;
        $route = 'route';

        $roleRepository->findOneById($roleId)->willReturn($role);

        $role->getPrivileges()->willReturn([$privilege]);

        $privilege->getRoute()->willReturn($route);

        $this->findPrivilegeByRoute($roleId, $route)->shouldReturn($privilege);
    }

    public function it_finds_privileges(
        RepositoryInterface $roleRepository,
        Role $role,
        Privilege $privilege
    ): void {
        $roleId = 1;

        $roleRepository->findOneById($roleId)->willReturn($role);

        $role->getPrivileges()->willReturn([$privilege]);

        $this->findPrivileges($roleId)->shouldReturn([$privilege]);
    }

    public function it_updates_privileges(
        EntityManagerInterface $_em,
        RepositoryInterface $roleRepository,
        RepositoryInterface $privilegeRepository,
        Role $role
    ): void {
        $roleId = 1;
        $privilegesIds = [1, 2, 3];

        $roleRepository->findOneById($roleId)->willReturn($role);

        $privilegeRepository->findByIds($privilegesIds)->willReturn([]);

        $role->updatePrivileges([])->shouldBeCalled();

        $_em->flush()->shouldBeCalled();

        $this->updatePrivileges($roleId, $privilegesIds);
    }

    public function it_removes_privileges(
        EntityManagerInterface $_em,
        RepositoryInterface $roleRepository,
        Role $role
    ): void {
        $roleId = 1;

        $roleRepository->findOneById($roleId)->willReturn($role);

        $role->removePrivileges()->shouldBeCalled();

        $_em->flush()->shouldBeCalled();

        $this->removePrivileges($roleId);
    }
}
