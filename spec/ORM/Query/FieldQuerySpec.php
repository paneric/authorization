<?php

namespace spec\Paneric\Authorization\ORM\Query;

use Doctrine\ORM\EntityManagerInterface;
use Paneric\Authorization\Interfaces\RepositoryInterface;
use Paneric\Authorization\ORM\Entity\Field;
use Paneric\Authorization\ORM\Entity\Privilege;
use Paneric\Authorization\ORM\Query\FieldQuery;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class FieldQuerySpec extends ObjectBehavior
{
    public function let(
        EntityManagerInterface $_em,
        RepositoryInterface $fieldRepository,
        RepositoryInterface $privilegeRepository
    ): void {
        $this->beConstructedWith($_em, $fieldRepository, $privilegeRepository);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(FieldQuery::class);
    }


    public function it_finds_privileges(
        RepositoryInterface $fieldRepository,
        Field $field,
        Privilege $privilege
    ): void {
        $fieldId = 1;

        $fieldRepository->findOneById(Argument::type('integer'))->willReturn($field);

        $field->getPrivileges()->willReturn([$privilege]);

        $this->findPrivileges($fieldId)->shouldReturn([$privilege]);
    }

    public function it_updates_privileges(
        EntityManagerInterface $_em,
        RepositoryInterface $fieldRepository,
        RepositoryInterface $privilegeRepository,
        Field $field
    ): void {
        $fieldId = 1;
        $privilegesIds = [1, 2, 3];

        $fieldRepository->findOneById($fieldId)->willReturn($field);

        $privilegeRepository->findByIds($privilegesIds)->willReturn([]);

        $field->updatePrivileges([])->shouldBeCalled();

        $_em->flush()->shouldBeCalled();

        $this->updatePrivileges($fieldId, $privilegesIds);
    }

    public function it_removes_privileges(
        EntityManagerInterface $_em,
        RepositoryInterface $fieldRepository,
        Field $field
    ): void {
        $fieldId = 1;

        $fieldRepository->findOneById($fieldId)->willReturn($field);

        $field->removePrivileges()->shouldBeCalled();

        $_em->flush()->shouldBeCalled();

        $this->removePrivileges($fieldId);
    }
}
