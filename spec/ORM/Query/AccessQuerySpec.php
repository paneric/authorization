<?php

namespace spec\Paneric\Authorization\ORM\Query;

use Doctrine\ORM\EntityManagerInterface;
use Paneric\Authorization\Interfaces\RepositoryInterface;
use Paneric\Authorization\ORM\Entity\Access;
use Paneric\Authorization\ORM\Entity\Privilege;
use Paneric\Authorization\ORM\Query\AccessQuery;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class AccessQuerySpec extends ObjectBehavior
{
    public function let(
        EntityManagerInterface $_em,
        RepositoryInterface $accessRepository,
        RepositoryInterface $privilegeRepository
    ): void {
        $this->beConstructedWith($_em, $accessRepository, $privilegeRepository);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(AccessQuery::class);
    }


    public function it_finds_privileges(
        RepositoryInterface $accessRepository,
        Access $access,
        Privilege $privilege
    ): void {
        $accessId = 1;

        $accessRepository->findOneById(Argument::type('integer'))->willReturn($access);

        $access->getPrivileges()->willReturn([$privilege]);

        $this->findPrivileges($accessId)->shouldReturn([$privilege]);
    }

    public function it_updates_privileges(
        EntityManagerInterface $_em,
        RepositoryInterface $accessRepository,
        RepositoryInterface $privilegeRepository,
        Access $access
    ): void {
        $accessId = 1;
        $privilegesIds = [1, 2];

        $accessRepository->findOneById($accessId)->willReturn($access);

        $privilegeRepository->findByIds($privilegesIds)->willReturn([]);

        $access->updatePrivileges([])->shouldBeCalled();

        $_em->flush()->shouldBeCalled();

        $this->updatePrivileges($accessId, $privilegesIds);
    }

    public function it_removes_privileges(
        EntityManagerInterface $_em,
        RepositoryInterface $accessRepository,
        Access $access
    ): void {
        $accessId = 1;

        $accessRepository->findOneById($accessId)->willReturn($access);

        $access->removePrivileges()->shouldBeCalled();

        $_em->flush()->shouldBeCalled();

        $this->removePrivileges($accessId);
    }
}
