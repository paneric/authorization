<?php /** @noinspection AccessModifierPresentedInspection */

declare(strict_types=1);

namespace spec\Paneric\Authorization\ORM\Repository;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\QueryBuilder;
use Paneric\Authorization\DTO\PrivilegeDTO;
use Paneric\Authorization\ORM\Entity\Privilege;
use Paneric\Authorization\ORM\Repository\PrivilegeRepository;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class PrivilegeRepositorySpec extends ObjectBehavior
{
    public function let(EntityManagerInterface $_em, ClassMetadata $classMetadata): void
    {
        $_em->getClassMetadata(Privilege::class)->willReturn($classMetadata);
        $this->beConstructedWith($_em);
    }

    function it_is_initializable(): void
    {
        $this->shouldHaveType(PrivilegeRepository::class);
    }

    public function it_finds_by_ids(EntityManagerInterface $_em, QueryBuilder $queryBuilder, AbstractQuery $query): void
    {
        $args = [1, 2, 3];

        $_em->createQueryBuilder()->shouldBeCalled()->willReturn($queryBuilder);

        $queryBuilder->select('t')->shouldBeCalled()->willReturn($queryBuilder);
        $queryBuilder->from(Privilege::class, 't')->shouldBeCalled()->willReturn($queryBuilder);
        $queryBuilder->where('t.id = ?0')->shouldBeCalled()->willReturn($queryBuilder);

        $queryBuilder->orWhere(Argument::type('string'))->shouldBeCalledTimes(2);

        $queryBuilder->setParameters($args)->shouldBeCalled();

        $queryBuilder->getQuery()->willReturn($query);

        $query->getResult()->willReturn([]);

        $this->findByIds($args);
    }

    public function it_creates(PrivilegeDTO $privilegeDTO, EntityManagerInterface $_em): void
    {
        $privilegeDTO->convert()->willReturn([]);

        $_em->persist(Argument::type(Privilege::class))->shouldBeCalled();
        $_em->flush()->shouldBeCalled();

        $this->create($privilegeDTO);
    }

    public function it_updates(Privilege $privilege, PrivilegeDTO $privilegeDTO, EntityManagerInterface $_em): void
    {
        $privilegeId = 1;

        $_em->find(Privilege::class, $privilegeId)->willReturn($privilege);

        $privilegeDTO->convert()->willReturn([]);

        $privilege->hydrate([])->shouldBeCalled();

        $_em->flush()->shouldBeCalled();

        $this->update($privilegeId, $privilegeDTO);
    }

    public function it_removes(Privilege $privilege, EntityManagerInterface $_em): void
    {
        $privilegeId = 1;

        $_em->find(Privilege::class, $privilegeId)->willReturn($privilege);

        $_em->remove($privilege)->shouldBeCalled();

        $_em->flush()->shouldBeCalled();

        $this->remove($privilegeId);
    }
}
