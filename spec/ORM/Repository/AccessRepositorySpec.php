<?php

namespace spec\Paneric\Authorization\ORM\Repository;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\QueryBuilder;
use Paneric\Authorization\DTO\AccessDTO;
use Paneric\Authorization\ORM\Entity\Access;
use Paneric\Authorization\ORM\Repository\AccessRepository;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class AccessRepositorySpec extends ObjectBehavior
{
    public function let(EntityManagerInterface $_em, ClassMetadata $classMetadata): void
    {
        $_em->getClassMetadata(Access::class)->willReturn($classMetadata);
        $this->beConstructedWith($_em);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(AccessRepository::class);
    }


    public function it_finds_by_ids(EntityManagerInterface $_em, QueryBuilder $queryBuilder, AbstractQuery $query): void
    {
        $ids = [1, 2];

        $_em->createQueryBuilder()->shouldBeCalled()->willReturn($queryBuilder);

        $queryBuilder->select('t')->shouldBeCalled()->willReturn($queryBuilder);
        $queryBuilder->from(Access::class, 't')->shouldBeCalled()->willReturn($queryBuilder);
        $queryBuilder->where('t.id = ?0')->shouldBeCalled()->willReturn($queryBuilder);

        $queryBuilder->orWhere(Argument::type('string'))->shouldBeCalledTimes(1);

        $queryBuilder->setParameters($ids)->shouldBeCalled();

        $queryBuilder->getQuery()->willReturn($query);

        $query->getResult()->willReturn([]);

        $this->findByIds($ids);
    }

    public function it_creates(AccessDTO $accessDTO, EntityManagerInterface $_em): void
    {
        $accessDTO->convert()->willReturn([]);

        $_em->persist(Argument::type(Access::class))->shouldBeCalled();
        $_em->flush()->shouldBeCalled();

        $this->create($accessDTO);
    }

    public function it_updates(Access $access, AccessDTO $accessDTO, EntityManagerInterface $_em): void
    {
        $accessId = 1;

        $_em->find(Access::class, $accessId)->willReturn($access);

        $accessDTO->convert()->willReturn([]);

        $access->hydrate([])->shouldBeCalled();

        $_em->flush()->shouldBeCalled();

        $this->update($accessId, $accessDTO);
    }

    public function it_removes(Access $access, EntityManagerInterface $_em): void
    {
        $accessId = 1;

        $_em->find(Access::class, $accessId)->willReturn($access);

        $_em->remove($access)->shouldBeCalled();

        $_em->flush()->shouldBeCalled();

        $this->remove($accessId);
    }
}
