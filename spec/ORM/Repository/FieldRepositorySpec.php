<?php

declare(strict_types=1);

namespace spec\Paneric\Authorization\ORM\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\QueryBuilder;
use Paneric\Authorization\DTO\FieldDTO;
use Paneric\Authorization\ORM\Entity\Field;
use Paneric\Authorization\ORM\Repository\FieldRepository;
use Doctrine\ORM\AbstractQuery;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class FieldRepositorySpec extends ObjectBehavior
{
    public function let(EntityManagerInterface $_em, ClassMetadata $classMetadata): void
    {
        $_em->getClassMetadata(Field::class)->willReturn($classMetadata);
        $this->beConstructedWith($_em);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(FieldRepository::class);
    }


    public function it_finds_by_ids(EntityManagerInterface $_em, QueryBuilder $queryBuilder, AbstractQuery $query): void
    {
        $args = [1, 2, 3];

        $_em->createQueryBuilder()->shouldBeCalled()->willReturn($queryBuilder);

        $queryBuilder->select('t')->shouldBeCalled()->willReturn($queryBuilder);
        $queryBuilder->from(Field::class, 't')->shouldBeCalled()->willReturn($queryBuilder);
        $queryBuilder->where('t.id = ?0')->shouldBeCalled()->willReturn($queryBuilder);

        $queryBuilder->orWhere(Argument::type('string'))->shouldBeCalledTimes(2);

        $queryBuilder->setParameters($args)->shouldBeCalled();

        $queryBuilder->getQuery()->willReturn($query);

        $query->getResult()->willReturn([]);

        $this->findByIds($args);
    }


    public function it_creates(FieldDTO $fieldDTO, EntityManagerInterface $_em): void
    {
        $fieldDTO->convert()->willReturn([]);

        $_em->persist(Argument::type(Field::class))->shouldBeCalled();
        $_em->flush()->shouldBeCalled();

        $this->create($fieldDTO);
    }

    public function it_updates(Field $field, FieldDTO $fieldDTO, EntityManagerInterface $_em): void
    {
        $fieldId = 1;

        $_em->find(Field::class, $fieldId)->willReturn($field);

        $fieldDTO->convert()->willReturn([]);

        $field->hydrate([])->shouldBeCalled();

        $_em->flush()->shouldBeCalled();

        $this->update($fieldId, $fieldDTO);
    }

    public function it_removes(Field $field, EntityManagerInterface $_em): void
    {
        $fieldId = 1;

        $_em->find(Field::class, $fieldId)->willReturn($field);

        $_em->remove($field)->shouldBeCalled();

        $_em->flush()->shouldBeCalled();

        $this->remove($fieldId);
    }
}
