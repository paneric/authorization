<?php /** @noinspection AccessModifierPresentedInspection */

declare(strict_types=1);

namespace spec\Paneric\Authorization\ORM\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\AbstractQuery;
use Paneric\Authorization\DTO\RoleDTO;
use Paneric\Authorization\ORM\Entity\Role;
use Paneric\Authorization\ORM\Repository\RoleRepository;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class RoleRepositorySpec extends ObjectBehavior
{
    public function let(EntityManagerInterface $_em, ClassMetadata $classMetadata): void
    {
        $_em->getClassMetadata(Role::class)->willReturn($classMetadata);
        $this->beConstructedWith($_em);
    }

    function it_is_initializable(): void
    {
        $this->shouldHaveType(RoleRepository::class);
    }


    public function it_finds_by_ids(
        EntityManagerInterface $_em,
        QueryBuilder $queryBuilder,
        AbstractQuery $query
    ): void
    {
        $args = [1, 2, 3];

        $_em->createQueryBuilder()->shouldBeCalled()->willReturn($queryBuilder);

        $queryBuilder->select('t')->shouldBeCalled()->willReturn($queryBuilder);
        $queryBuilder->from(Role::class, 't')->shouldBeCalled()->willReturn($queryBuilder);
        $queryBuilder->where('t.id = ?0')->shouldBeCalled()->willReturn($queryBuilder);

        $queryBuilder->orWhere(Argument::type('string'))->shouldBeCalledTimes(2);

        $queryBuilder->setParameters($args)->shouldBeCalled();

        $queryBuilder->getQuery()->willReturn($query);

        $query->getResult()->willReturn([]);

        $this->findByIds($args);
    }

    public function it_creates(RoleDTO $roleDTO, EntityManagerInterface $_em): void
    {
        $roleDTO->convert()->willReturn([]);

        $_em->persist(Argument::type(Role::class))->shouldBeCalled();
        $_em->flush()->shouldBeCalled();

        $this->create($roleDTO);
    }

    public function it_updates(Role $role, RoleDTO $roleDTO, EntityManagerInterface $_em): void
    {
        $roleId = 1;

        $_em->find(Role::class, $roleId)->willReturn($role);

        $roleDTO->convert()->willReturn([]);

        $role->hydrate([])->shouldBeCalled();

        $_em->flush()->shouldBeCalled();

        $this->update($roleId, $roleDTO);
    }

    public function it_removes(Role $role, EntityManagerInterface $_em): void
    {
        $roleId = 1;

        $_em->find(Role::class, $roleId)->willReturn($role);

        $_em->remove($role)->shouldBeCalled();

        $_em->flush()->shouldBeCalled();

        $this->remove($roleId);
    }
}
