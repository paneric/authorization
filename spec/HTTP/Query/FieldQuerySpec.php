<?php

namespace spec\Paneric\Authorization\HTTP\Query;

use GuzzleHttp\ClientInterface as HttpClient;
use GuzzleHttp\Psr7\Request;
use Paneric\Authorization\Interfaces\FieldQueryInterface;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\StreamInterface;

class FieldQuerySpec extends ObjectBehavior
{
    public function let(HttpClient $httpClient): void
    {
        $this->beConstructedWith($httpClient);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(FieldQueryInterface::class);
    }


    public function it_finds_privileges(HttpClient $httpClient, Response $response, StreamInterface $stream): void
    {
        $httpClient->send(Argument::type(Request::class))->willReturn($response);

        $this->specSetResponse($response, 200, $stream);

        $stream->__toString()->willReturn('["NULL"]');

        $this->findPrivileges(1)->shouldReturn(['NULL']);
    }

    public function it_updates_privileges(HttpClient $httpClient, Response $response, StreamInterface $stream): void
    {
        $httpClient->send(Argument::type(Request::class))->willReturn($response);

        $this->specSetResponse($response, 301, $stream);

        $stream->__toString()->willReturn('["NULL"]');

        $this->updatePrivileges(1, [1, 2, 3])->shouldReturn(['NULL']);
    }

    public function it_removes_privileges(HttpClient $httpClient, Response $response, StreamInterface $stream): void
    {
        $httpClient->send(Argument::type(Request::class))->willReturn($response);

        $this->specSetResponse($response, 301, $stream);

        $stream->__toString()->willReturn('["NULL"]');

        $this->removePrivileges(1)->shouldReturn(['NULL']);
    }


    private function specSetResponse(Response $response, int $expectedStatusCode, StreamInterface $stream): void
    {
        $response->getStatusCode()->willReturn($expectedStatusCode);
        $response->hasHeader(Argument::type('string'))->willReturn(true);
        $response->getHeader(Argument::type('string'))->willReturn('application/json');
        $response->getBody()->willReturn($stream);
    }
}
