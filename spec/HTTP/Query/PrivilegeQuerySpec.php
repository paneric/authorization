<?php

namespace spec\Paneric\Authorization\HTTP\Query;

use GuzzleHttp\ClientInterface as HttpClient;
use GuzzleHttp\Psr7\Request;
use Paneric\Authorization\Interfaces\PrivilegeQueryInterface;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\StreamInterface;

class PrivilegeQuerySpec extends ObjectBehavior
{
    public function let(HttpClient $httpClient): void
    {
        $this->beConstructedWith($httpClient);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(PrivilegeQueryInterface::class);
    }


    public function it_finds_field_by_id(HttpClient $httpClient, Response $response, StreamInterface $stream): void
    {
        $httpClient->send(Argument::type(Request::class))->willReturn($response);

        $this->specSetResponse($response, 200, $stream);

        $stream->__toString()->willReturn('["NULL"]');

        $this->findFieldById(1, 1)->shouldReturn(null);
    }


    public function it_finds_fields(HttpClient $httpClient, Response $response, StreamInterface $stream): void
    {
        $httpClient->send(Argument::type(Request::class))->willReturn($response);

        $this->specSetResponse($response, 200, $stream);

        $stream->__toString()->willReturn('["NULL"]');

        $this->findFields(1)->shouldReturn(['NULL']);
    }

    public function it_updates_fields(HttpClient $httpClient, Response $response, StreamInterface $stream): void
    {
        $httpClient->send(Argument::type(Request::class))->willReturn($response);

        $this->specSetResponse($response, 301, $stream);

        $stream->__toString()->willReturn('["NULL"]');

        $this->updateFields(1, [1, 2, 3])->shouldReturn(['NULL']);
    }

    public function it_removes_fields(HttpClient $httpClient, Response $response, StreamInterface $stream): void
    {
        $httpClient->send(Argument::type(Request::class))->willReturn($response);

        $this->specSetResponse($response, 301, $stream);

        $stream->__toString()->willReturn('["NULL"]');

        $this->removeFields(1)->shouldReturn(['NULL']);
    }


    public function it_finds_roles(HttpClient $httpClient, Response $response, StreamInterface $stream): void
    {
        $httpClient->send(Argument::type(Request::class))->willReturn($response);

        $this->specSetResponse($response, 200, $stream);

        $stream->__toString()->willReturn('["NULL"]');

        $this->findRoles(1)->shouldReturn(['NULL']);
    }

    public function it_updates_roles(HttpClient $httpClient, Response $response, StreamInterface $stream): void
    {
        $httpClient->send(Argument::type(Request::class))->willReturn($response);

        $this->specSetResponse($response, 301, $stream);

        $stream->__toString()->willReturn('["NULL"]');

        $this->updateRoles(1, [1, 2, 3])->shouldReturn(['NULL']);
    }

    public function it_removes_roles(HttpClient $httpClient, Response $response, StreamInterface $stream): void
    {
        $httpClient->send(Argument::type(Request::class))->willReturn($response);

        $this->specSetResponse($response, 301, $stream);

        $stream->__toString()->willReturn('["NULL"]');

        $this->removeRoles(1)->shouldReturn(['NULL']);
    }


    public function it_finds_access(HttpClient $httpClient, Response $response, StreamInterface $stream): void
    {
        $httpClient->send(Argument::type(Request::class))->willReturn($response);

        $this->specSetResponse($response, 200, $stream);

        $stream->__toString()->willReturn('["NULL"]');

        $this->findAccess(1)->shouldReturn(null);
    }

    public function it_sets_access(HttpClient $httpClient, Response $response, StreamInterface $stream): void
    {
        $httpClient->send(Argument::type(Request::class))->willReturn($response);

        $this->specSetResponse($response, 301, $stream);

        $stream->__toString()->willReturn('["NULL"]');

        $this->setAccess(1, 1)->shouldReturn(['NULL']);
    }

    public function it_removes_access(HttpClient $httpClient, Response $response, StreamInterface $stream): void
    {
        $httpClient->send(Argument::type(Request::class))->willReturn($response);

        $this->specSetResponse($response, 301, $stream);

        $stream->__toString()->willReturn('["NULL"]');

        $this->removeAccess(1)->shouldReturn(['NULL']);
    }


    private function specSetResponse(Response $response, int $expectedStatusCode, StreamInterface $stream): void
    {
        $response->getStatusCode()->willReturn($expectedStatusCode);
        $response->hasHeader(Argument::type('string'))->willReturn(true);
        $response->getHeader(Argument::type('string'))->willReturn('application/json');
        $response->getBody()->willReturn($stream);
    }
}
