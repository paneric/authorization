<?php

namespace spec\Paneric\Authorization\HTTP\Repository;

use GuzzleHttp\Psr7\Request;
use Prophecy\Argument;
use Psr\Http\Message\ResponseInterface as Response;
use Paneric\Authorization\DTO\RoleDTO;
use Paneric\Authorization\HTTP\Repository\RoleRepository;
use GuzzleHttp\ClientInterface as HttpClient;
use PhpSpec\ObjectBehavior;
use Psr\Http\Message\StreamInterface;

class RoleRepositorySpec extends ObjectBehavior
{
    public function let(HttpClient $httpClient): void
    {
        $this->beConstructedWith($httpClient);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(RoleRepository::class);
    }

    public function it_sets_route_pattern(): void
    {
        $this->setRoutePattern('field/{id}/privilege/{id}', '{')
            ->shouldReturn('field/%s/privilege/%s');

        $this->setRoutePattern('field/{id}/privilege/{id}[/{limit}[/{offset}]]', '{')
            ->shouldReturn('field/%s/privilege/%s/%s/%s');
    }


    public function it_finds_all(HttpClient $httpClient, Response $response, StreamInterface $stream): void
    {
        $httpClient->send(Argument::type(Request::class))->willReturn($response);

        $this->specSetResponse($response, 200, $stream);

        $stream->__toString()->willReturn('["NULL"]');

        $this->findAll()->shouldReturn(['NULL']);
    }

    public function it_finds_by_ids(HttpClient $httpClient, Response $response, StreamInterface $stream): void
    {
        $httpClient->send(Argument::type(Request::class))->willReturn($response);

        $this->specSetResponse($response, 200, $stream);

        $stream->__toString()->willReturn('["NULL"]');

        $this->findByIds([1, 2, 3])->shouldReturn(['NULL']);
    }

    public function it_finds_one_by_id(HttpClient $httpClient, Response $response, StreamInterface $stream): void
    {
        $httpClient->send(Argument::type(Request::class))->willReturn($response);

        $this->specSetResponse($response, 200, $stream);

        $stream->__toString()->willReturn('{"ref" : "ref1"}');

        $this->findOneById(1)->shouldBeAnInstanceOf(RoleDTO::class);
    }

    public function it_creates(HttpClient $httpClient, Response $response, RoleDTO $roleDTO, StreamInterface $stream): void
    {
        $roleDTO->convert()->willReturn(['ref' => 'ref1']);

        $httpClient->send(Argument::type(Request::class))->willReturn($response);

        $this->specSetResponse($response, 201, $stream);

        $stream->__toString()->willReturn('["NULL"]');

        $this->create($roleDTO)->shouldReturn(['NULL']);
    }

    public function it_updates(HttpClient $httpClient, Response $response, RoleDTO $roleDTO, StreamInterface $stream): void
    {
        $roleDTO->convert()->willReturn(['ref' => 'ref1']);

        $httpClient->send(Argument::type(Request::class))->willReturn($response);

        $this->specSetResponse($response, 301, $stream);

        $stream->__toString()->willReturn('["NULL"]');

        $this->update(1, $roleDTO)->shouldReturn(['NULL']);
    }

    public function it_removes(HttpClient $httpClient, Response $response, StreamInterface $stream): void
    {
        $httpClient->send(Argument::type(Request::class))->willReturn($response);

        $this->specSetResponse($response, 200, $stream);

        $stream->__toString()->willReturn('["NULL"]');

        $this->remove(1)->shouldReturn(['NULL']);
    }


    private function specSetResponse(Response $response, int $expectedStatusCode, StreamInterface $stream): void
    {
        $response->getStatusCode()->willReturn($expectedStatusCode);
        $response->hasHeader(Argument::type('string'))->willReturn(true);
        $response->getHeader(Argument::type('string'))->willReturn('application/json');
        $response->getBody()->willReturn($stream);
    }
}
