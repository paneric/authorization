<?php /** @noinspection AccessModifierPresentedInspection */

namespace spec\Paneric\Authorization\Service;

use Paneric\Authorization\DTO\FieldDTO;
use Paneric\Authorization\DTO\PrivilegeDTO;
use Paneric\Authorization\DTO\RoleDTO;
use Paneric\Authorization\Interfaces\RepositoryInterface;
use Paneric\Authorization\Interfaces\PrivilegeQueryInterface;
use Paneric\Authorization\ORM\Entity\Access;
use Paneric\Authorization\ORM\Entity\Field;
use Paneric\Authorization\ORM\Entity\Privilege;
use Paneric\Authorization\Service\PrivilegeService;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class PrivilegeServiceSpec extends ObjectBehavior
{
    public function let(
        RepositoryInterface $privilegeRepository,
        PrivilegeQueryInterface $privilegeQuery,
        RepositoryInterface $fieldRepository,
        RepositoryInterface $roleRepository,
        RepositoryInterface $accessRepository,
        SessionInterface $session
    ): void {
        $this->beConstructedWith(
            $privilegeRepository,
            $privilegeQuery,
            $fieldRepository,
            $roleRepository,
            $accessRepository,
            $session
        );
    }

    function it_is_initializable(): void
    {
        $this->shouldHaveType(PrivilegeService::class);
    }


    public function it_gets_all(
        SessionInterface $session,
        RepositoryInterface $privilegeRepository,
        RepositoryInterface $accessRepository
    ): void {
        $session->setFlash(Argument::type('array'), Argument::type('string'));

        $privilegeRepository->findAll()->willReturn([]);
        $accessRepository->findAll()->willReturn([]);

        $this->getAll()->shouldReturn([
            'privileges' => [],
            'accesses' => [],
        ]);
    }

    public function it_gets_by_ids(
        Request $request,
        RepositoryInterface $privilegeRepository,
        Privilege $privilege
    ): void {// (POST) /accesses
        $request->getAttribute('validation')->willReturn([PrivilegeDTO::class => []]);
        $request->getMethod()->willReturn('POST');
        $request->getParsedBody()->willReturn([1, 2, 3]);

        $privilegeRepository->findByIds(Argument::type('array'))->willReturn([$privilege]);

        $privilege->jsonSerialize()->willReturn(['ref' => 'ref1']);

        $this->getByIds($request)->shouldReturn([['ref' => 'ref1']]);
    }


    public function it_gets_one_by_id(RepositoryInterface $privilegeRepository, Privilege $privilege): void
    {
        $privilegeRepository->findOneById(1)->willReturn($privilege);

        $this->getOneById(1)->shouldReturn($privilege);
    }

    public function it_creates(
        Request $request,
        RepositoryInterface $privilegeRepository,
        PrivilegeDTO $privilege,
        PrivilegeQueryInterface $privilegeQuery
    ): void {
        $request->getAttribute('validation')->willReturn([PrivilegeDTO::class => []]);
        $request->getMethod()->willReturn('POST');
        $request->getParsedBody()->willReturn(['ref' => 'privilege_1', 'access' => 1]);

        $privilegeRepository->findByEnhanced(
            Argument::type('array'),
            Argument::type('string')
        )->willReturn(null);

        $privilegeRepository->create(Argument::type(PrivilegeDTO::class))->willReturn(null);

        $privilegeRepository->findOneByRef(Argument::type('string'))->willReturn($privilege);

        $privilege->getId()->willReturn(1);

        $privilegeQuery->setAccess(
            Argument::type('integer'),
            Argument::type('integer')
        )->willReturn(null);

        $this->create($request)->shouldReturn(null);
    }

    public function it_updates(
        Request $request,
        RepositoryInterface $privilegeRepository,
        PrivilegeDTO $privilege,
        PrivilegeQueryInterface $privilegeQuery
    ): void {
        $request->getAttribute('validation')->willReturn([PrivilegeDTO::class => []]);
        $request->getParsedBody()->willReturn(['ref' => 'privilege_1', 'access' => 1]);
        $request->getMethod()->willReturn('POST');

        $privilegeRepository->findByEnhanced(
            Argument::type('array'),
            Argument::type('string'),
            Argument::type('integer')
        )->willReturn(null);

        $privilegeRepository->update(
            Argument::type('integer'),
            Argument::type(PrivilegeDTO::class)
        )->willReturn(null);

        $privilegeRepository->findOneByRef(Argument::type('string'))->willReturn($privilege);

        $privilege->getId()->willReturn(1);

        $privilegeQuery->setAccess(
            Argument::type('integer'),
            Argument::type('integer')
        )->willReturn(null);

        $this->update(1, $request)->shouldReturn(null);
    }

    public function it_deletes(RepositoryInterface $privilegeRepository, Request $request): void
    {
        $request->getMethod()->willReturn('POST');

        $privilegeRepository->remove(Argument::type('integer'))->willReturn(null);

        $this->delete($request, 1)->shouldReturn(null);
    }


    public function it_gets_field_by_id(PrivilegeQueryInterface $privilegeQuery, Field $field): void
    {
        $privilegeQuery->findFieldById(
            Argument::type('integer'),
            Argument::type('integer')
        )->willReturn($field);

        $this->getFieldById(1, 1)->shouldReturn($field);
    }

    public function it_gets_fields(PrivilegeQueryInterface $privilegeQuery): void
    {
        $privilegeQuery->findFields(Argument::type('integer'))->willReturn([]);

        $this->getFields(1)->shouldReturn([]);
    }

    public function it_updates_fields(Request $request, PrivilegeQueryInterface $privilegeQuery): void
    {
        $request->getAttribute('validation')->willReturn([FieldDTO::class => []]);
        $request->getMethod()->willReturn('POST');
        $request->getParsedBody()->willReturn(['id' => []]);

        $privilegeQuery->updateFields(
            Argument::type('integer'),
            Argument::type('array')
        )->willReturn(null);

        $this->updateFields(1, $request)->shouldReturn(null);
    }

    public function it_deletes_fields(Request $request, PrivilegeQueryInterface $privilegeQuery): void
    {
        $request->getMethod()->willReturn('POST');

        $privilegeQuery->removeFields(Argument::type('integer'))->willReturn(null);

        $this->deleteFields($request, 1)->shouldReturn(null);
    }


    public function it_gets_roles(PrivilegeQueryInterface $privilegeQuery): void
    {
        $privilegeQuery->findRoles(Argument::type('integer'))->willReturn([]);

        $this->getRoles(1)->shouldReturn([]);
    }

    public function it_updates_roles(Request $request, PrivilegeQueryInterface $privilegeQuery): void
    {
        $request->getAttribute('validation')->willReturn([RoleDTO::class => []]);
        $request->getMethod()->willReturn('POST');
        $request->getParsedBody()->willReturn(['id' => []]);

        $privilegeQuery->updateRoles(
            Argument::type('integer'),
            Argument::type('array')
        )->willReturn(null);

        $this->updateRoles(1, $request)->shouldReturn(null);
    }

    public function it_deletes_roles(Request $request, PrivilegeQueryInterface $privilegeQuery): void
    {
        $request->getMethod()->willReturn('POST');

        $privilegeQuery->removeRoles(Argument::type('integer'))->willReturn(null);

        $this->deleteRoles($request, 1)->shouldReturn(null);
    }


    public function it_gets_access(PrivilegeQueryInterface $privilegeQuery, Access $access): void
    {
        $privilegeQuery->findAccess(Argument::type('integer'))->willReturn($access);

        $this->getAccess(1)->shouldReturn($access);
    }

    public function it_sets_access(Request $request, PrivilegeQueryInterface $privilegeQuery, Access $access): void
    {
        $request->getAttribute('validation')->willReturn([PrivilegeDTO::class => []]);
        $request->getMethod()->willReturn('POST');
        $request->getParsedBody()->willReturn(['id' => 1]);

        $privilegeQuery->setAccess(
            Argument::type('integer'),
            Argument::type('integer')
        )->willReturn(null);

        $this->setAccess(1, $request)->shouldReturn(null);

    }

    public function it_deletes_access(PrivilegeQueryInterface $privilegeQuery): void
    {
        $privilegeQuery->removeAccess(Argument::type('integer'))->willReturn(null);

        $this->deleteAccess(1)->shouldReturn(null);
    }
}
