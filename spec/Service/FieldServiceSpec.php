<?php

namespace spec\Paneric\Authorization\Service;

use Paneric\Authorization\DTO\FieldDTO;
use Paneric\Authorization\DTO\PrivilegeDTO;
use Paneric\Authorization\Interfaces\FieldQueryInterface;
use Paneric\Authorization\Interfaces\RepositoryInterface;
use Paneric\Authorization\Service\FieldService;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class FieldServiceSpec extends ObjectBehavior
{
    public function let(
        RepositoryInterface $fieldRepository,
        FieldQueryInterface $fieldQuery,
        RepositoryInterface $privilegeRepository,
        SessionInterface $session
    ): void {
        $this->beConstructedWith($fieldRepository, $fieldQuery, $privilegeRepository, $session);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(FieldService::class);
    }


    public function it_gets_one_by_id(RepositoryInterface $fieldRepository, FieldDTO $access): void
    {
        $fieldRepository->findOneById(1)->willReturn($access);

        $this->getOneById(1)->shouldReturn($access);
    }

    public function it_creates(Request $request, RepositoryInterface $fieldRepository): void
    {
        $request->getAttribute('validation')->willReturn([FieldDTO::class => []]);
        $request->getMethod()->willReturn('POST');
        $request->getParsedBody()->willReturn(['ref' => 'access_1']);

        $fieldRepository->findByEnhanced(Argument::type('array'), Argument::type('string'))->willReturn(null);

        $fieldRepository->create(Argument::type(FieldDTO::class))->willReturn(null);

        $this->create($request)->shouldReturn(null);
    }

    public function it_updates(Request $request, RepositoryInterface $fieldRepository): void
    {
        $request->getAttribute('validation')->willReturn([FieldDTO::class => []]);
        $request->getParsedBody()->willReturn(['ref' => 'access_1']);
        $request->getMethod()->willReturn('POST');

        $fieldRepository->findByEnhanced(
            Argument::type('array'),
            Argument::type('string'),
            Argument::type('integer')
        )->willReturn(null);

        $fieldRepository->update(
            Argument::type('integer'),
            Argument::type(FieldDTO::class)
        )->willReturn(null);

        $this->update(1, $request)->shouldReturn(null);
    }

    public function it_deletes(Request $request,RepositoryInterface $fieldRepository): void
    {
        $request->getMethod()->willReturn('POST');

        $fieldRepository->remove(Argument::type('integer'))->willReturn(null);

        $this->delete($request, 1);
    }


    public function it_gets_privileges(FieldQueryInterface $fieldQuery): void
    {
        $fieldQuery->findPrivileges(Argument::type('integer'))->willReturn([]);

        $this->getPrivileges(1)->shouldReturn([]);
    }

    public function it_updates_privileges(Request $request, FieldQueryInterface $fieldQuery): void
    {
        $request->getAttribute('validation')->willReturn([PrivilegeDTO::class => []]);
        $request->getMethod()->willReturn('POST');
        $request->getParsedBody()->willReturn(['id' => []]);

        $fieldQuery->updatePrivileges(
            Argument::type('integer'),
            Argument::type('array')
        )->willReturn(null);

        $this->updatePrivileges(1, $request)->shouldReturn(null);
    }

    public function it_deletes_privileges(Request $request, FieldQueryInterface $fieldQuery): void
    {
        $request->getMethod()->willReturn('POST');

        $fieldQuery->removePrivileges(Argument::type('integer'))->willReturn(null);

        $this->deletePrivileges($request, 1)->shouldReturn(null);
    }
}
