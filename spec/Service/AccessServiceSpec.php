<?php

namespace spec\Paneric\Authorization\Service;

use Paneric\Authorization\DTO\AccessDTO;
use Paneric\Authorization\DTO\PrivilegeDTO;
use Paneric\Authorization\Interfaces\AccessQueryInterface;
use Paneric\Authorization\Interfaces\RepositoryInterface;
use Paneric\Authorization\ORM\Entity\Access;
use Paneric\Authorization\Service\AccessService;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class AccessServiceSpec extends ObjectBehavior
{
    public function let(
        RepositoryInterface $accessRepository,
        AccessQueryInterface $accessQuery,
        RepositoryInterface $privilegeRepository,
        SessionInterface $session
    ): void {
        $this->beConstructedWith($accessRepository, $accessQuery, $privilegeRepository, $session);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(AccessService::class);
    }


    public function it_gets_all(RepositoryInterface $accessRepository, SessionInterface $session): void
    {
        $session->setFlash(Argument::type('array'), Argument::type('string'))->shouldBeCalled();

        $accessRepository->findAll()->willReturn([]);

        $this->getAll()->shouldReturn(['accesses' => []]);
    }

    public function it_gets_by_ids(
        Request $request,
        RepositoryInterface $accessRepository,
        Access $access
    ): void {// (POST) /accesses
        $request->getAttribute('validation')->willReturn([AccessDTO::class => []]);
        $request->getMethod()->willReturn('POST');
        $request->getParsedBody()->willReturn([1, 2, 3]);

        $accessRepository->findByIds(Argument::type('array'))->willReturn([$access]);

        $access->jsonSerialize()->willReturn(['ref' => 'ref1']);

        $this->getByIds($request)->shouldReturn([['ref' => 'ref1']]);
    }


    public function it_gets_one_by_id(RepositoryInterface $accessRepository, Access $access): void
    {
        $accessRepository->findOneById(1)->willReturn($access);

        $this->getOneById(1)->shouldReturn($access);
    }

    public function it_creates(Request $request, RepositoryInterface $accessRepository): void
    {
        $request->getAttribute('validation')->willReturn([AccessDTO::class => []]);
        $request->getMethod()->willReturn('POST');
        $request->getParsedBody()->willReturn(['ref' => 'access_1', 'level' => 1]);

        $accessRepository->findByEnhanced(Argument::type('array'), Argument::type('string'))->willReturn(null);

        $accessRepository->create(Argument::type(AccessDTO::class))->willReturn(null);

        $this->create($request)->shouldReturn(null);
    }

    public function it_updates(Request $request, RepositoryInterface $accessRepository): void
    {
        $request->getAttribute('validation')->willReturn([AccessDTO::class => []]);
        $request->getParsedBody()->willReturn(['ref' => 'access_1', 'level' => 1]);
        $request->getMethod()->willReturn('POST');

        $accessRepository->findByEnhanced(
            Argument::type('array'),
            Argument::type('string'),
            Argument::type('integer')
        )->willReturn(null);

        $accessRepository->update(
            Argument::type('integer'),
            Argument::type(AccessDTO::class)
        )->willReturn(null);

        $this->update(1, $request)->shouldReturn(null);
    }

    public function it_deletes(RepositoryInterface $accessRepository, Request $request): void
    {
        $request->getMethod()->willReturn('POST');

        $accessRepository->remove(Argument::type('integer'))->willReturn(null);

        $this->delete($request, 1)->shouldReturn(null);
    }


    public function it_gets_privileges(AccessQueryInterface $accessQuery): void
    {
        $accessQuery->findPrivileges(Argument::type('integer'))->willReturn([]);

        $this->getPrivileges(1)->shouldReturn([]);
    }

    public function it_updates_privileges(Request $request, AccessQueryInterface $accessQuery): void
    {
        $request->getAttribute('validation')->willReturn([PrivilegeDTO::class => []]);
        $request->getMethod()->willReturn('POST');
        $request->getParsedBody()->willReturn(['id' => []]);

        $accessQuery->updatePrivileges(
            Argument::type('integer'),
            Argument::type('array')
        )->willReturn(null);

        $this->updatePrivileges(1, $request)->shouldReturn(null);
    }

    public function it_deletes_privileges(Request $request, AccessQueryInterface $accessQuery): void
    {
        $request->getMethod()->willReturn('POST');

        $accessQuery->removePrivileges(Argument::type('integer'))->willReturn(null);

        $this->deletePrivileges($request, 1)->shouldReturn(null);
    }
}
