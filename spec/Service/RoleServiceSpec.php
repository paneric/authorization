<?php

namespace spec\Paneric\Authorization\Service;

use Paneric\Authorization\DTO\PrivilegeDTO;
use Paneric\Authorization\DTO\RoleDTO;
use Paneric\Authorization\Interfaces\RepositoryInterface;
use Paneric\Authorization\ORM\Entity\Privilege;
use Paneric\Authorization\ORM\Entity\Role;
use Paneric\Interfaces\Session\SessionInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Paneric\Authorization\Interfaces\RoleQueryInterface;
use Paneric\Authorization\Service\RoleService;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class RoleServiceSpec extends ObjectBehavior
{
    public function let(
        RepositoryInterface $roleRepository,
        RoleQueryInterface $roleQuery,
        RepositoryInterface $privilegeRepository,
        SessionInterface $session
    ): void {
        $this->beConstructedWith($roleRepository, $roleQuery, $privilegeRepository, $session);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(RoleService::class);
    }


    public function it_gets_all(SessionInterface $session, RepositoryInterface $roleRepository): void
    {
        $session->setFlash(Argument::type('array'), Argument::type('string'));

        $roleRepository->findAll()->willReturn([]);

        $this->getAll()->shouldReturn(['roles' => []]);
    }

    public function it_gets_by_ids(
        Request $request,
        RepositoryInterface $roleRepository,
        Role $role
    ): void {// (POST) /accesses
        $request->getAttribute('validation')->willReturn([RoleDTO::class => []]);
        $request->getMethod()->willReturn('POST');
        $request->getParsedBody()->willReturn([1, 2, 3]);

        $roleRepository->findByIds(Argument::type('array'))->willReturn([$role]);

        $role->jsonSerialize()->willReturn(['ref' => 'ref1']);

        $this->getByIds($request)->shouldReturn([['ref' => 'ref1']]);
    }


    public function it_gets_one_by_id(RepositoryInterface $roleRepository, Role $role): void
    {
        $roleRepository->findOneById(1)->willReturn($role);

        $this->getOneById(1)->shouldReturn($role);
    }

    public function it_creates(Request $request, RepositoryInterface $roleRepository): void
    {
        $request->getAttribute('validation')->willReturn([RoleDTO::class => []]);
        $request->getMethod()->willReturn('POST');
        $request->getParsedBody()->willReturn(['ref' => 'role_1']);

        $roleRepository->findByEnhanced(Argument::type('array'), Argument::type('string'))->willReturn(null);

        $roleRepository->create(Argument::type(RoleDTO::class))->willReturn(null);

        $this->create($request)->shouldReturn(null);
    }

    public function it_updates(Request $request, RepositoryInterface $roleRepository): void
    {
        $request->getAttribute('validation')->willReturn([RoleDTO::class => []]);
        $request->getParsedBody()->willReturn(['ref' => 'role_1']);
        $request->getMethod()->willReturn('POST');

        $roleRepository->findByEnhanced(
            Argument::type('array'),
            Argument::type('string'),
            Argument::type('integer'),
            )->willReturn(null);

        $roleRepository->update(
            Argument::type('integer'),
            Argument::type(RoleDTO::class)
        )->willReturn(null);

        $this->update(1, $request)->shouldReturn(null);
    }

    public function it_deletes(Request $request, RepositoryInterface $roleRepository): void
    {
        $request->getMethod()->willReturn('POST');

        $roleRepository->remove(Argument::type('integer'))->willReturn(null);

        $this->delete($request, 1)->shouldReturn(null);
    }


    public function it_gets_privilege_by_id(RoleQueryInterface $roleQuery, Privilege $privilege): void
    {
        $roleQuery->findPrivilegeById(
            Argument::type('integer'),
            Argument::type('integer')
        )->willReturn($privilege);

        $this->getPrivilegeById(1, 1)->shouldReturn($privilege);
    }

    public function it_gets_privileges(RoleQueryInterface $roleQuery): void
    {
        $roleQuery->findPrivileges(Argument::type('integer'))->willReturn([]);

        $this->getPrivileges(1)->shouldReturn([]);
    }


    public function it_updates_privileges(Request $request, RoleQueryInterface $roleQuery): void
    {
        $request->getAttribute('validation')->willReturn([PrivilegeDTO::class => []]);
        $request->getMethod()->willReturn('POST');
        $request->getParsedBody()->willReturn(['id' => []]);

        $roleQuery->updatePrivileges(
            Argument::type('integer'),
            Argument::type('array')
        )->willReturn(null);

        $this->updatePrivileges(1, $request)->shouldReturn(null);
    }

    public function it_deletes_privileges(Request $request, RoleQueryInterface $roleQuery): void
    {
        $request->getMethod()->willReturn('POST');

        $roleQuery->removePrivileges(Argument::type('integer'))->willReturn(null);

        $this->deletePrivileges($request, 1)->shouldReturn(null);
    }
}
