<?php

namespace spec\Paneric\Authorization\PDO\Query;

use Paneric\Authorization\DTO\FieldDTO;
use Paneric\Authorization\DTO\PrivilegeDTO;
use Paneric\Authorization\PDO\Query\PrivilegeQuery;
use Paneric\Authorization\PDO\Query\RoleQuery;
use Paneric\PdoWrapper\Manager;
use PDOStatement;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class RoleQuerySpec extends ObjectBehavior
{
    public function let(Manager $manager, PrivilegeQuery $privilegeQuery): void
    {
        $this->beConstructedWith($manager, $privilegeQuery);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(RoleQuery::class);
    }

    public function it_finds_privilege_by_id(
        Manager $manager,
        PDOStatement $stmt,
        PrivilegeDTO $privilege
    ): void {
        $roleId = 1;
        $privilegeId = 1;

        $manager->setTable(Argument::type('string'))->shouldBeCalled();
        $manager->setDTOClass(Argument::type('string'))->shouldBeCalled();
        $manager->setFetchMode(Argument::type('integer'))->shouldBeCalled();

        $manager->setStmt(Argument::type('string'), Argument::type('array'))->willReturn($stmt);

        $stmt->fetch()->willReturn($privilege);

        $this->findPrivilegeById($roleId, $privilegeId)->shouldReturn($privilege);
    }

    public function it_finds_privilege_by_route(
        Manager $manager,
        PDOStatement $stmt,
        PrivilegeQuery $privilegeQuery,
        PrivilegeDTO $privilege,
        FieldDTO $field
    ): void {
        $roleId = 1;
        $route = 'route';

        $manager->setTable(Argument::type('string'))->shouldBeCalled();
        $manager->setDTOClass(Argument::type('string'))->shouldBeCalled();
        $manager->setFetchMode(Argument::type('integer'));

        $manager->beginTransaction()->shouldBeCalled();

        $manager->setStmt(Argument::type('string'), Argument::type('array'))->willReturn($stmt);

        $stmt->fetch()->willReturn($privilege);

        $privilege->getId()->willReturn(1);

        $privilegeQuery->findFields(Argument::type('integer'))->willReturn([$field]);

        $privilege->updateFields(Argument::type('array'))->shouldBeCalled();

        $manager->commit()->shouldBeCalled();

        $this->findPrivilegeByRoute($roleId, $route)->shouldReturn($privilege);
    }

    public function it_finds_privileges(Manager $manager, PDOStatement $stmt): void
    {
        $privilegeId = 1;

        $manager->setTable(Argument::type('string'))->shouldBeCalled();
        $manager->setDTOClass(Argument::type('string'))->shouldBeCalled();
        $manager->setFetchMode(Argument::type('integer'))->shouldBeCalled();

        $manager->setStmt(Argument::type('string'), Argument::type('array'))->willReturn($stmt);

        $stmt->fetchAll()->willReturn([]);

        $this->findPrivileges($privilegeId)->shouldReturn([]);
    }

    public function it_updates_privileges(Manager $manager): void
    {
        $roleId = 1;
        $privilegesIds = [1, 2, 3];

        $manager->setTable(Argument::type('string'))->shouldBeCalled();
        $manager->beginTransaction()->shouldBeCalled();
        $manager->delete(Argument::type('array'))->shouldBeCalled();
        $manager->createMultiple(Argument::type('array'))->shouldBeCalled();
        $manager->commit()->shouldBeCalled();

        $this->updatePrivileges($roleId, $privilegesIds);
    }

    public function it_removes_privileges(Manager $manager): void
    {
        $roleId = 1;

        $manager->setTable(Argument::type('string'))->shouldBeCalled();
        $manager->delete(Argument::type('array'))->shouldBeCalled();

        $this->removePrivileges($roleId);
    }
}
