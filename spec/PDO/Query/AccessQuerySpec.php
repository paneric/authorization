<?php /** @noinspection AccessModifierPresentedInspection */

namespace spec\Paneric\Authorization\PDO\Query;

use Paneric\Authorization\PDO\Query\AccessQuery;
use Paneric\PdoWrapper\Manager;
use PDOStatement;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class AccessQuerySpec extends ObjectBehavior
{
    public function let(Manager $manager): void
    {
        $this->beConstructedWith($manager);
    }

    function it_is_initializable(): void
    {
        $this->shouldHaveType(AccessQuery::class);
    }


    public function it_finds_privileges(Manager $manager, PDOStatement $stmt): void
    {
        $accessId = 1;

        $manager->setTable(Argument::type('string'))->shouldBeCalled();
        $manager->setDTOClass(Argument::type('string'))->shouldBeCalled();
        $manager->setFetchMode(Argument::type('integer'))->shouldBeCalled();

        $manager->setStmt(Argument::type('string'), Argument::type('array'))->willReturn($stmt);

        $stmt->fetchAll()->willReturn([]);

        $this->findPrivileges($accessId)->shouldReturn([]);
    }

    public function it_updates_privileges(Manager $manager): void
    {
        $accessId = 1;
        $privilegesIds = [1, 2, 3];

        $manager->setTable(Argument::type('string'))->shouldBeCalled();
        $manager->beginTransaction()->shouldBeCalled();
        $manager->setStmt(Argument::type('string'), Argument::type('array'))->shouldBeCalled();
        $manager->updateSame(
            Argument::type('array'),
            Argument::type('array'),
            Argument::type('string')
        )->shouldBeCalled();
        $manager->commit()->shouldBeCalled();

        $this->updatePrivileges($accessId, $privilegesIds);
    }

    public function it_removes_privileges(Manager $manager): void
    {
        $accessId = 1;

        $manager->setTable(Argument::type('string'))->shouldBeCalled();
        $manager->setStmt(Argument::type('string'), Argument::type('array'))->shouldBeCalled();

        $this->removePrivileges($accessId);

    }
}
