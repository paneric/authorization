<?php /** @noinspection AccessModifierPresentedInspection */

namespace spec\Paneric\Authorization\PDO\Query;

use Paneric\Authorization\PDO\Query\FieldQuery;
use Paneric\PdoWrapper\Manager;
use PDOStatement;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class FieldQuerySpec extends ObjectBehavior
{
    public function let(Manager $manager): void
    {
        $this->beConstructedWith($manager);
    }

    function it_is_initializable(): void
    {
        $this->shouldHaveType(FieldQuery::class);
    }

    public function it_finds_privileges(Manager $manager, PDOStatement $stmt): void
    {
        $fieldId = 1;

        $manager->setTable(Argument::type('string'))->shouldBeCalled();
        $manager->setDTOClass(Argument::type('string'))->shouldBeCalled();
        $manager->setFetchMode(Argument::type('integer'))->shouldBeCalled();

        $manager->setStmt(Argument::type('string'), Argument::type('array'))->willReturn($stmt);

        $stmt->fetchAll()->willReturn([]);

        $this->findPrivileges($fieldId)->shouldReturn([]);
    }

    public function it_updates_privileges(Manager $manager): void
    {
        $fieldId = 1;
        $privilegesIds = [1, 2, 3];

        $manager->setTable(Argument::type('string'))->shouldBeCalled();
        $manager->beginTransaction()->shouldBeCalled();
        $manager->delete(Argument::type('array'))->shouldBeCalled();
        $manager->createMultiple(Argument::type('array'))->shouldBeCalled();
        $manager->commit()->shouldBeCalled();

        $this->updatePrivileges($fieldId, $privilegesIds);
    }

    public function it_removes_privileges(Manager $manager): void
    {
        $fieldId = 1;

        $manager->setTable(Argument::type('string'))->shouldBeCalled();
        $manager->delete(Argument::type('array'))->shouldBeCalled();

        $this->removePrivileges($fieldId);

    }
}
