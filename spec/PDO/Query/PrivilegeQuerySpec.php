<?php /** @noinspection AccessModifierPresentedInspection */

namespace spec\Paneric\Authorization\PDO\Query;

use Paneric\Authorization\DTO\AccessDTO;
use Paneric\Authorization\DTO\FieldDTO;
use Paneric\Authorization\PDO\Query\FieldQuery;
use Paneric\Authorization\PDO\Query\PrivilegeQuery;
use Paneric\PdoWrapper\Manager;
use PDOStatement;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class PrivilegeQuerySpec extends ObjectBehavior
{
    public function let(Manager $manager, FieldQuery $fieldQuery): void
    {
        $this->beConstructedWith($manager, $fieldQuery);
    }

    function it_is_initializable(): void
    {
        $this->shouldHaveType(PrivilegeQuery::class);
    }


    public function it_finds_field_by_id(
        Manager $manager,
        PDOStatement $stmt,
        FieldDTO $field
    ): void {
        $privilegeId = 1;
        $fieldId = 1;

        $manager->setTable(Argument::type('string'))->shouldBeCalled();
        $manager->setDTOClass(Argument::type('string'))->shouldBeCalled();
        $manager->setFetchMode(Argument::type('integer'))->shouldBeCalled();

        $manager->setStmt(Argument::type('string'), Argument::type('array'))->willReturn($stmt);

        $stmt->fetch()->willReturn($field);

        $this->findFieldById($privilegeId, $fieldId)->shouldReturn($field);
    }

    public function it_finds_fields(Manager $manager, PDOStatement $stmt): void
    {
        $privilegeId = 1;

        $manager->setTable(Argument::type('string'))->shouldBeCalled();
        $manager->setDTOClass(Argument::type('string'))->shouldBeCalled();
        $manager->setFetchMode(Argument::type('integer'))->shouldBeCalled();

        $manager->setStmt(Argument::type('string'), Argument::type('array'))->willReturn($stmt);

        $stmt->fetchAll()->willReturn([]);

        $this->findFields($privilegeId)->shouldReturn([]);
    }

    public function it_updates_fields(Manager $manager): void
    {
        $privilegeId = 1;
        $fieldsIds = [1, 2, 3];

        $manager->setTable(Argument::type('string'))->shouldBeCalled();
        $manager->beginTransaction()->shouldBeCalled();
        $manager->delete(Argument::type('array'))->shouldBeCalled();
        $manager->createMultiple(Argument::type('array'))->shouldBeCalled();
        $manager->commit()->shouldBeCalled();

        $this->updateFields($privilegeId, $fieldsIds);
    }

    public function it_removes_fields(Manager $manager): void
    {
        $privilegeId = 1;

        $manager->setTable(Argument::type('string'))->shouldBeCalled();
        $manager->delete(Argument::type('array'))->shouldBeCalled();

        $this->removeFields($privilegeId);
    }


    public function it_finds_roles(Manager $manager, PDOStatement $stmt): void
    {
        $privilegeId = 1;

        $manager->setTable(Argument::type('string'))->shouldBeCalled();
        $manager->setDTOClass(Argument::type('string'))->shouldBeCalled();
        $manager->setFetchMode(Argument::type('integer'))->shouldBeCalled();

        $manager->setStmt(Argument::type('string'), Argument::type('array'))->willReturn($stmt);

        $stmt->fetchAll()->willReturn([]);

        $this->findRoles($privilegeId)->shouldReturn([]);
    }

    public function it_updates_roles(Manager $manager): void
    {
        $privilegeId = 1;
        $rolesIds = [1, 2, 3];

        $manager->setTable(Argument::type('string'))->shouldBeCalled();
        $manager->beginTransaction()->shouldBeCalled();
        $manager->delete(Argument::type('array'))->shouldBeCalled();
        $manager->createMultiple(Argument::type('array'))->shouldBeCalled();
        $manager->commit()->shouldBeCalled();

        $this->updateRoles($privilegeId, $rolesIds);
    }

    public function it_removes_roles(Manager $manager): void
    {
        $privilegeId = 1;

        $manager->setTable(Argument::type('string'))->shouldBeCalled();
        $manager->delete(Argument::type('array'))->shouldBeCalled();

        $this->removeRoles($privilegeId);
    }


    public function it_finds_access(Manager $manager, PDOStatement $stmt, AccessDTO $access): void
    {
        $privilegeId = 1;

        $manager->setTable(Argument::type('string'))->shouldBeCalled();
        $manager->setDTOClass(Argument::type('string'))->shouldBeCalled();
        $manager->setFetchMode(Argument::type('integer'))->shouldBeCalled();

        $manager->setStmt(Argument::type('string'), Argument::type('array'))->willReturn($stmt);

        $access->getId()->willReturn(1);

        $stmt->fetch()->willReturn($access);

        $this->findAccess($privilegeId)->shouldReturn($access);
    }

    public function it_sets_access(Manager $manager): void
    {
        $privilegeId = 1;
        $accessId = 1;

        $manager->setTable(Argument::type('string'))->shouldBeCalled();
        $manager->update(Argument::type('array'), Argument::type('array'))->shouldBeCalled();

        $this->setAccess($privilegeId, $accessId);
    }

    public function it_removes_access(Manager $manager): void
    {
        $privilegeId = 1;

        $manager->setTable(Argument::type('string'))->shouldBeCalled();
        $manager->update(['access_id' => null], Argument::type('array'))->shouldBeCalled();

        $this->removeAccess($privilegeId);
    }
}
