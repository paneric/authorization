<?php /** @noinspection AccessModifierPresentedInspection */

namespace spec\Paneric\Authorization\PDO\Repository;

use Paneric\Authorization\DTO\AccessDTO;
use Paneric\Authorization\ORM\Entity\Field;
use Paneric\Authorization\PDO\Repository\AccessRepository;
use Paneric\PdoWrapper\Manager;
use Paneric\PdoWrapper\QueryBuilder;
use PDOStatement;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class AccessRepositorySpec extends ObjectBehavior
{
    public function let(Manager $manager): void
    {
        $this->beConstructedWith($manager);
    }

    function it_is_initializable(): void
    {
        $this->shouldHaveType(AccessRepository::class);
    }


    public function it_finds_by_ids(Manager $manager): void
    {
        $ids = [1, 2];

        $manager->setTable(Argument::type('string'))->shouldBeCalled();
        $manager->setDTOClass(Argument::type('string'))->shouldBeCalled();
        $manager->setFetchMode(Argument::type('integer'))->shouldBeCalled();

        $manager->findBySame(Argument::type('array'))->willReturn([]);

        $this->findByIds($ids)->shouldReturn([]);
    }


    public function it_finds_one_by_id(
        Manager $manager,
        QueryBuilder $queryBuilder,
        PDOStatement $stmt,
        Field $field
    ): void {
        $fieldId = 1;

        $manager->setTable(Argument::type('string'))->shouldBeCalled();
        $manager->setDTOClass(Argument::type('string'))->shouldBeCalled();
        $manager->setFetchMode(Argument::type('integer'))->shouldBeCalled();

        $manager->getQueryBuilder()->willReturn($queryBuilder);

        $queryBuilder->select(Argument::type('string'))->willReturn($queryBuilder);
        $queryBuilder->where(Argument::type('array'))->willReturn($queryBuilder);

        $queryBuilder->getQuery()->willReturn('query');

        $manager->setStmt(Argument::type('string'), Argument::type('array'))->willReturn($stmt);

        $stmt->fetch()->willReturn($field);

        $this->findOneById($fieldId)->shouldReturn($field);
    }

    public function it_creates(Manager $manager, AccessDTO $accessDTO): void
    {
        $manager->setTable(Argument::type('string'))->shouldBeCalled();

        $accessDTO->convert()->willReturn([]);

        $manager->create(Argument::type('array'))->shouldBeCalled();

        $this->create($accessDTO);
    }

    public function it_updates(Manager $manager, AccessDTO $accessDTO): void
    {
        $accessId = 1;

        $manager->setTable(Argument::type('string'))->shouldBeCalled();

        $accessDTO->convert()->willReturn([]);

        $manager->update(Argument::type('array'), Argument::type('array'))->shouldBeCalled();

        $this->update($accessId, $accessDTO);
    }

    public function it_removes(Manager $manager): void
    {
        $accessId = 1;

        $manager->setTable(Argument::type('string'))->shouldBeCalled();

        $manager->delete(Argument::type('array'))->shouldBeCalled();

        $this->remove($accessId);
    }
}
