<?php /** @noinspection AccessModifierPresentedInspection */

namespace spec\Paneric\Authorization\PDO\Repository;

use Paneric\Authorization\DTO\FieldDTO;
use Paneric\Authorization\Interfaces\FieldQueryInterface;
use Paneric\Authorization\ORM\Entity\Field;
use Paneric\Authorization\PDO\Repository\FieldRepository;
use Paneric\PdoWrapper\Manager;
use Paneric\PdoWrapper\QueryBuilder;
use PDOStatement;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class FieldRepositorySpec extends ObjectBehavior
{
    public function let(Manager $manager, FieldQueryInterface $fieldQuery): void
    {
        $this->beConstructedWith($manager, $fieldQuery);
    }

    function it_is_initializable(): void
    {
        $this->shouldHaveType(FieldRepository::class);
    }


    public function it_finds_by_ids(Manager $manager): void
    {
        $ids = [1, 2, 3];

        $manager->setTable(Argument::type('string'))->shouldBeCalled();
        $manager->setDTOClass(Argument::type('string'))->shouldBeCalled();
        $manager->setFetchMode(Argument::type('integer'))->shouldBeCalled();

        $manager->findBySame(Argument::type('array'))->willReturn([]);

        $this->findByIds($ids)->shouldReturn([]);
    }


    public function it_finds_one_by_id(
        Manager $manager,
        QueryBuilder $queryBuilder,
        PDOStatement $stmt,
        Field $field,
        FieldQueryInterface $fieldQuery
    ): void {
        $fieldId = 1;

        $manager->setTable(Argument::type('string'))->shouldBeCalled();
        $manager->setDTOClass(Argument::type('string'))->shouldBeCalled();
        $manager->setFetchMode(Argument::type('integer'))->shouldBeCalled();

        $manager->getQueryBuilder()->willReturn($queryBuilder);

        $queryBuilder->select(Argument::type('string'))->willReturn($queryBuilder);
        $queryBuilder->where(Argument::type('array'))->willReturn($queryBuilder);

        $queryBuilder->getQuery()->willReturn('query');

        $manager->beginTransaction()->shouldBeCalled();

        $manager->setStmt(Argument::type('string'), Argument::type('array'))->willReturn($stmt);

        $stmt->fetch()->willReturn($field);

        $field->getId()->willReturn(1);

        $fieldQuery->findPrivileges(1)->willReturn([]);

        $manager->commit()->shouldBeCalled();

        $field->updatePrivileges([])->shouldBeCalled();


        $this->findOneById($fieldId)->shouldReturn($field);
    }

    public function it_creates(Manager $manager, FieldDTO $fieldDTO): void
    {
        $manager->setTable(Argument::type('string'))->shouldBeCalled();

        $fieldDTO->convert()->willReturn([]);

        $manager->create(Argument::type('array'))->shouldBeCalled();

        $this->create($fieldDTO);
    }

    public function it_updates(Manager $manager, FieldDTO $fieldDTO): void
    {
        $fieldId = 1;

        $manager->setTable(Argument::type('string'))->shouldBeCalled();

        $fieldDTO->convert()->willReturn([]);

        $manager->update(Argument::type('array'), Argument::type('array'))->shouldBeCalled();

        $this->update($fieldId, $fieldDTO);
    }

    public function it_removes(Manager $manager): void
    {
        $fieldId = 1;

        $manager->setTable(Argument::type('string'))->shouldBeCalled();

        $manager->delete(Argument::type('array'))->shouldBeCalled();

        $this->remove($fieldId);
    }
}
