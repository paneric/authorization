<?php /** @noinspection AccessModifierPresentedInspection */

namespace spec\Paneric\Authorization\PDO\Repository;

use Paneric\Authorization\DTO\RoleDTO;
use Paneric\Authorization\Interfaces\RoleQueryInterface;
use Paneric\Authorization\PDO\Query\RoleQuery;
use Paneric\Authorization\PDO\Repository\RoleRepository;
use Paneric\PdoWrapper\Manager;
use Paneric\PdoWrapper\QueryBuilder;
use PDOStatement;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class RoleRepositorySpec extends ObjectBehavior
{
    public function let(Manager $manager, RoleQueryInterface $roleQuery): void
    {
        $this->beConstructedWith($manager, $roleQuery);
    }

    function it_is_initializable(): void
    {
        $this->shouldHaveType(RoleRepository::class);
    }


    public function it_finds_by_ids(Manager $manager): void
    {
        $ids = [1, 2, 3];

        $manager->setTable(Argument::type('string'))->shouldBeCalled();
        $manager->setDTOClass(Argument::type('string'))->shouldBeCalled();
        $manager->setFetchMode(Argument::type('integer'))->shouldBeCalled();

        $manager->findBySame(Argument::type('array'))->willReturn([]);

        $this->findByIds($ids)->shouldReturn([]);
    }


    public function it_finds_one_by_id(
        Manager $manager,
        QueryBuilder $queryBuilder,
        PDOStatement $stmt,
        RoleDTO $role,
        RoleQuery $roleQuery
    ): void {
        $roleId = 1;

        $manager->setTable(Argument::type('string'))->shouldBeCalled();
        $manager->setDTOClass(Argument::type('string'))->shouldBeCalled();
        $manager->setFetchMode(Argument::type('integer'))->shouldBeCalled();

        $manager->getQueryBuilder()->willReturn($queryBuilder);

        $queryBuilder->select(Argument::type('string'))->willReturn($queryBuilder);
        $queryBuilder->where(Argument::type('array'))->willReturn($queryBuilder);

        $queryBuilder->getQuery()->willReturn(Argument::type('string'));

        $manager->beginTransaction()->shouldBeCalled();

        $manager->setStmt(Argument::type('string'), Argument::type('array'))->willReturn($stmt);

        $stmt->fetch()->willReturn($role);

        $role->getId()->willReturn(1);

        $roleQuery->findPrivileges(1)->willReturn([]);

        $manager->commit()->shouldBeCalled();

        $role->updatePrivileges([])->shouldBeCalled();


        $this->findOneById($roleId)->shouldReturn($role);
    }

    public function it_creates(Manager $manager, RoleDTO $roleDTO): void
    {
        $manager->setTable(Argument::type('string'))->shouldBeCalled();

        $roleDTO->convert()->willReturn([]);

        $manager->create(Argument::type('array'))->shouldBeCalled();

        $this->create($roleDTO);
    }

    public function it_updates(Manager $manager, RoleDTO $roleDTO): void
    {
        $roleId = 1;

        $manager->setTable(Argument::type('string'))->shouldBeCalled();

        $roleDTO->convert()->willReturn([]);

        $manager->update(Argument::type('array'), Argument::type('array'))->shouldBeCalled();

        $this->update($roleId, $roleDTO);
    }

    public function it_removes(Manager $manager): void
    {
        $roleId = 1;

        $manager->setTable(Argument::type('string'))->shouldBeCalled();

        $manager->delete(Argument::type('array'))->shouldBeCalled();

        $this->remove($roleId);
    }
}
