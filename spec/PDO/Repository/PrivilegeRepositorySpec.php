<?php /** @noinspection AccessModifierPresentedInspection */

namespace spec\Paneric\Authorization\PDO\Repository;

use Paneric\Authorization\DTO\PrivilegeDTO;
use Paneric\Authorization\Interfaces\PrivilegeQueryInterface;
use Paneric\Authorization\ORM\Entity\Access;
use Paneric\Authorization\ORM\Entity\Privilege;
use Paneric\Authorization\PDO\Repository\PrivilegeRepository;
use Paneric\PdoWrapper\Manager;
use Paneric\PdoWrapper\QueryBuilder;
use PDOStatement;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class PrivilegeRepositorySpec extends ObjectBehavior
{
    public function let(Manager $manager, PrivilegeQueryInterface $privilegeQuery): void
    {
        $this->beConstructedWith($manager, $privilegeQuery);
    }

    function it_is_initializable(): void
    {
        $this->shouldHaveType(PrivilegeRepository::class);
    }


    public function it_finds_by_ids(Manager $manager): void
    {
        $ids = [1, 2, 3];

        $manager->setTable(Argument::type('string'))->shouldBeCalled();
        $manager->setDTOClass(Argument::type('string'))->shouldBeCalled();
        $manager->setFetchMode(Argument::type('integer'))->shouldBeCalled();

        $manager->findBySame(Argument::type('array'))->willReturn([]);

        $this->findByIds($ids)->shouldReturn([]);
    }


    public function it_finds_one_by_id(
        Manager $manager,
        QueryBuilder $queryBuilder,
        PDOStatement $stmt,
        Privilege $privilege,
        PrivilegeQueryInterface $privilegeQuery,
        Access $access
    ): void {
        $privilegeId = 1;

        $manager->setTable(Argument::type('string'))->shouldBeCalled();
        $manager->setDTOClass(Argument::type('string'))->shouldBeCalled();
        $manager->setFetchMode(Argument::type('integer'))->shouldBeCalled();

        $manager->getQueryBuilder()->willReturn($queryBuilder);

        $queryBuilder->select(Argument::type('string'))->willReturn($queryBuilder);
        $queryBuilder->where(Argument::type('array'))->willReturn($queryBuilder);

        $queryBuilder->getQuery()->willReturn('query');

        $manager->beginTransaction()->shouldBeCalled();

        $manager->setStmt(Argument::type('string'), Argument::type('array'))->willReturn($stmt);

        $stmt->fetch()->willReturn($privilege);

        $privilege->getId()->willReturn(1);

        $privilegeQuery->findRoles(1)->willReturn([]);
        $privilegeQuery->findFields(1)->willReturn([]);
        $privilegeQuery->findAccess(1)->willReturn($access);

        $manager->commit()->shouldBeCalled();

        $privilege->updateRoles([])->shouldBeCalled();
        $privilege->updateFields([])->shouldBeCalled();
        $privilege->updateAccess($access)->shouldBeCalled();


        $this->findOneById($privilegeId)->shouldReturn($privilege);
    }

    public function it_creates(Manager $manager, PrivilegeDTO $privilegeDTO): void
    {
        $manager->setTable(Argument::type('string'))->shouldBeCalled();

        $privilegeDTO->convert()->willReturn([]);

        $manager->create(Argument::type('array'))->shouldBeCalled();

        $this->create($privilegeDTO);
    }

    public function it_updates(Manager $manager, PrivilegeDTO $privilegeDTO): void
    {
        $fieldId = 1;

        $manager->setTable(Argument::type('string'))->shouldBeCalled();

        $privilegeDTO->convert()->willReturn([]);

        $manager->update(Argument::type('array'), Argument::type('array'))->shouldBeCalled();

        $this->update($fieldId, $privilegeDTO);
    }

    public function it_removes(Manager $manager): void
    {
        $privilegeId = 1;

        $manager->setTable(Argument::type('string'))->shouldBeCalled();

        $manager->delete(Argument::type('array'))->shouldBeCalled();

        $this->remove($privilegeId);
    }
}
