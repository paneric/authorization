<?php

declare(strict_types=1);

require 'vendor/autoload.php';

use Paneric\Authorization\PDO\Query\FieldQuery;
use Paneric\Authorization\PDO\Query\PrivilegeQuery;
use Paneric\Authorization\PDO\Query\RoleQuery;
use Paneric\Authorization\PDO\Repository\AccessRepository;
use Paneric\Authorization\PDO\Repository\PrivilegeRepository;
use Paneric\PdoWrapper\DataPreparator;
use Paneric\PdoWrapper\Manager;
use Paneric\PdoWrapper\PDOBuilder;
use Paneric\PdoWrapper\QueryBuilder;
use Paneric\PdoWrapper\SequencePreparator;
use test\Paneric\Authorization\PDO\Query\AccessQueryTest;
use test\Paneric\Authorization\PDO\Query\FieldQueryTest;
use test\Paneric\Authorization\PDO\Query\PrivilegeQueryTest;
use test\Paneric\Authorization\PDO\Query\RoleQueryTest;
use test\Paneric\Authorization\PDO\Repository\AccessRepositoryTest;
use test\Paneric\Authorization\PDO\Repository\FieldRepositoryTest;
use test\Paneric\Authorization\PDO\Repository\PrivilegeRepositoryTest;
use test\Paneric\Authorization\PDO\Repository\RoleRepositoryTest;

/**  PREPARATIONS (with PDO):
 * ----------------
 * 1) CLI: sudo service mysql start;
 * 2) CLI: mysql -u root -p
 * 3) MySql Shell: mysql> create database authorization;
 * 4) MySql Shell: mysql> exit;
 * 5) fixtures and schema from: authorization.sql
 * 6) php raw-orm-test
 */

$config = include(__DIR__ . '/pdo-config.php');

$sequencePreparator = new SequencePreparator();
$dataPreparator = new DataPreparator();

$pdoBuilder = new PDOBuilder();

$manager = new Manager(
    $pdoBuilder->build($config),
    new QueryBuilder($sequencePreparator),
    $dataPreparator
);

$accessRepository = new AccessRepository($manager);
$fieldQuery = new FieldQuery($manager);

$privilegeQuery = new PrivilegeQuery($manager, $fieldQuery);
$roleQuery = new RoleQuery($manager, $privilegeQuery);


$accessRepositoryTest = new AccessRepositoryTest($manager);
$accessRepositoryTest->runTests();

$fieldRepositoryTest = new FieldRepositoryTest($manager, $fieldQuery);
$fieldRepositoryTest->runTests();

$privilegeRepositoryTest = new PrivilegeRepositoryTest($manager, $privilegeQuery);
$privilegeRepositoryTest->runTests();

$roleRepositoryTest = new RoleRepositoryTest($manager, $roleQuery);
$roleRepositoryTest->runTests();


$accessQueryTest = new AccessQueryTest($manager);
$accessQueryTest->runTests();

$fieldQueryTest = new FieldQueryTest($manager);
$fieldQueryTest->runTests();

$privilegeQueryTest = new PrivilegeQueryTest($manager, $fieldQuery);
$privilegeQueryTest->runTests();

$roleQueryTest = new RoleQueryTest($manager, $privilegeQuery);
$roleQueryTest->runTests();
