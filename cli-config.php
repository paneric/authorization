<?php

declare(strict_types=1);

use Doctrine\ORM\Tools\Console\ConsoleRunner;

define('APP_ROOT', __DIR__);

require APP_ROOT.'/vendor/autoload.php';

return ConsoleRunner::createHelperSet(
    require 'doctrine-orm.php'
);
