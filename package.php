<?php

declare(strict_types=1);

use DI\Container;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\PdoWrapper\Manager;
use Paneric\Authorization\PDO\Query\AccessQuery;
use Paneric\Authorization\PDO\Query\FieldQuery;
use Paneric\Authorization\PDO\Query\PrivilegeQuery;
use Paneric\Authorization\PDO\Query\RoleQuery;
use Paneric\Authorization\PDO\Repository\AccessRepository;
use Paneric\Authorization\PDO\Repository\FieldRepository;
use Paneric\Authorization\PDO\Repository\PrivilegeRepository;
use Paneric\Authorization\PDO\Repository\RoleRepository;
use Paneric\Authorization\Service\AccessService;
use Paneric\Authorization\Service\FieldService;
use Paneric\Authorization\Service\PrivilegeService;
use Paneric\Authorization\Service\RoleService;
use Paneric\Authorization\Controller\Client\AccessController;
use Paneric\Authorization\Controller\Client\FieldController;
use Paneric\Authorization\Controller\Client\PrivilegeController;
use Paneric\Authorization\Controller\Client\RoleController;
use Twig\Environment as Twig;

return [
    AccessQuery::class => static function (Container $container): AccessQuery
    {
        return new AccessQuery($container->get(Manager::class));
    },

    FieldQuery::class => static function (Container $container): FieldQuery
    {
        return new FieldQuery($container->get(Manager::class));
    },

    PrivilegeQuery::class => static function (Container $container): PrivilegeQuery
    {
        return new PrivilegeQuery(
            $container->get(Manager::class),
            $container->get(FieldQuery::class)
        );
    },

    RoleQuery::class => static function (Container $container): RoleQuery
    {
        return new RoleQuery(
            $container->get(Manager::class),
            $container->get(PrivilegeQuery::class)
        );
    },


    AccessRepository::class => static function (Container $container): AccessRepository
    {
        return new AccessRepository($container->get(Manager::class));
    },

    FieldRepository::class => static function (Container $container): FieldRepository
    {
        return new FieldRepository(
            $container->get(Manager::class),
            $container->get(FieldQuery::class)
        );
    },

    PrivilegeRepository::class => static function (Container $container): PrivilegeRepository
    {
        return new PrivilegeRepository(
            $container->get(Manager::class),
            $container->get(PrivilegeQuery::class)
        );
    },

    RoleRepository::class => static function (Container $container): RoleRepository
    {
        return new RoleRepository(
            $container->get(Manager::class),
            $container->get(RoleQuery::class)
        );
    },


    AccessService::class => static function (Container $container): AccessService
    {
        return new AccessService(
            $container->get(AccessRepository::class),
            $container->get(AccessQuery::class),
            $container->get(PrivilegeRepository::class),
            $container->get(SessionInterface::class)
        );
    },

    FieldService::class => static function (Container $container): FieldService
    {
        return new FieldService(
            $container->get(FieldRepository::class),
            $container->get(FieldQuery::class),
            $container->get(PrivilegeRepository::class),
            $container->get(SessionInterface::class)
        );
    },

    PrivilegeService::class => static function (Container $container): PrivilegeService
    {
        return new PrivilegeService(
            $container->get(PrivilegeRepository::class),
            $container->get(PrivilegeQuery::class),
            $container->get(FieldRepository::class),
            $container->get(RoleRepository::class),
            $container->get(AccessRepository::class),
            $container->get(SessionInterface::class)
        );
    },

    RoleService::class => static function (Container $container): RoleService
    {
        return new RoleService(
            $container->get(RoleRepository::class),
            $container->get(RoleQuery::class),
            $container->get(PrivilegeRepository::class),
            $container->get(SessionInterface::class)
        );
    },


    AccessController::class => static function (Container $container): AccessController
    {
        return new AccessController(
            $container->get(AccessService::class),
            $container->get(Twig::class)
        );
    },

    FieldController::class => static function (Container $container): FieldController
    {
        return new FieldController(
            $container->get(FieldService::class),
            $container->get(Twig::class)
        );
    },

    PrivilegeController::class => static function (Container $container): PrivilegeController
    {
        return new PrivilegeController(
            $container->get(PrivilegeService::class),
            $container->get(Twig::class)
        );
    },

    RoleController::class => static function (Container $container): RoleController
    {
        return new RoleController(
            $container->get(RoleService::class),
            $container->get(Twig::class)
        );
    },
];
