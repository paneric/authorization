<?php

declare(strict_types=1);

use Paneric\Authentication\CredentialDTO;
use Paneric\Authorization\DTO\AccessDTO;
use Paneric\Authorization\DTO\FieldDTO;
use Paneric\Authorization\DTO\PrivilegeDTO;
use Paneric\Authorization\DTO\RoleDTO;

return [
    'validation' => [
        'autho.access.add' => [
            'methods' => ['POST'],
            AccessDTO::class => [
                'rules' => [
                    'level' => [
                        'required' => [],
                        'is_integer' => [],
                    ],
                    'ref' => [
                        'required' => [],
                        'is_all_alpha_numeric' => [],
                    ],
                    'info' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                ],
            ],
        ],
        'autho.access.edit' => [
            'methods' => ['POST'],
            AccessDTO::class => [
                'rules' => [
                    'level' => [
                        'required' => [],
                        'is_integer' => [],
                    ],
                    'ref' => [
                        'required' => [],
                        'is_all_alpha_numeric' => [],
                    ],
                    'info' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                ],
            ],
        ],
        'autho.access.delete' => [
            'methods' => ['POST'],
            AccessDTO::class => [
                'rules' => [],
            ],
        ],

        'autho.field.add' => [
            'methods' => ['POST'],
            FieldDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                        'is_all_alpha_numeric' => [],
                    ],
                ],
            ],
        ],
        'autho.field.edit' => [
            'methods' => ['POST'],
            FieldDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                        'is_all_alpha_numeric' => [],
                    ],
                ],
            ],
        ],
        'autho.field.delete' => [
            'methods' => ['POST'],
            AccessDTO::class => [
                'rules' => [],
            ],
        ],

        'autho.privilege.add' => [
            'methods' => ['POST'],
            PrivilegeDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                        'is_all_alpha_numeric' => [],
                    ],
                    'access' => [
                        'is_integer' => [],
                    ],
                    'route' => [
                        'required' => [],
                        'is_all_alpha_numeric' => [],
                    ],
                    'info' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                ],
            ],
        ],
        'autho.privilege.edit' => [
            'methods' => ['POST'],
            PrivilegeDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                        'is_all_alpha_numeric' => [],
                    ],
                    'access' => [
                        'is_integer' => [],
                    ],
                    'route' => [
                        'is_all_alpha_numeric' => [],
                    ],
                    'info' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                ],
            ],
        ],
        'autho.privilege.delete' => [
            'methods' => ['POST'],
            PrivilegeDTO::class => [
                'rules' => [],
            ],
        ],

        'autho.role.add' => [
            'methods' => ['POST'],
            RoleDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                        'is_all_alpha_numeric' => [],
                    ],
                    'info' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                ],
            ],
        ],
        'autho.role.edit' => [
            'methods' => ['POST'],
            RoleDTO::class => [
                'rules' => [
                    'ref' => [
                        'required' => [],
                        'is_all_alpha_numeric' => [],
                    ],
                    'info' => [
                        'required' => [],
                        'is_text' => [],
                    ],
                ],
            ],
        ],
        'autho.role.delete' => [
            'methods' => ['POST'],
            RoleDTO::class => [
                'rules' => [],
            ],
        ],

        'autho.role.privileges.edit' => [
            'methods' => ['POST'],
            PrivilegeDTO::class => [
                'rules' => [
                    'id' => [
                        'is_integer' => [],
                    ],
                ],
            ],
        ],
        'autho.role.privileges.delete' => [
            'methods' => ['POST'],
            PrivilegeDTO::class => [
                'rules' => [],
            ],
        ],

        'autho.privilege.roles.edit' => [
            'methods' => ['POST'],
            RoleDTO::class => [
                'rules' => [
                    'id' => [
                        'is_integer' => [],
                    ],
                ],
            ],
        ],
        'autho.privilege.roles.delete' => [
            'methods' => ['POST'],
            RoleDTO::class => [
                'rules' => [],
            ],
        ],

        'autho.privilege.fields.edit' => [
            'methods' => ['POST'],
            FieldDTO::class => [
                'rules' => [
                    'id' => [
                        'is_integer' => [],
                    ],
                ],
            ],
        ],
        'autho.privilege.fields.delete' => [
            'methods' => ['POST'],
            FieldDTO::class => [
                'rules' => [],
            ],
        ],

        'autho.field.privileges.edit' => [
            'methods' => ['POST'],
            PrivilegeDTO::class => [
                'rules' => [
                    'id' => [
                        'is_integer' => [],
                    ],
                ],
            ],
        ],
        'autho.field.privileges.delete' => [
            'methods' => ['POST'],
            PrivilegeDTO::class => [
                'rules' => [],
            ],
        ],

        'autho.access.privileges.edit' => [
            'methods' => ['POST'],
            PrivilegeDTO::class => [
                'rules' => [
                    'id' => [
                        'is_integer' => [],
                    ],
                ],
            ],
        ],
        'autho.access.privileges.delete' => [
            'methods' => ['POST'],
            PrivilegeDTO::class => [
                'rules' => [],
            ],
        ],
    ],
];
